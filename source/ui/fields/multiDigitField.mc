import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    class MultiDigitField extends MultiField {
        /*
        Field for choosing a series of digits
        */
        protected var _numDigits as Number;

        protected var _valueDigits as Array<Number?>;

        function initialize(value as NumberRef, callback as NumberCallback?, numDigits as Number, theme as Theme, options as {
            :isEnabled as BooleanRef, :isFocused as BooleanRef, :isEditing as BooleanRef, :isValid as BooleanRef,
            :widthReferenceChar as Char, :doneEditingCallback as ObjectCallback,
        }) {
            /*
            :param value: Value of this field
            :param callback: (optional) Callback when the value of this field changes
            :param numDigits: Number of digits to choose
            :param options:
                :isEnabled: Whether this field is enabled (default: true)
                :isFocused: Whether this field is focused (default: false)
                :isEditing: Whether this field is being edited (default: false)
                :isValid: Whether this field is valid (default: true)
                :isDigitWrapEnabled: If true, wrap digits from 0-9 and vice versa (default: true)
                :widthReferenceChar: If provided, fix the width of each digit at the width of this char. If not provided,
                    fix the width of each digit at the width of a 9.
                :doneEditingCallback: Callback when user is finished editing field (presses enter at the end)
            */
            _numDigits = numDigits;

            var widthReferenceChar = Utils.getDefault(options, :widthReferenceChar, null) as Char?;

            var currentValue = value instanceof Ref ? (value.value() as Number) : value;
            if (currentValue != null) {
                _valueDigits = Utils.calculateDigitsFromValue(currentValue, numDigits);
            } else {
                _valueDigits = Utils.repeat(null, numDigits);
            }
            var isDigitWrapEnabled = Utils.getDefault(options, :isDigitWrapEnabled, true) as Boolean;
            
            var fields = [];
            for (var i = 0; i < numDigits; i++) {
                var numberFieldOptions = {
                    :minValue => 0,
                    :maxValue => 9,
                    :nullDisplayValue => "–",
                    :isWrapEnabled => isDigitWrapEnabled,
                    :justification => Graphics.TEXT_JUSTIFY_CENTER,
                };
                if (widthReferenceChar != null) {
                    numberFieldOptions[:widthReferenceValue] = widthReferenceChar as Number;
                } else {
                    numberFieldOptions[:widthNumDigits] = 1;
                }
                fields.add(new NumberField(_valueDigits[i], null, theme, numberFieldOptions));
            }

            options[:spacing] = 0;

            MultiField.initialize(value, callback, fields, theme, options);
        }

        function zero() as Void {
            // Set all digits to zero
            setValue(0);
        }

        protected function _setSubfieldValues(value as Object?) as Void {
            // Calculate digits from value and update each digit in the field
            value = value as Number?;
            if (value != null) {
                _valueDigits = Utils.calculateDigitsFromValue(value, _numDigits);
                for (var i = 0; i < _numDigits; i++) {
                    var field = _children[i] as NumberField;
                    field.setValue(_valueDigits[i]);
                }
            } else {
                // Null value, "zero out" subfields
                for (var i = 0; i < _numDigits; i++) {
                    var field = _children[i] as NumberField;
                    field.setValue(null);
                }
            }
        }

        protected function _incrementDigit(n as Number) as Boolean {
            if (_valueDigits[n] < 9) {
                _valueDigits[n] += 1;
            } else {
                _valueDigits[n] = 0;
            }
            var digit = _children[n] as NumberField;
            digit.setValue(_valueDigits[n]);
            digit._playTone();
            var newValue = Utils.calculateValueFromDigits(_valueDigits);
            _updateValue(newValue);
            return true;
        }

        protected function _decrementDigit(n as Number) as Boolean {
            if (_valueDigits[n] > 0) {
                _valueDigits[n] -= 1;
            } else {
                _valueDigits[n] = 9;
            }
            var digit = _children[n] as NumberField;
            digit.setValue(_valueDigits[n]);
            digit._playTone();
            var newValue = Utils.calculateValueFromDigits(_valueDigits);
            _updateValue(newValue);
            return true;
        }

        function onPreviousPage() as Boolean {
            if (isEditing()) {
                // Sub-field is in control
                return _incrementDigit(_focus);
            } else if (!isAtStart()) {
                // Scroll to previous field
                goToPrevious();
                return true;
            }
            return false;
        }

        function onEnter() as Boolean {
            if (isEditing()) {
                // Done editing
                setIsEditing(false);
                if (!isAtEnd()) {
                    // If not at the end, switch to editing the next digit
                    goToNext();
                    setIsEditing(true);
                } else {
                    _notifyDoneEditing();
                }
            } else {
                // Edit focused field
                setIsEditing(true);
            }
            return true;
        }

        function onNextPage() as Boolean {
            if (isEditing()) {
                // Sub-field is in control
                return _decrementDigit(_focus);
            } else if (!isAtEnd()) {
                // Scroll to next field
                goToNext();
                return true;
            }
            return false;
        }

        function toString() as String {
            return "MultiDigitField{digits=" + _children.toString() + "}";
        }
    }
}
