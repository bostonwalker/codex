import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    class EnumField extends BaseField {

        protected var _valueToDisplayString as Dictionary<Object, String>;

        protected var _isWrapEnabled as Boolean;
        protected var _nullDisplayValue as String?;

        protected var _valueIndex as Number;

        function initialize(value as Ref or Object, callback as ObjectCallback?,
            valueToDisplayString as Dictionary<Object, String or ResourceId>, theme as Theme, options as {
                :prefix as String or ResourceId, :suffix as String or ResourceId, :justification as Graphics.TextJustification,
                :isWrapEnabled as Boolean, :widthReferenceValue as String,
                :isEnabled as BooleanRef, :isFocused as BooleanRef, :isEditing as BooleanRef, :isValid as BooleanRef,
                :isVisible as BooleanRef,
        }) {
            /*
            :param value: Initial value or reference to reactive value
            :param callback: (optional) Callback on value change
            :param valueToDisplayString: Mapping of possible values -> display values
            :param theme: Styling options
            :param options:
                :prefix: Prefix to display before value being selected
                :suffix: Suffix to display after value being selected
                :justification: Text justification
                :nullDisplayValue: If provided, display this string when value is null
                :isWrapEnabled: If true, selection can wrap from first to last value and vice versa (default: false)
                :widthReferenceValue: If provided, fix field width to contain this value
                :isEnabled: Whether this field is enabled (default: true)
                :isFocused: Whether this field is focused (default: false)
                :isEditing: Whether this field is being edited (default: false)
                :isValid: Whether this field is valid (default: true)
                :isVisible: Whether to draw this field or not (default: true)
            */
            if (valueToDisplayString.size() < 2) {
                throw new ValueError(
                    "Must supply at least 2 possible values to select from");
            }

            // Resolve all ResourceIds to Strings
            _valueToDisplayString = {};
            var keys = valueToDisplayString.keys();
            for (var i = 0; i < keys.size(); i++) {
                var k = keys[i];
                var v = Utils.resolveString(valueToDisplayString[k]);
                _valueToDisplayString[k] = v;
            }

            var currentValue = value instanceof Ref ? (value.value() as Object) : value;
            
            _valueIndex = _valueToDisplayString.keys().indexOf(currentValue);
            if (_valueIndex == null) {
                throw new ValueError(
                    "Value out of range: " + currentValue.toString());
            }
            
            _isWrapEnabled = Utils.getDefault(options, :isWrapEnabled, false) as Boolean;
            _nullDisplayValue = Utils.getDefault(options, :nullDisplayValue, null) as String?;

            BaseField.initialize(value, callback, theme, options);
        }

        protected function _formatValue(value as Object?) as String {
            // Get formatted value text
            // Value is a key, return display string
            if (value != null) {
                if (!_valueToDisplayString.hasKey(value)) {
                    throw new ValueError(
                        "Value out of range: " + value.toString());
                }
                return _valueToDisplayString[value];
            } else if (_nullDisplayValue != null) {
                return _nullDisplayValue;
            } else {
                throw new ValueError(
                    "Unexpected null value");
            }
        }

        function _incrementValue() as Boolean {
            var newValueIndex;
            if (_valueIndex < _valueToDisplayString.size() - 1) {
                newValueIndex = _valueIndex + 1;
            } else if (_isWrapEnabled) {
                newValueIndex = 0;
            } else {
                newValueIndex = _valueIndex;
            }
            if (newValueIndex != _valueIndex) {
                var newValue = _valueToDisplayString.keys()[newValueIndex];
                _updateValue(newValue);
                return true;
            }
            return false;
        }

        function _decrementValue() as Boolean {
            var newValueIndex;
            if (_valueIndex > 0) {
                newValueIndex = _valueIndex - 1;
            } else if (_isWrapEnabled) {
                newValueIndex = _valueToDisplayString.size() - 1;
            } else {
                newValueIndex = _valueIndex;
            }
            if (newValueIndex != _valueIndex) {
                var newValue = _valueToDisplayString.keys()[newValueIndex];
                _updateValue(newValue);
                return true;
            }
            return false;
        }

        function toString() as String {
            return "EnumField{value=" + _value.toString() + "}";
        }
    }
}
