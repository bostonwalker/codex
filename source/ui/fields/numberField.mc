import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    class NumberField extends BaseField {

        protected var _minValue as Number?;
        protected var _maxValue as Number?;
        protected var _isWrapEnabled as Boolean;
        protected var _format as String?;
        protected var _nullDisplayValue as String;

        function initialize(value as NumberRef, callback as NumberCallback?, theme as Codex.Theme, options as {
            :minValue as NumberRef, :maxValue as NumberRef, :isWrapEnabled as Boolean, :format as String, :nullDisplayValue as String,
            :prefix as String or ResourceId, :suffix as String or ResourceId,
            :justification as Graphics.TextJustification, :widthReferenceValue as Number, :widthNumDigits as Number,
            :isEnabled as BooleanRef, :isFocused as BooleanRef, :isEditing as BooleanRef, :isValid as BooleanRef,
        }) {
            /*
            :param value: Initial value or reference to reactive value
            :param callback: (optional) Callback on value change
            :param theme: Styling options
            :param options:
                :minValue: If provided, minimum number value
                :maxValue: If provided, maximum number value
                :isWrapEnabled: If true, wrap from min to max value and vice versa (default: false)
                :format: If provided, format string for number being selected
                :nullDisplayValue: If provided, display this string when value is null (default: hyphen)
                :prefix: Prefix to display before value being selected
                :suffix: Suffix to display after value being selected
                :justification: Text justification
                :widthReferenceValue: If provided, fix field width to contain this value. Cannot pass along with `widthNumDigits`.
                :widthNumDigits: If provided, fix field width to contain a given number of digits. Cannot pass along with `widthReferenceValue`.
                :isEnabled: Whether this field is enabled (default: true)
                :isFocused: Whether this field is focused (default: false)
                :isEditing: Whether this field is being edited (default: false)
                :isValid: Whether this field is valid (default: true)
            */
            _minValue = Ref.link(self, :setMinValue, Utils.getDefault(options, :minValue, null) as NumberRef?) as Number?;
            _maxValue = Ref.link(self, :setMaxValue, Utils.getDefault(options, :maxValue, null) as NumberRef?) as Number?;
            _isWrapEnabled = Utils.getDefault(options, :isWrapEnabled, false) as Boolean;
            if (_isWrapEnabled && (_minValue == null || _maxValue == null)) {
                throw new Lang.InvalidValueException(
                    "Must supply both `minValue` and `maxValue` if `isWrapEnabled` = true");
            }

            _format = Utils.getDefault(options, :format, null) as String?;
            _nullDisplayValue = Utils.getDefault(options, :nullDisplayValue, "-") as String;

            var widthNumDigits = Utils.getDefault(options, :widthNumDigits, null) as Number?;
            var widthReferenceValue = Utils.getDefault(options, :widthReferenceValue, null) as Number?;
            if (widthNumDigits != null) {
                if (widthReferenceValue != null) {
                    throw new ValueError(
                        "Cannot supply both `widthNumDigits` and `widthReferenceValue`");
                }
                // Set width of field using a string of nines
                options[:widthReferenceValue] = Utils.calculateValueFromDigits(Utils.repeat(9, widthNumDigits) as Array<Number>);
            }

            BaseField.initialize(value, callback, theme, options);
        }

        function setMinValue(value as NumberRef) {
            // Set min value for field. If array is given, can set each min value separately
            _minValue = Ref.link(self, :setMinValue, value) as Number?;
        }

        function setMaxValue(value as NumberRef) {
            // Set max value for field. If array is given, can set each max value separately.
            _minValue = Ref.link(self, :setMaxValue, value) as Number?;
        }

        protected function _incrementValue() as Boolean {
            var value = getValue() as Number;
            var newValue;
            if (_maxValue == null || value < _maxValue) {
                newValue = value + 1;
            } else if (value == _maxValue && _isWrapEnabled) {
                newValue = _minValue;
            } else {
                newValue = value;
            }
            if (newValue != value) {
                _updateValue(newValue);
                return true;
            }
            return false;
        }

        protected function _decrementValue() as Boolean {
            var value = getValue() as Number;
            var newValue;
            if (_minValue == null || value > _minValue) {
                newValue = value - 1;
            } else if (value == _minValue && _isWrapEnabled) {
                newValue = _maxValue;
            } else {
                newValue = value;
            }
            if (newValue != value) {
                _updateValue(newValue);
                return true;
            }
            return false;
        }

        protected function _formatValue(value as Object?) as String {
            // Get formatted value text
            value = value as Number?;
            if (value != null) {
                if (_format != null) {
                    return value.format(_format);
                } else {
                    return value.toString();
                }
            } else {
                return _nullDisplayValue;
            }
        }

        function toString() as String {
            return "NumberField{value=" + _value.toString() + "}";
        }
    }
}
