import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    typedef Field as interface {  // extends Drawable, Focusable, Scrollable, Controllable

        function getValue() as Object?;

        function setValue(value as Ref or Object or Null) as Void;

        function isEnabled() as Boolean;

        function setIsEnabled(value as BooleanRef) as Void;

        function isEditing() as Boolean;

        function setIsEditing(value as BooleanRef) as Void;

        function isValid() as Boolean;

        function setIsValid(value as BooleanRef) as Void;

        /* Focusable */
        function isFocusable() as Boolean;
        
        function isFocused() as Boolean;

        function setIsFocused(value as BooleanRef) as Void;

        /* Drawable */
        function isVisible() as Boolean;

        function setIsVisible(value as BooleanRef) as Void;

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void;

        function getWidth() as Number;

        function getHeight() as Number;

        /* Scrollable */
        function goToStart() as Void;

        function goToEnd() as Void;

        function goToNext() as Void;

        function goToPrevious() as Void;

        function isAtStart() as Boolean;

        function isAtEnd() as Boolean;

        /* Controllable */
        function onEnter() as Boolean;

        function onPreviousPage() as Boolean;

        function onNextPage() as Boolean;

        function onBack() as Boolean;        

        function onTapAt(x as Number, y as Number) as Boolean;

        function onTapElsewhere() as Void;
    };
}
