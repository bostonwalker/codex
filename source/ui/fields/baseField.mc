import Toybox.Lang;
using Toybox.Graphics;
using Toybox.Attention;


module Codex {

    class BaseField {  // implements Field
        /*
        A field displaying a value to be chosen by selecting next/previous on the device
        */
        protected var _value as Ref or Object or Null;
        protected var _callback as ObjectCallback?;

        protected var _tone as Attention.Tone?;

        protected var _isEnabled as Boolean;
        protected var _isFocused as Boolean;
        protected var _isEditing as Boolean;
        protected var _isValid as Boolean;
        protected var _isVisible as Boolean;
        protected var _requestReloadCallback as Callback?;
        protected var _doneEditingCallback as ObjectCallback?;

        protected var _prefixText as TextElement?;
        protected var _valueText as TextElement;
        protected var _suffixText as TextElement?;

        // Colors
        protected var _textColor as Number;
        protected var _textColorFocused as Number;
        protected var _textColorDisabled as Number;
        protected var _textColorInvalid as Number;
        protected var _backgroundColor as Number;
        protected var _backgroundColorFocused as Number;
        protected var _backgroundColorEditing as Number;
        protected var _underscoreColor as Number;

        // State variables
        protected var _isLoaded as Boolean;
        protected var _width as Number?;
        protected var _height as Number;

        function initialize(value as Object or Codex.Ref or Null, callback as ObjectCallback?, theme as Codex.Theme, options as {
            :prefix as String or ResourceId, :suffix as String or ResourceId, :justification as Graphics.TextJustification,
            :widthReferenceValue as Object?, :tone as Attention.Tone?,
            :isEnabled as BooleanRef, :isFocused as BooleanRef, :isEditing as BooleanRef, :isValid as BooleanRef,
            :isVisible as BooleanRef, :requestReloadCallback as Callback, :doneEditingCallback as ObjectCallback
        }) {
            /*
            :param value: Value of this field
            :param callback: (optional) Callback when the value of this field changes
            :param theme: Styling options
            :param options:
                :prefix: Prefix to display before value being selected
                :suffix: Suffix to display after value being selected
                :justification: Text justification
                :widthReferenceValue: If provided, fix field width to contain this value
                :tone: Tone to play when editing field (default: Attention.TONE_KEY)
                :isEnabled: Whether this field is enabled (default: true)
                :isFocused: Whether this field is focused (default: false)
                :isEditing: Whether this field is being edited (default: false)
                :isValid: Whether this field is valid (default: true)
                :isVisible: Whether to draw this field or not (default: true)
                :requestReloadCallback: Callback to parent to request reload when this Field changes size
                :doneEditingCallback: Callback when user is finished editing field (presses enter)
            */
            _value = value;
            _callback = callback;

            var prefix = Utils.getDefault(options, :prefix, null) as String?;
            var suffix = Utils.getDefault(options, :suffix, null) as String?;
            var justification = Utils.getDefault(options, :justification, Graphics.TEXT_JUSTIFY_RIGHT) as Graphics.TextJustification;
            var widthReferenceValue = Utils.getDefault(options, :widthReferenceValue, null) as Object?;
            _tone = Utils.getDefault(options, :tone, Attention has :playTone ? Attention.TONE_KEY : null) as Attention.Tone?;

            _isEnabled = Ref.linkBoolean(self, :setIsEnabled, Utils.getDefault(options, :isEnabled, true) as BooleanRef);
            _isFocused = Ref.linkBoolean(self, :setIsFocused, Utils.getDefault(options, :isFocused, false) as BooleanRef);
            _isEditing = Ref.linkBoolean(self, :setIsEditing, Utils.getDefault(options, :isEditing, false) as BooleanRef);
            _isValid = Ref.linkBoolean(self, :setIsValid, Utils.getDefault(options, :isValid, true) as BooleanRef);
            _isVisible = Ref.linkBoolean(self, :setIsVisible, Utils.getDefault(options, :isVisible, true) as BooleanRef);
            _requestReloadCallback = Utils.getDefault(options, :requestReloadCallback, null) as Callback?;
            _doneEditingCallback = Utils.getDefault(options, :doneEditingCallback, null) as ObjectCallback?;

            _textColor = theme.color.text;
            _textColorFocused = theme.color.background;
            _textColorDisabled = theme.color.majorAccent;
            _textColorInvalid = theme.color.indicatorDisabled;
            _backgroundColor = theme.color.background;
            _backgroundColorFocused = theme.color.text;
            _backgroundColorEditing = theme.color.majorAccent;
            _underscoreColor = theme.color.majorAccent;

            if (prefix != null) {
                _prefixText = new TextElement(prefix, theme.field.fieldFont,
                    theme.color.majorAccent, theme.color.background, {});
            }

            var valueTextOptions = {
                :justification => justification,
                :horizontalRule => true,
                :horizontalRuleColor => theme.color.majorAccent,
                :fillWidthWithBackgroundColor => true,
            };
            if (widthReferenceValue != null) {
                // Fix text width to contain width reference value
                var widthReferenceString = _formatValue(widthReferenceValue);
                valueTextOptions[:widthReferenceString] = widthReferenceString;
            }
            _valueText = new TextElement("", theme.field.fieldFont,  _getTextColor(), _getTextBackgroundColor(), valueTextOptions);

            if (suffix != null) {
                _suffixText = new TextElement(suffix, theme.field.fieldFont, 
                    theme.color.majorAccent, theme.color.background, {});
            }

            _height = Graphics.getFontHeight(theme.field.fieldFont);

            _isLoaded = false;

            // Initialize number field with value
            if (_value instanceof Ref) {
                setValue(_value.value());
                // Update number field automatically
                Ref.link(self, :setValue, _value);
            } else {
                setValue(_value);
            }
        }

        function getValue() as Object? {
            if (_value instanceof Ref) {
                return _value.value();
            } else {
                return _value;
            }
        }

        function setValue(value as Ref or Object or Null) as Void {
            // Called when value is set externally
            if (!(_value instanceof Ref)) {
                _value = value;
                _updateDisplayValue(value);
            } else {
                _updateDisplayValue(value.value());
            }
        }

        protected function _updateValue(value as Object?) as Void {
            // Called when value is set internally by field
            // Only update if value is not a ref, otherwise callback should trigger a value update
            if (!(_value instanceof Ref)) {
                _value = value;
                _updateDisplayValue(value);
            }
            if (_callback != null) {
                // Allow external reaction to change
                _callback.invoke(value);
            }
            _playTone();
        }

        protected function _updateDisplayValue(value as Object?) as Void {
            // Change text to be displayed
            var displayValue = _formatValue(value);
            _valueText.setStr(displayValue);
            _requestReload();
        }

        protected function _incrementValue() as Boolean {
            // Called when user tries to increment the field value
            throw new NotImplementedError();
        }

        protected function _decrementValue() as Boolean {
            // Called when user tries to decrement the field value
            throw new NotImplementedError();
        }

        protected function _formatValue(value as Object?) as String {
            // Get formatted value text
            throw new NotImplementedError();
        }

        function isEnabled() as Boolean {
            return _isEnabled;
        }
        
        function setIsEnabled(value as BooleanRef) as Void {
            _isEnabled = Ref.linkBoolean(self, :setIsEnabled, value);
            _valueText.setTextColor(_getTextColor());
            _valueText.setBackgroundColor(_getTextBackgroundColor());
        }

        function isEditing() as Boolean {
            return _isEditing;
        }

        function setIsEditing(value as BooleanRef) as Void {
            _isEditing = Ref.linkBoolean(self, :setIsEditing, value);
            _valueText.setTextColor(_getTextColor());
            _valueText.setBackgroundColor(_getTextBackgroundColor());
            if (_isEditing) {
                _playTone();
            }
        }

        function isValid() as Boolean {
            return _isValid;
        }

        function setIsValid(value as BooleanRef) as Void {
            _isValid = Ref.linkBoolean(self, :setIsValid, value);
            _valueText.setTextColor(_getTextColor());
            _valueText.setBackgroundColor(_getTextBackgroundColor());
        }

        /* Focusable */
        function isFocusable() as Boolean {
            return _isEnabled;
        }
        
        function isFocused() as Boolean {
            return _isFocused;
        }

        function setIsFocused(value as BooleanRef) as Void {
            _isFocused = Ref.linkBoolean(self, :setIsFocused, value);
            _valueText.setTextColor(_getTextColor());
            _valueText.setBackgroundColor(_getTextBackgroundColor());
        }

        protected function _getTextColor() as Number {
            // Get color of value text
            if (_isFocused) {
                return _textColorFocused;
            } else if (_isValid) {
                if (_isEnabled) {
                    return _textColor;
                } else {
                    return _textColorDisabled;
                }
            } else {
                return _textColorInvalid;
            }
        }

        protected function _getTextBackgroundColor() as Number {
            // Get background color of value text
            if (_isEditing) {
                return _backgroundColorEditing;
            } else if (_isFocused) {
                return _backgroundColorFocused;
            } else {
                return _backgroundColor;
            }
        }

        /* Loadable */
        function load(dc as Graphics.Dc) as Void {
            // Load elements and add up element widths
            _width = 0;
            if (_prefixText != null) {
                _prefixText.load(dc);
                _width += _prefixText.getWidth();
            }
             _valueText.load(dc);
            _width += _valueText.getWidth();
            if (_suffixText != null) {
                _suffixText.load(dc);
                _width += _suffixText.getWidth();
            }
            _isLoaded = true;
        }

        function unload() as Void {
            if (_prefixText != null) {
                _prefixText.unload();
            }
            _valueText.unload();
            if (_suffixText != null) {
                _suffixText.unload();
            }
            _isLoaded = false;
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        function setRequestReloadCallback(value as Callback) as Void {
            _requestReloadCallback = value;
        }

        protected function _requestReload() as Void {
            _isLoaded = false;
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        protected function _requestParentReload() as Void {
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        protected function _notifyDoneEditing() as Void {
            // Send value to done editing callback, if registered
            if (_doneEditingCallback != null) {
                _doneEditingCallback.invoke(getValue());
            }
        }

        /* Drawable */
        function isVisible() as Boolean {
            return _isVisible;
        }

        function setIsVisible(value as BooleanRef) as Void {
            _isVisible = Ref.linkBoolean(self, :setIsVisible, value);
            _requestParentReload();
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            if (!_isLoaded) {
                load(dc);
            }

            // Draw prefix, value, suffix
            var xDraw = x;
            if (_prefixText != null) {
                _prefixText.draw(dc, xDraw, y);
                xDraw += _prefixText.getWidth();
            }
            _valueText.draw(dc, xDraw, y);
            if (_suffixText != null) {
                xDraw += _valueText.getWidth();
                _suffixText.draw(dc, xDraw, y);
            }
        }

        function getWidth() as Number {
            if (!_isLoaded) {
                throw new Lang.OperationNotAllowedException(
                    "Field not loaded");
            }
            return _width as Number;
        }

        /* Scrollable */
        function getHeight() as Number {
            return _height;
        }

        function goToStart() as Void {}

        function goToEnd() as Void {}

        function goToNext() as Void {}

        function goToPrevious() as Void {}

        function isAtStart() as Boolean {
            return true;
        }

        function isAtEnd() as Boolean {
            return true;
        }

        /* Controllable */
        function onEnter() as Boolean {
            if (isEditing()) {
                // Move onto editing the next field
                setIsEditing(false);
                _notifyDoneEditing();
            } else {
                // Edit focused field
                setIsEditing(true);
            }
            return true;

        }

        function onPreviousPage() as Boolean {
            if (isEditing()) {
                // Edit value
                return _incrementValue();
            }
            return false;
        }

        function onNextPage() as Boolean {
            if (isEditing()) {
                // Edit value
                return _decrementValue();
            }
            return false;
        }

        function onBack() as Boolean {
            // Back button was pressed or screen was swiped right
            if (isEditing()) {
                setIsEditing(false);
                return true;
            }
            return false;
        }

        function onTapAt(x as Number, y as Number) as Boolean {
            if (!_isEditing) {
                setIsEditing(true);
                return true;
            }
            return false;
        }

        function onTapElsewhere() as Void {
            setIsEditing(false);
        }

        function _playTone() {
            // Play tone if device supports
            if (_tone != null) {
                Attention.playTone(_tone);
            }
        }
    }
}
