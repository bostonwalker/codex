import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    class MultiField extends ScrollContainer {

        protected var _value as Ref or Object or Null;
        protected var _callback as ObjectCallback?;
        protected var _doneEditingCallback as ObjectCallback?;

        // Selection/editing state variables
        protected var _isValid as Boolean;

        function initialize(value as Ref or Object or Null, callback as ObjectCallback?, fields as Array<Field>, theme as Theme, options as {
            :isEnabled as BooleanRef, :isFocused as BooleanRef, :focus as NumberRef, :isEditing as BooleanRef, :isValid as BooleanRef,
            :layout as Container.Layout, :width as Number, :height as Number, :spacing as Number,
            :horizontalAlign as Container.HorizontalAlign, :verticalAlign as Container.VerticalAlign, :isVisible as BooleanRef,
            :doneEditingCallback as ObjectCallback,
        }) {
            /*
            :param value: Value of this field
            :param callback: (optional) Callback when the value of this field changes
            :param fields: Fields within this multifield
            :param theme: Styling options
            :param options:
                :isEnabled: Whether this field is enabled (default: true)
                :isFocused: Whether this field is focused (default: false)
                :focus: Current focus of this field (default: 0)
                :isValid: Whether this field is valid (default: true)
                :layout: Whether container is laid out horizontally or vertically (default: horizontal)
                :width: Width of container. If not provided, will be set automatically to the width of its contents.
                :height: Height of container. If not provided, will be set automatically to the height of its contents.
                :spacing: Spacing in between child components (default: use theme)
                :horizontalAlign: How to align elements horizontally (default: left align)
                :verticalAlign: How to align elements vertically (default: top align)
                :isVisible: Whether to draw this field or not (default: true)
                :doneEditingCallback: Callback when user is finished editing field (presses enter at the end)
            */
            _value = Ref.link(self, :setValue, value);
            _callback = callback;

            _isValid = Ref.linkBoolean(self, :setIsValid, Utils.getDefault(options, :isValid, true) as BooleanRef);
            _doneEditingCallback = Utils.getDefault(options, :doneEditingCallback, null) as ObjectCallback?;

            ScrollContainer.initialize(fields as Array<Drawable>, theme, options);  // Note: redundant cast due to type checker error

            // Propagate isValid and isEnabled values to child fields
            for (var i = 0; i < _children.size(); i++) {
                var field = _children[i] as Field;
                field.setIsValid(_isValid);
                field.setIsEnabled(_isEnabled);
            }
            
            _setSubfieldValues(_value);
        }

        function getValue() as Object? {
            if (_value instanceof Ref) {
                return _value.value();
            } else {
                return _value;
            }
        }

        function setValue(value as Ref or Object or Null) as Void {
            // Called when value is set externally
            _value = Ref.link(self, :setValue, value);
            _setSubfieldValues(_value);
        }

        protected function _setSubfieldValues(value as Object?) as Void {
            throw new NotImplementedError();
        }

        protected function _updateValue(value as Object?) as Void {
            // Called when value is updated by a subfield
            // Only update if value is not a ref, otherwise callback should trigger a value update
            if (!(_value instanceof Ref)) {
                _value = value;
            }
            if (_callback != null) {
                // Allow external reaction to change
                _callback.invoke(value);
            }
        }

        function setIsEnabled(value as BooleanRef) as Void {
            // Enable or disable all subfields together
            ScrollContainer.setIsEnabled(value);
            for (var i = 0; i < _children.size(); i++) {
                var field = _children[i] as Field;
                field.setIsEnabled(_isEnabled);  // Call to super method first will set `_isEnabled` field
            }
        }

        function isFocusable() as Boolean {
            // Override so field is not focusable if not enabled
            return _isEnabled;
        }

        function isEditing() as Boolean {
            if (_focus != null) {
                var focusField = _children[_focus] as Field;
                return focusField.isEditing();
            }
            return false;
        }

        function setIsEditing(value as BooleanRef) as Void {
            value = Ref.linkBoolean(self, :setIsEditing, value);
            if (_focus != null) {
                var focusField = _children[_focus] as Field;
                focusField.setIsEditing(value);
            }
        }

        function isValid() as Boolean {
            return _isValid;
        }

        function setIsValid(value as BooleanRef) as Void {
            _isValid = Ref.linkBoolean(self, :setIsValid, value);
            for (var i = 0; i < _children.size(); i++) {
                var field = _children[i] as Field;
                field.setIsValid(_isValid);
            }
        }

        function onEnter() as Boolean {
            if (_focus != null) {
                var focusField = _children[_focus] as Field;
                if (focusField.isEnabled()) {
                    if (focusField.isEditing() && focusField.isAtEnd()) {
                        // Move to editing next field, if possible
                        focusField.setIsEditing(false);
                        if (!isAtEnd()) {
                            goToNext();
                            var newFocusField = _children[_focus] as Field;
                            if (newFocusField.isEnabled()) {
                                newFocusField.setIsEditing(true);
                            }
                        } else {
                            // User was at the end and pressed enter
                            _notifyDoneEditing();
                        }
                        return true;
                    } else {
                        return focusField.onEnter();
                    }
                } else {
                    return false;
                }
            } else {
                goToStart();
                return true;
            }
        }

        protected function _notifyDoneEditing() as Void {
            // Send value to done editing callback, if registered
            if (_doneEditingCallback != null) {
                _doneEditingCallback.invoke(getValue());
            }
        }
        
        function toString() as String {
            return "MultiField{fields=" + _children.toString() + "}";
        }
    }
}
