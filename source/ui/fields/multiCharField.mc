import Toybox.Lang;
using Toybox.Graphics;
using Toybox.StringUtil;


module Codex {

    class MultiCharField extends MultiField {
        /*
        Field for choosing a series of chars
        */
        enum Mode {
            MODE_ALPHANUM,  // Allow A-Z, 0-9 + space
            MODE_RANGE,  // Allow char values between `minValue` and `maxValue`
        }

        protected var _mode as Mode;
        protected var _numChars as Number;
        
        protected var _isCharWrapEnabled as Boolean;
        protected var _minValue as Array<Char>?;
        protected var _maxValue as Array<Char>?;

        protected var _valueChars as Array<Char?>;

        function initialize(value as StringRef, callback as StringCallback?, mode as Mode, numChars as Number, theme as Theme, options as {
            :isEnabled as BooleanRef, :isFocused as BooleanRef, :isEditing as BooleanRef, :isValid as BooleanRef,
            :minValue as CharRef or ArrayRef, :maxValue as CharRef or ArrayRef, :isCharWrapEnabled as Boolean,
            :widthReferenceChar as Char, :doneEditingCallback as ObjectCallback,
        }) {
            /*
            :param value: Value of this field
            :param callback: (optional) Callback when the value of this field changes
            :param mode: Operating mode. If MODE_RANGE, uses `minValue` and `maxValue` options to set a range of chars to choose from.
            :param numDigits: Number of digits to choose
            :param options:
                :isEnabled: Whether this field is enabled (default: true)
                :isFocused: Whether this field is focused (default: false)
                :isEditing: Whether this field is being edited (default: false)
                :isValid: Whether this field is valid (default: true)
                :minValue: If `mode` = MODE_RANGE, minimum value for each char in the field (default: 'A'). If an array is given, is set separately for each char.
                :maxValue: If `mode` = MODE_RANGE, maximum value for each char in the field (default: 'Z'). If an array is given, is set separately for each char.
                :isCharWrapEnabled: If true, wrap from min-max value and vice versa (default: false)
                :widthReferenceChar: Fix the width of each char subfield at the width of this char (default: 'M')
                :doneEditingCallback: Callback when user is finished editing field (presses enter at the end)
            */
            _mode = mode;
            _numChars = numChars;

            if (mode == MODE_ALPHANUM) {
                if (options.hasKey(:minValue) || options.hasKey(:maxValue)) {
                    throw new ValueError(
                        "Cannot supply `minValue` or `maxValue` if `mode` = MODE_ALPHANUM");
                }
            } else if (mode == MODE_RANGE) {
                // Handle min and max value
                var minValue = Utils.getDefault(options, :minValue, 'A') as CharRef or ArrayRef;
                var maxValue = Utils.getDefault(options, :maxValue, 'Z') as CharRef or ArrayRef;

                // Link min and max value
                setMinValue(minValue);
                setMaxValue(maxValue);
            } else {
                throw new NotImplementedError();
            }

            _isCharWrapEnabled = Utils.getDefault(options, :isCharWrapEnabled, false) as Boolean;
            var widthReferenceChar = Utils.getDefault(options, :widthReferenceChar, 'M') as Char;
            
            var currentValue = value instanceof Ref ? value.value() : value;
            if (currentValue != null) {
                _valueChars = currentValue.toCharArray();
                if (_valueChars.size() != numChars) {
                    throw new ValueError(
                        "`value` string must be same length as number of chars in this MultiCharField");
                }
            } else {
                _valueChars = Utils.repeat(null, numChars);
            }
            
            var fields = [];
            for (var i = 0; i < numChars; i++) {
                var numberFieldOptions = {
                    :nullDisplayValue => "–",  // en dash
                    :justification => Graphics.TEXT_JUSTIFY_CENTER,
                };
                if (widthReferenceChar != null) {
                    numberFieldOptions[:widthReferenceValue] = widthReferenceChar as Number;
                }
                fields.add(new NumberField(_valueChars[i] as Number?, null, theme, numberFieldOptions));
            }

            options[:spacing] = 0;

            MultiField.initialize(value, callback, fields, theme, options);
        }

        function clear(char as Char) as Void {
            // Set all chars to value
            var clearString = StringUtil.charArrayToString(Utils.repeat(char, _numChars) as Array<Char>);
            setValue(clearString);
        }

        function setMinValue(value as CharRef or ArrayRef) {
            // Set min value for field. If array is given, can set each min value separately
            if (_mode != MODE_RANGE) {
                throw new NotImplementedError();
            }
            _minValue = Ref.link(self, :setMinValue, value) as Char or Array<Char>;
            if (_minValue instanceof Char) {
                _minValue = Utils.repeat(_minValue, _numChars) as Array<Char>;
            }
        }

        function setMaxValue(value as CharRef or ArrayRef) {
            // Set max value for field. If array is given, can set each max value separately.
            if (_mode != MODE_RANGE) {
                throw new NotImplementedError();
            }
            _maxValue = Ref.link(self, :setMaxValue, value) as Char or Array<Char>;
            if (_maxValue instanceof Char) {
                _maxValue = Utils.repeat(_maxValue, _numChars) as Array<Char>;
            }
        }

        protected function _setSubfieldValues(value as Object?) as Void {
            // Update char fields with string
            value = value as String?;
            if (value != null) {
                if (value.length() != _numChars) {
                    throw new ValueError(
                        "`value` string must be same length as number of chars in this MultiCharField");
                }
                _valueChars = value.toCharArray();
                for (var i = 0; i < _numChars; i++) {
                    var field = _children[i] as NumberField;
                    field.setValue(_valueChars[i]);
                }
            } else {
                // Null value, "zero out" subfields
                for (var i = 0; i < _numChars; i++) {
                    var field = _children[i] as NumberField;
                    field.setValue(null);
                }
            }
        }

        protected function _incrementChar(n as Number) as Boolean {
            var char = _valueChars[n];
            if (_mode == MODE_ALPHANUM) {
                // Handle alphanum discontinuities
                if (char == ' ') {
                    char = 'A';
                } else if (char >= 'A' && char < 'Z') {
                    char += 1;
                } else if (char == 'Z') {
                    char = '0';
                } else if (char >= '0' && char < '9') {
                    char += 1;
                } else if (char == '9' && _isCharWrapEnabled) {
                    char = ' ';
                }
            } else if (_mode == MODE_RANGE) {
                // Enforce min value
                if (char < _maxValue[n]) {
                    char += 1;
                } else if (_isCharWrapEnabled) {
                    char = _minValue[n];
                }
            }
            _valueChars[n] = char;
            var charField = _children[n] as NumberField;
            charField.setValue(_valueChars[n]);
            charField._playTone();
            var newValue = StringUtil.charArrayToString(_valueChars);
            _updateValue(newValue);
            return true;
        }

        protected function _decrementChar(n as Number) as Boolean {
            var char = _valueChars[n];
            if (_mode == MODE_ALPHANUM) {
                // Handle alphanum discontinuities
                if (char == ' ' && _isCharWrapEnabled) {
                    char = '9';
                } else if (char == 'A') {
                    char = ' ';
                } else if (char > 'A' && char <= 'Z') {
                    char += -1;
                } else if (char == '0') {
                    char = 'Z';
                } else if (char > '0' && char <= '9') {
                    char += -1;
                }
            } else if (_mode == MODE_RANGE) {
                // Enforce min value
                if (char > _minValue[n]) {
                    char += -1;
                } else if (_isCharWrapEnabled) {
                    char = _maxValue[n];
                }
            }
            _valueChars[n] = char;
            var charField = _children[n] as NumberField;
            charField.setValue(_valueChars[n]);
            charField._playTone();
            var newValue = StringUtil.charArrayToString(_valueChars);
            _updateValue(newValue);
            return true;
        }

        function onEnter() as Boolean {
            if (isEditing()) {
                // Done editing
                setIsEditing(false);
                if (!isAtEnd()) {
                    // If not at the end, switch to editing the next char
                    goToNext();
                    setIsEditing(true);
                } else {
                    _notifyDoneEditing();
                }
            } else {
                // Edit focused field
                setIsEditing(true);
            }
            return true;
        }

        function onPreviousPage() as Boolean {
            if (isEditing()) {
                // Sub-field is in control
                return _incrementChar(_focus);
            } else if (!isAtStart()) {
                // Scroll to previous field
                goToPrevious();
                return true;
            }
            return false;
        }

        function onNextPage() as Boolean {
            if (isEditing()) {
                // Sub-field is in control
                return _decrementChar(_focus);
            } else if (!isAtEnd()) {
                // Scroll to next field
                goToNext();
                return true;
            }
            return false;
        }

        function toString() as String {
            return "MultiCharField{chars=" + _children.toString() + "}";
        }
    }
}
