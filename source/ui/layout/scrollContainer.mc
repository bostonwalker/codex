import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    class ScrollContainer extends Container {  // implements Focusable, Scrollable, Controllable
        /*
        Container with the ability to direct input to scrollable content
        */
        protected var _isEnabled as Boolean;
        protected var _isFocused as Boolean;
        protected var _focus as Number?;
        protected var _isWrapEnabled as Boolean;

        function initialize(children as Array<Drawable>, theme as Theme, options as {
                :isEnabled as BooleanRef, :isFocused as BooleanRef, :focus as Number or Drawable or Null,
                :isWrapEnabled as Boolean, :layout as Container.Layout, :width as Number, :height as Number, :spacing as Number,
                :horizontalAlign as Container.HorizontalAlign, :verticalAlign as Container.VerticalAlign, :isVisible as BooleanRef
        }) {
            /*
            :param children: Elements to layout within container. Optional interfaces:
                Loadable, Focusable, Scrollable, Controllable
            :param theme: Styling options
            :param options:
                :isEnabled: Whether ScrollContainer is enabled (default: true)
                :isFocused: Whether ScrollContainer is focused (default: true)
                :focus: Focus of ScrollContainer (default: index of first visible item)
                :isWrapEnabled: If true, wrap to bottom when scrolling up from top and vice versa (default: true)
                :layout: Whether container is laid out horizontally or vertically (default: LAYOUT_HORIZONTAL)
                :width: Width of container. If not provided, will be set automatically to the width of its contents.
                :height: Height of container. If not provided, will be set automatically to the height of its contents.
                :spacing: Spacing in between child components (default: 0)
                :horizontalAlign: How to align elements horizontally (default: HORIZONTAL_ALIGN_LEFT)
                :verticalAlign: How to align elements vertically (default: VERTICAL_ALIGN_TOP)
                :isVisible: Whether or not to draw container & contents (default: true)
            */
            Container.initialize(children, theme, options);
            
            _isEnabled = Ref.linkBoolean(self, :setIsEnabled, Utils.getDefault(options, :isEnabled, true) as BooleanRef);
            _isFocused = Ref.linkBoolean(self, :setIsFocused, Utils.getDefault(options, :isFocused, false) as BooleanRef);
            var focus = Utils.getDefault(options, :focus, _getFirstFocusableIndex()) as Ref or Drawable or Null;
            _isWrapEnabled = Utils.getDefault(options, :isWrapEnabled, false) as Boolean;
            
            setFocus(focus);
        }

        function setContents(value as Array<Drawable>) {
            Container.setContents(value);
            _focus = null;
            goToStart();
        }

        function isEnabled() as Boolean {
            return _isEnabled;
        }

        function setIsEnabled(value as BooleanRef) as Void {
            _isEnabled = Ref.linkBoolean(self, :setIsEnabled, value);
        }

        /* Focusable */
        function isFocusable() as Boolean {
            return true;
        }

        function isFocused() as Boolean {
            return _isFocused;
        }

        function setIsFocused(value as BooleanRef) as Void {
            value = Ref.linkBoolean(self, :setIsFocused, value);
            if (_focus != null) {
                var focusObject = _children[_focus];
                if (focusObject has :setIsFocused) {
                    focusObject.setIsFocused(value);
                }
                requestReload();
            }
            _isFocused = value;
        }

        function getFocus() as Number? {
            return _focus;
        }

        (:typecheck(false))
        function setFocus(newFocus as Number or Drawable or Null) as Void {
            if (newFocus != null) {
                if (newFocus instanceof Number) {
                    // Check if number in range
                    if (newFocus < 0 || newFocus >= _children.size()) {
                        throw new ValueError(
                            "Focus out of range: " + newFocus.toString());
                    }
                } else {
                    // Find index of object
                    var newFocusIndex = _children.indexOf(newFocus);
                    if (newFocusIndex == null) {
                        throw new ValueError(
                            "Object not found in container: " + newFocus.toString());
                    }
                    newFocus = newFocusIndex;
                }
            }
            if (_focus != null) {
                var oldFocusObject = _children[_focus];
                oldFocusObject.setIsFocused(false);
            }
            if (_isFocused && newFocus != null) {
                var newFocusObject = _children[newFocus];
                if (!(newFocusObject has :isFocusable) || !newFocusObject.isFocusable()) {
                    throw new ValueError(
                        "Focused object not focusable!");
                }
                if (!newFocusObject.isVisible()) {
                    throw new ValueError(
                        "Focused object not visible");
                }
                newFocusObject.setIsFocused(true);
            }
            _focus = newFocus;
            requestReload();
        }

        /* Scrollable */
        function goToStart() as Void {
            setFocus(_getFirstFocusableIndex());
            if (_children[_focus] has :goToStart) {
                // Item is scrollable
                (_children[_focus] as Scrollable).goToStart();
            }
        }

        function goToEnd() as Void {
            setFocus(_getLastFocusableIndex());
            if (_children[_focus] has :goToEnd) {
                // Item is scrollable
                (_children[_focus] as Scrollable).goToEnd();
            }
        }

        function goToNext() as Void {
            setFocus(_getNextFocusableIndex());
            if (_children[_focus] has :goToStart) {
                // Item is scrollable
                (_children[_focus] as Scrollable).goToStart();
            }
        }

        function goToPrevious() as Void {
            setFocus(_getPreviousFocusableIndex());
            if (_children[_focus] has :goToEnd) {
                // Item is scrollable
                (_children[_focus] as Scrollable).goToEnd();
            }
        }

        function isAtStart() as Boolean {
            if (_focus == _getFirstFocusableIndex()) {
                if (_children[_focus] has :isAtStart) {
                    // Item is scrollable
                    return (_children[_focus] as Scrollable).isAtStart();
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }

        function isAtEnd() as Boolean {
            if (_focus == _getLastFocusableIndex()) {
                if (_children[_focus] has :isAtEnd) {
                    // Item is scrollable
                    return (_children[_focus] as Scrollable).isAtEnd();
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }

        protected function _getNextFocusableIndex() as Number {
            for (var i = _focus + 1; i < _children.size(); i++) {
                var child = _children[i];
                if (child.isVisible() && child has :isFocusable && child.isFocusable()) {
                    return i;
                }
            }
            throw new ValueError(
                "No further objects were focusable");
        }

        protected function _getPreviousFocusableIndex() as Number {
            for (var i = _focus - 1; i >= 0; i--) {
                var child = _children[i];
                if (child.isVisible() && child has :isFocusable && child.isFocusable()) {
                    return i;
                }
            }
            throw new ValueError(
                "No previous objects were focusable");
        }

        protected function _getFirstFocusableIndex() as Number {
            for (var i = 0; i < _children.size(); i++) {
                var child = _children[i];
                if (child.isVisible() && child has :isFocusable && child.isFocusable()) {
                    return i;
                }
            }
            throw new ValueError(
                "No objects were focusable");
        }

        protected function _getLastFocusableIndex() as Number {
            for (var i = _children.size() - 1; i >= 0; i--) {
                var child = _children[i];
                if (child.isVisible() && child has :isFocusable && child.isFocusable()) {
                    return i;
                }
            }
            throw new ValueError(
                "No objects were focusable");
        }

        /* Controllable */
        /* ScrollContainer should be focused as a precondition to receiving input */
        function onEnter() as Boolean {
            if (_focus != null) {
                var focusObject = _children[_focus];
                if ((!(focusObject has :isEnabled) || focusObject.isEnabled()) && focusObject has :onEnter) {
                    return focusObject.onEnter();
                } else {
                    return false;
                }
            } else {
                goToStart();
                return true;
            }
        }

        function onPreviousPage() as Boolean {
            if (_focus != null) {
                var focusObject = _children[_focus];
                var isHandledByChild;
                if ((!(focusObject has :isEnabled) || focusObject.isEnabled()) && focusObject has :onPreviousPage) {
                    isHandledByChild = focusObject.onPreviousPage();
                } else {
                    isHandledByChild = false;
                }
                if (isHandledByChild) {
                    // Child handled input somehow
                    return true;
                } else if (_focus > _getFirstFocusableIndex()) {
                    // Set focus on previous object
                    goToPrevious();
                    return true;
                } else if (_isWrapEnabled) {
                    // Wrap to end
                    goToEnd();
                    return true;
                } else {
                    return false;
                }
            } else {
                // Reset
                goToStart();
                return true;
            }
        }

        function onNextPage() as Boolean {
            if (_focus != null) {
                var focusObject = _children[_focus];
                var isHandledByChild;
                if ((!(focusObject has :isEnabled) || focusObject.isEnabled()) && focusObject has :onNextPage) {
                    isHandledByChild = focusObject.onNextPage();
                } else {
                    isHandledByChild = false;
                }
                if (isHandledByChild) {
                    // Child handled input somehow
                    return true;
                } else if (_focus < _getLastFocusableIndex()) {
                    // Set focus on next item
                    goToNext();
                    return true;
                } else if (_isWrapEnabled) {
                    // Wrap to start
                    goToStart();
                    return true;
                } else {
                    return false;
                }
            } else {
                // Reset
                goToStart();
                return true;
            }
        }

        function onBack() as Boolean {
            if (_focus != null) {
                var focusObject = _children[_focus];
                if ((!(focusObject has :isEnabled) || focusObject.isEnabled()) && focusObject has :onBack) {
                    return focusObject.onBack();
                }
            }
            return false;
        }

        function onTapAt(x as Number, y as Number) as Boolean {
            var tappedChildAndFocus = getChildAt(x, y);
            if (tappedChildAndFocus != null) {
                // User tapped on a menu item
                var tappedChild = tappedChildAndFocus[0];
                var tappedFocus = tappedChildAndFocus[1];
                if (tappedChild.isVisible()) {
                    var hasEffect = false;
                    if (tappedFocus != _focus) {
                        if (_focus != null) {
                            // Handle existing focus
                            var currentFocusObject = _children[_focus];
                            if (currentFocusObject has :onTapElsewhere) {
                                currentFocusObject.onTapElsewhere();
                            }
                        }
                        if (tappedChild has :isFocusable && tappedChild.isFocusable()) {
                            setFocus(tappedFocus);
                        } else {
                            setFocus(null);
                        }
                        hasEffect = true;
                    }
                    if (tappedChild has :onTapAt) {
                        var tappedChildX = _childX[tappedFocus];
                        var tappedChildY = _childY[tappedFocus];
                        hasEffect |= tappedChild.onTapAt(x - tappedChildX, y - tappedChildY);
                    }
                    return hasEffect;
                } else {
                    // This should not happen
                    throw new Codex.ValueError("tapped child not visible");
                }
            } else {
                // User tapped outside any focusable menu item
                if (_focus != null) {
                    // Handle existing focus
                    var currentFocusObject = _children[_focus];
                    if (currentFocusObject has :onTapElsewhere) {
                        currentFocusObject.onTapElsewhere();
                    }
                    setFocus(null);
                    return true;
                } else {
                    // Was already defocused
                    return false;
                }
            }
        }

        function onTapElsewhere() as Void {
            // User tapped outside of menu (usually impossible)
            if (_focus != null) {
                var currentFocusObject = _children[_focus];
                if (currentFocusObject has :onTapElsewhere) {
                    currentFocusObject.onTapElsewhere();
                }
            }
            setIsFocused(false);
        }
    }
}
