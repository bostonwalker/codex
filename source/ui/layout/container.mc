import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    class Container {  // implements Drawable, Loadable, Resizeable, Tappable, MarkdownElement
        /*
        A container for laying out and rendering elements vertically or horizontally
        */
        enum Layout {
            LAYOUT_HORIZONTAL,
            LAYOUT_VERTICAL,
        }

        enum HorizontalAlign {
            HORIZONTAL_ALIGN_LEFT,
            HORIZONTAL_ALIGN_CENTER,
            HORIZONTAL_ALIGN_RIGHT,
        }

        enum VerticalAlign {
            VERTICAL_ALIGN_TOP,
            VERTICAL_ALIGN_CENTER,
            VERTICAL_ALIGN_BOTTOM,
        }

        protected var _children as Array<Drawable>;

        protected var _layout as Layout;
        protected var _width as Number;  // null = unknown
        protected var _height as Number;  // null = unknown
        protected var _spacing as Number;
        protected var _paddingTopBottom as Number;
        protected var _paddingSides as Number;
        protected var _horizontalAlign as HorizontalAlign;
        protected var _verticalAlign as VerticalAlign;
        protected var _isVisible as Boolean;
        protected var _requestReloadCallback as Callback?;

        // Layout state variables
        protected var _isLoaded as Boolean;
        protected var _contentWidth as Number?;
        protected var _contentHeight as Number?;
        protected var _childX as Array<Number>;  // X coordinates to draw fields at
        protected var _childY as Array<Number>;  // X coordinates to draw fields at


        function initialize(contents as Array<Drawable>, theme, options as {
                :layout as Layout, :width as Number, :height as Number,
                :spacing as Number, :padding as Number, :paddingTopBottom as Number, :paddingSides as Number,
                :horizontalAlign as HorizontalAlign, :verticalAlign as VerticalAlign, :isVisible as BooleanRef,
                :requestReloadCallback as Callback}) {
            /*
            :param contents: Elements to layout within container
            :param theme: Styling options
            :param options:
                :layout: Whether container is laid out horizontally or vertically (default: LAYOUT_HORIZONTAL)
                :width: Width of container. If not provided, will be set automatically to the width of its contents.
                :height: Height of container. If not provided, will be set automatically to the height of its contents.
                :spacing: Spacing in between child components (default: 0)
                :padding: Padding on all sides of container. (default: use separate values for top/bottom and sides from theme)
                :paddingTopBottom: Padding on top and bottom of container (default: use theme)
                :paddingSides: Padding on sides of container (default: use theme)
                :horizontalAlign: How to align elements horizontally (default: HORIZONTAL_ALIGN_LEFT)
                :verticalAlign: How to align elements vertically (default: VERTICAL_ALIGN_TOP)
                :isVisible: Whether or not to draw container & contents (default: true)
                :requestReloadCallback: Callback to parent to request reload when this Container changes size
            */
            _children = contents;
            _linkChildren();

            _layout = Utils.getDefault(options, :layout, LAYOUT_HORIZONTAL) as Layout;
            _width = Utils.getDefault(options, :width, null) as Number?;
            _height = Utils.getDefault(options, :height, null) as Number?;
            if (options.hasKey(:padding)) {
                if (options.hasKey(:paddingTopBottom)) {
                    throw new ValueError(
                        "Cannot provide both `padding` and `paddingTopBottom`");
                }
                if (options.hasKey(:paddingSides)) {
                    throw new ValueError(
                        "Cannot provide both `padding` and `paddingSides`");
                }
                var padding = options[:padding];
                _paddingTopBottom = padding;
                _paddingSides = padding;
            } else {
                _paddingTopBottom = Utils.getDefault(options, :paddingTopBottom, theme.layout.containerPaddingTopBottom) as Number;
                _paddingSides = Utils.getDefault(options, :paddingSides, theme.layout.containerPaddingSides) as Number;
            }

            if (options.hasKey(:spacing)) {
                _spacing = options[:spacing];
            } else {
                // Set default spacing depending on direction (horizontal or vertical)
                if (_layout == LAYOUT_HORIZONTAL) {
                    _spacing = theme.layout.containerSpacingHorizontal;
                } else {
                    _spacing = theme.layout.containerSpacingVertical;
                }
            }
            _horizontalAlign = Utils.getDefault(options, :horizontalAlign, HORIZONTAL_ALIGN_LEFT) as HorizontalAlign;
            _verticalAlign = Utils.getDefault(options, :verticalAlign, VERTICAL_ALIGN_TOP) as VerticalAlign;
            _isVisible = Ref.linkBoolean(self, :setIsVisible, Utils.getDefault(options, :isVisible, true) as BooleanRef);
            _requestReloadCallback = Utils.getDefault(options, :requestReloadCallback, null) as Callback?;

            // Vertical layout to be performed when loading
            _isLoaded = false;
            _childX = new [_children.size()];
            _childY = new [_children.size()];
        }

        function setContents(value as Array<Drawable>) {
            _children = value;
            _linkChildren();

            // Hard unload
            requestReload();
            _childX = new [_children.size()];
            _childY = new [_children.size()];
        }

        protected function _linkChildren() as Void {
            // Link children to request parent container reload when necessary
            for (var i = 0; i < _children.size(); i++) {
                var child = _children[i];
                if (child has :setRequestReloadCallback) {
                    child.setRequestReloadCallback(method(:requestReload));
                }
            }
        }

        function getWidth() as Number {
            if (_width != null) {
                return _width;
            } else {
                if (!_isLoaded) {
                    throw new ValueError(
                        "Container must be loaded!");
                }
                return _contentWidth + 2 * _paddingSides;
            }
        }

        function setWidth(value as Number) as Void {
            _width = value;
            requestReload();
        }

        function getHeight() as Number {
            if (_height != null) {
                return _height;
            } else {
                if (!_isLoaded) {
                    throw new ValueError(
                        "Container must be loaded!");
                }
                return _contentHeight + 2 * _paddingTopBottom;
            }
        }

        function setHeight(value as Number) as Void {
            _height = value;
            requestReload();
        }

        function isVisible() as Boolean {
            return _isVisible;
        }

        function setIsVisible(value as BooleanRef) as Void {
            _isVisible = Ref.linkBoolean(self, :setIsVisible, value);
            _requestParentReload();
        }

        function setRequestReloadCallback(value as Callback) as Void {
            _requestReloadCallback = value;
        }

        function load(dc as Graphics.Dc) as Void {
            // Load content, if loadable
            for (var i = 0; i < _children.size(); i++) {
                var child = _children[i];
                if (child has :load) {
                    // Component implements loadable
                    if (child has :isLoaded) {
                        if (child.isLoaded()) {
                            // Child already loaded
                            continue;
                        }
                    }
                    child.load(dc);
                }
            }
            _isLoaded = true;
            _updateLayout();
        }

        function loadYRange(dc as Graphics.Dc, yMin as Number, yMax as Number?) as Void {
            // Possible TODO: implement partial loading
            load(dc);
        }

        function unload() as Void {
            for (var i = 0; i < _children.size(); i++) {
                var child = _children[i];
                if (child has :unload) {
                    // Component implements loadable
                    child.unload();
                }
            }
            _isLoaded = false;
            if (_requestReloadCallback != null) {
                // Call up the chain
                _requestReloadCallback.invoke();
            }
        }

        function isLoaded() as Boolean {
            return _isLoaded;
        }

        function requestReload() as Void {
            _isLoaded = false;
            if (_requestReloadCallback != null) {
                // Call up the chain
                _requestReloadCallback.invoke();
            }
        }

        protected function _requestParentReload() as Void {
            if (_requestReloadCallback != null) {
                // Call up the chain
                _requestReloadCallback.invoke();
            }
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            // Layout components
            if (!_isLoaded) {
                load(dc);
            }

            // Draw components where they were laid out
            for (var i = 0; i < _children.size(); i++) {
                var child = _children[i];
                if (child.isVisible()) {
                    child.draw(dc, x + _childX[i], y + _childY[i]);
                }
            }
        }

        protected function _updateLayout() as Void {
            if (!_isLoaded) {
                throw new Lang.OperationNotAllowedException(
                    "Container not loaded");
            }
            // Layout content
            _contentWidth = 2 * _paddingSides;
            _contentHeight = 2 * _paddingTopBottom;

            // First pass, load and compute content width/height
            for (var i = 0; i < _children.size(); i++) {
                if (i > 0) {
                    // Factor in spacing
                    if (_layout == LAYOUT_HORIZONTAL) {
                        _contentWidth += _spacing;
                    } else {
                        _contentHeight += _spacing;
                    }
                }

                var component = _children[i];

                if (component.isVisible()) {
                    // Only include component in layout if visible
                    var componentWidth = component.getWidth();
                    var componentHeight = component.getHeight();
                    
                    if (_layout == LAYOUT_HORIZONTAL) {
                        // Horizontal layout
                        _contentWidth += componentWidth;
                        if (componentHeight > _contentHeight) {
                            _contentHeight = componentHeight;
                        }
                    } else {
                        // Vertical layout
                        if (componentWidth > _contentWidth) {
                            _contentWidth = componentWidth;
                        }
                        _contentHeight += componentHeight;
                    }
                }
            }

            if (_width != null && _contentWidth > _width) {
                throw new ValueError(
                    "Content too wide for container.\n" +
                    "Container width: " + _width.toString() + "\n" +
                    "Content width (including padding): " + _contentWidth.toNumber());
            }

            if (_height != null && _contentHeight > _height) {
                throw new ValueError(
                    "Content too tall for container.\n" +
                    "Container height: " + _height.toString() + "\n" +
                    "Content height (including padding): " + _contentHeight.toNumber());
            }

            var containerWidth = _width != null ? _width : _contentWidth;
            var containerHeight = _height != null ? _height : _contentHeight;
            
            // Second pass, layout x and y pos
            var currentPos;  // Represents xPos or yPos, depending on layout
            if (_layout == LAYOUT_HORIZONTAL) {
                // Horizontal layout
                if (_horizontalAlign == HORIZONTAL_ALIGN_LEFT) {
                    currentPos = _paddingSides;
                } else if (_horizontalAlign == HORIZONTAL_ALIGN_CENTER) {
                    currentPos = _paddingSides + (containerWidth - _contentWidth) / 2;
                } else {
                    currentPos = _paddingSides + containerWidth - _contentWidth;
                }
            } else {
                // Vertical layout
                if (_verticalAlign == VERTICAL_ALIGN_TOP) {
                    currentPos = _paddingTopBottom;
                } else if (_verticalAlign == VERTICAL_ALIGN_CENTER) {
                    currentPos = _paddingTopBottom + (containerHeight - _contentHeight) / 2;
                } else {
                    currentPos = _paddingTopBottom + containerHeight - _contentHeight;
                }
            }

            for (var i = 0; i < _children.size(); i++) {

                var component = _children[i];

                var componentWidth;
                var componentHeight;

                if (component.isVisible()) {
                    if (i > 0) {
                        currentPos += _spacing;
                    }
                    componentWidth = component.getWidth();
                    componentHeight = component.getHeight();
                } else {
                    componentWidth = 0;
                    componentHeight = 0;
                }

                if (_layout == LAYOUT_HORIZONTAL) {
                    // Horizontal layout
                    _childX[i] = currentPos;
                    if (_verticalAlign == VERTICAL_ALIGN_TOP) {
                        _childY[i] = _paddingTopBottom;
                    } else if (_verticalAlign == VERTICAL_ALIGN_CENTER) {
                        _childY[i] = _paddingTopBottom + (containerHeight - componentHeight) / 2;
                    } else {
                        _childY[i] = _paddingTopBottom + containerHeight - componentHeight;
                    }
                    currentPos += componentWidth;
                } else {
                    // Vertical layout
                    if (_horizontalAlign == HORIZONTAL_ALIGN_LEFT) {
                        _childX[i] = _paddingSides;
                    } else if (_horizontalAlign == HORIZONTAL_ALIGN_CENTER) {
                        _childX[i] = _paddingSides + (containerWidth - componentWidth) / 2;
                    } else {
                        _childX[i] = _paddingSides + containerWidth - componentWidth;
                    }
                    _childY[i] = currentPos;
                    currentPos += componentHeight;
                }
            }
        }

        function getChildAt(x as Number, y as Number) as [Drawable, Number]? {
            // Get index of tapped on item
            // x and y are relative coords within the view
            if (_layout == LAYOUT_HORIZONTAL) {
                // Horizontal layout, scan x
                for (var i = 0; i < _children.size(); i++) {
                    if (x < _childX[i]) {
                        // x has been passed already without a hit
                        return null;
                    }
                    var child = _children[i];
                    if (x < _childX[i] + child.getWidth()) {
                        // x in range, test y
                        if (y >= _childY[i] && y < _childY[i] + child.getHeight()) {
                            // y in range
                            return [child, i];
                        } else {
                            // y out of range
                            return null;
                        }
                    }
                }
            } else {
                // Vertical layout, scan y
                for (var i = 0; i < _children.size(); i++) {
                    if (y < _childY[i]) {
                        // y has been passed already without a hit
                        return null;
                    }
                    var child = _children[i];
                    if (y < _childY[i] + child.getHeight()) {
                        // y in range, test x
                        if (x >= _childX[i] && x < _childX[i] + child.getWidth()) {
                            // x in range
                            return [child, i];
                        } else {
                            // x out of range
                            return null;
                        }
                    }
                }
            }
            return null;
        }

        function getChildPos(index as Number) as [Number, Number] {
            // Return x and y position of child component
            return [_childX[index], _childY[index]];
        }
    }
}
