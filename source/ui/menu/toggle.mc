import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    class Toggle {  // implements MenuItem

        protected var _callback as BooleanCallback;

        protected var _isVisible as Boolean;
        protected var _isEnabled as Boolean;
        protected var _isFocused as Boolean;
        protected var _value as BooleanRef;

        protected var _text as TextElement;
        protected var _textFocused as TextElement;
        protected var _topBottomPadding as Number;
        protected var _topBottomPaddingFocused as Number;
        protected var _toggleKnobRadius as Number;
        protected var _toggleKnobRadiusFocused as Number;
        protected var _toggleKnobTravel as Number;
        protected var _toggleKnobTravelFocused as Number;

        protected var _textColor as Number;
        protected var _textColorDisabled as Number;
        protected var _textColorFocused as Number;
        protected var _textColorFocusedDisabled as Number;
        protected var _backgroundColor;
        protected var _backgroundColorFocused;
        protected var _borderColor;
        protected var _toggleKnobColor;
        protected var _toggleOutlineColor;
        protected var _toggleOutlineColorFocused;
        protected var _toggleBackgroundColor;
        protected var _toggleBackgroundColorToggled;

        protected var _width as Number;
        protected var _xText as Number;
        protected var _xToggleCenter as Number; 

        function initialize(text as String or ResourceId, callback as BooleanCallback?, theme as Codex.Theme,
                options as {:isVisible as BooleanRef, :isEnabled as BooleanRef, :isFocused as BooleanRef, :value as BooleanRef}) {
            /*
            :param callback: Method to be called with new value when state of toggle is changed
            :param options:
                :isVisible: Whether Toggle is visible (default: true)
                :isEnabled: Whether Toggle is enabled (default: true)
                :isFocused: Whether Toggle is focused (default: false)
                :value: Toggle value (default: false/off). If passed, a reactive Ref can be linked here.
            }
            */
            _callback = callback;

            _isVisible = Ref.linkBoolean(self, :setIsVisible, Utils.getDefault(options, :isVisible, true) as BooleanRef);
            if (options.hasKey(:isEnabled)) {
                _isEnabled = Ref.linkBoolean(self, :setIsEnabled, options[:isEnabled]);
                if (callback == null && _isEnabled) {
                    throw new ValueError(
                        "Toggle cannot be enabled if `callback` is null");
                }
            } else if (callback != null) {
                _isEnabled = true;
            } else {
                _isEnabled = false;
            }
            _isFocused = Ref.linkBoolean(self, :setIsFocused, Utils.getDefault(options, :isFocused, false) as BooleanRef);
            // Keep value as Ref if passed as Ref
            _value = Utils.getDefault(options, :value, false) as Boolean or Ref;

            var screenWidth = System.getDeviceSettings().screenWidth;
            var sidePadding = theme.menu.itemSidePadding;
            var toggleKnobRadius = theme.menu.toggleKnobRadius;
            var toggleKnobTravel = theme.menu.toggleKnobTravel;
            var toggleSidePadding = theme.menu.toggleSidePadding;

            var textWidth = screenWidth - Utils.multiplyCeiling(
                toggleKnobTravel + 2 * toggleKnobRadius + 2 * sidePadding + toggleSidePadding,
                theme.menu.focusFactor);

            _text = new Codex.TextElement(
                text,
                theme.menu.itemFont,
                theme.color.text,
                theme.color.background,
                {
                    :width => textWidth,
                    :justification => Graphics.TEXT_JUSTIFY_CENTER,
                }
            );
            _textFocused = new Codex.TextElement(
                text,
                theme.menu.itemFontFocused,
                theme.color.background,
                theme.color.text,
                {
                    :width => textWidth,
                    :justification => Graphics.TEXT_JUSTIFY_CENTER,
                }
            );

            _topBottomPadding = theme.menu.itemPadding;
            _topBottomPaddingFocused = Utils.multiplyCeiling(theme.menu.itemPadding, theme.menu.focusFactor);
            _toggleKnobRadius = theme.menu.toggleKnobRadius;
            _toggleKnobRadiusFocused = Utils.multiplyCeiling(theme.menu.toggleKnobRadius, theme.menu.focusFactor);
            _toggleKnobTravel = theme.menu.toggleKnobTravel;
            _toggleKnobTravelFocused = Utils.multiplyCeiling(theme.menu.toggleKnobTravel, theme.menu.focusFactor);  
            
            _textColor = theme.color.text;
            _textColorDisabled = theme.color.majorAccent;
            _textColorFocused = theme.color.background;
            _textColorFocusedDisabled = theme.color.minorAccent;
            _backgroundColor = theme.color.background;
            _backgroundColorFocused = theme.color.text;
            _borderColor = theme.color.majorAccent;
            _toggleKnobColor = theme.color.text;
            _toggleOutlineColor = theme.color.majorAccent;
            _toggleOutlineColorFocused = theme.color.minorAccent;
            _toggleBackgroundColor = theme.color.background;
            _toggleBackgroundColorToggled = theme.color.indicatorEnabled;

            _updateTextColor();

            _width = screenWidth;
            _xText = sidePadding;
            _xToggleCenter = textWidth + 2 * sidePadding + toggleKnobRadius + toggleKnobTravel / 2;  
        }

        function getWidth() as Number {
            return _width;
        }

        function getHeight() as Number {
            // Get height of this menu item (setIsFocused() will always be called first)
            if (_isFocused) {
                return _textFocused.getHeight() + 2 * _topBottomPaddingFocused;
            } else {
                return _text.getHeight() + 2 * _topBottomPadding;
            }
        }

        function isVisible() as Boolean {
            // Get whether this menu item is visible
            return _isVisible;
        }

        function setIsVisible(value as BooleanRef) as Void {
            // Set whether this menu item is visible
            _isVisible = Ref.linkBoolean(self, :setIsVisible, value);
        }

        function isEnabled() as Boolean {
            // Get whether this menu item is enabled
            return _isEnabled;
        }

        function setIsEnabled(value as BooleanRef) as Void {
            _isEnabled = Ref.linkBoolean(self, :setIsEnabled, value);
            if (_callback == null && _isEnabled) {
                throw new ValueError(
                    "MenuItem cannot be enabled if `intent` is null");
            }
            _updateTextColor();
        }

        function isFocusable() as Boolean {
            return _isEnabled;
        }

        function isFocused() as Boolean {
            // Get whether this menu item is focused
            return _isFocused;
        }

        function setIsFocused(value as BooleanRef) as Void {
            // Set whether this menu item is focused
            _isFocused = Ref.linkBoolean(self, :setIsFocused, value);
        }

        function getValue() as Boolean {
            if (_value instanceof Ref) {
                return _value.value() as Boolean;
            } else {
                return _value;
            }
        }

        function setValue(value as Boolean) as Void {
            if (_value instanceof Ref) {
                throw new Lang.OperationNotAllowedException(
                    "Value is reactive and cannot be set");
            }
            _value = value;
        }

        function load(dc as Graphics.Dc) as Void {
            // Load this menu item with the device context
            _text.load(dc);
            _textFocused.load(dc);
        }

        function unload() as Void {
            _text.unload();
            _textFocused.unload();
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            // Draw this menu item with top-left corner at absolute screen position

            var isToggled = getValue();
            
            // Styling
            var backgroundColor = _isFocused ? _backgroundColorFocused : _backgroundColor;
            var toggleOutlineColor = _isFocused ? _toggleOutlineColorFocused : _toggleOutlineColor;
            var toggleBackgroundColor = isToggled ? _toggleBackgroundColorToggled : _toggleBackgroundColor;
            var toggleKnobRadius = _isFocused ? _toggleKnobRadiusFocused : _toggleKnobRadius;
            var toggleKnobTravel = _isFocused ? _toggleKnobTravelFocused : _toggleKnobTravel;

            var height = getHeight();

            if (_isFocused) {
                // Fill background
                dc.setColor(backgroundColor, backgroundColor);
                dc.fillRectangle(x, y, _width, height);

                // Draw text lines
                _textFocused.draw(dc, x + _xText, y + _topBottomPaddingFocused);
            } else {
                // Draw text
                _text.draw(dc, x + _xText, y + _topBottomPadding);

                // Draw bottom border
                dc.setColor(_borderColor, backgroundColor);
                dc.setPenWidth(1);
                dc.drawLine(x, y + height, _width, y + height);
            }
            
            // Draw toggle
            var yMid = y + height / 2;
            var xToggleLeftCircle = x + _xToggleCenter - toggleKnobTravel / 2;
            var xToggleRightCircle = x + _xToggleCenter + toggleKnobTravel / 2;
            var yToggleTop = yMid - toggleKnobRadius;
            var yToggleBottom = yMid + toggleKnobRadius;

            // Draw toggle background
            dc.setColor(toggleBackgroundColor, backgroundColor);
            if (isToggled) {
                dc.fillCircle(xToggleLeftCircle, yMid, toggleKnobRadius);
            } else {
                dc.fillCircle(xToggleRightCircle, yMid, toggleKnobRadius);
            }
            dc.fillRectangle(xToggleLeftCircle, yToggleTop, toggleKnobTravel, 2 * toggleKnobRadius);

            // Draw toggle outline
            dc.setPenWidth(1);
            dc.setColor(toggleOutlineColor, backgroundColor);
            if (isToggled) {
                dc.drawArc(xToggleLeftCircle, yMid, toggleKnobRadius, Graphics.ARC_COUNTER_CLOCKWISE, 90, 270);
            } else {
                dc.drawArc(xToggleRightCircle, yMid, toggleKnobRadius, Graphics.ARC_COUNTER_CLOCKWISE, 270, 90);
            }
            dc.drawLine(xToggleLeftCircle, yToggleTop, xToggleRightCircle, yToggleTop);
            dc.drawLine(xToggleLeftCircle, yToggleBottom, xToggleRightCircle, yToggleBottom);

            // Draw toggle knob
            dc.setColor(_toggleKnobColor, backgroundColor);
            dc.fillCircle(isToggled ? xToggleRightCircle : xToggleLeftCircle, yMid, toggleKnobRadius);
            dc.setColor(toggleOutlineColor, backgroundColor);
            dc.drawCircle(isToggled ? xToggleRightCircle : xToggleLeftCircle, yMid, toggleKnobRadius);
        }

        private function _updateTextColor() as Void {
            // Update text color when enabling/disabling menu item
            _text.setTextColor(_isEnabled ? _textColor : _textColorDisabled);
            _textFocused.setTextColor(_isEnabled ? _textColorFocused : _textColorFocusedDisabled);
        }

        function toggle() as Boolean {
            // Flip value and invoke callback
            var newValue = !getValue();
            _callback.invoke(newValue);
            if (!(_value instanceof Ref)) {
                // Toggle value is reactive - rely on callback to affect new value
                setValue(newValue);
            }
            return true;
        }
        
        function onEnter() as Boolean {
            // Select this menu item
            return toggle();
        }

        function onPreviousPage() as Boolean {
            // Not handled
            return false;
        }

        function onNextPage() as Boolean {
            // Not handled
            return false;
        }

        function onBack() as Boolean {
            // Not handled
            return false;
        }
        
        function onTapAt(x as Number, y as Number) as Boolean {
            // Press on this menu item
            return toggle();
        }

        function onTapElsewhere() as Void {
            // No action required
        }
    }
}