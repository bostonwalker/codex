import Toybox.Lang;
using Toybox.Graphics;
using Toybox.System;


module Codex {

    class _MenuOptionText extends TextElement {

        protected var _isEnabled as Boolean;
        protected var _isFocused as Boolean;

        protected var _fontNormal as Graphics.FontDefinition or Graphics.FontReference;
        protected var _fontFocused as Graphics.FontDefinition or Graphics.FontReference;
        
        protected var _textColorEnabled as Number;
        protected var _textColorDisabled as Number;
        protected var _textColorFocusedEnabled as Number;
        protected var _textColorFocusedDisabled as Number;

        function initialize(text as String or ResourceId, theme as Theme, options as {
                :isEnabled as Boolean, :isFocused as Boolean, :font as Graphics.FontReference or Graphics.FontDefinition,
                :focusFont as Graphics.FontReference or Graphics.FontDefinition}) {

            _isEnabled = Utils.getDefault(options, :isEnabled, true) as Boolean;
            _isFocused = Utils.getDefault(options, :isFocused, true) as Boolean;

            _fontNormal = Utils.getDefault(options, :font, theme.menu.itemFont) as Graphics.FontReference or Graphics.FontDefinition;
            _fontFocused = Utils.getDefault(options, :focusFont, theme.menu.itemFontFocused) as Graphics.FontReference or Graphics.FontDefinition;;

            _textColorEnabled = theme.color.text;
            _textColorDisabled = theme.color.majorAccent;
            _textColorFocusedEnabled = theme.color.background;
            _textColorFocusedDisabled = theme.color.minorAccent;

            var screenWidth = System.getDeviceSettings().screenWidth;
            var textWidth = screenWidth - 2 * theme.menu.itemSidePadding;

            if (!options.hasKey(:multiline)) {
                options[:multiline] = false;
            }
            options[:maxWidth] = textWidth;
            if (!options.hasKey(:justification)) {
                options[:justification] = Graphics.TEXT_JUSTIFY_CENTER;
            }

            TextElement.initialize(text, _getFont(), _getTextColor(), Graphics.COLOR_TRANSPARENT, options);
        }

        function setIsEnabled(value as Boolean) as Void {
            _isEnabled = value;
            setTextColor(_getTextColor());
        }

        function setIsFocused(value as Boolean) as Void {
            _isFocused = value;
            setFont(_getFont());
            setTextColor(_getTextColor());
        }

        protected function _getFont() as Graphics.FontDefinition or Graphics.FontReference {
            if (_isFocused) {
                return _fontFocused;
            } else {
                return _fontNormal;
            }
        }

        protected function _getTextColor() as Number {
            if (_isFocused) {
                return _isEnabled ? _textColorFocusedEnabled : _textColorFocusedDisabled;
            } else {
                return _isEnabled ? _textColorEnabled : _textColorDisabled;
            }
        }
    }

    class TextMenuOption extends MenuOption {

        protected var _text as _MenuOptionText;

        function initialize(text as String or ResourceId, intent as Intent?, theme as Theme, options as {
                :isVisible as BooleanRef, :isEnabled as BooleanRef, :isFocused as BooleanRef,
                :drawTopBorder as Boolean, :drawBottomBorder as Boolean,
                :multiline as Boolean, :font as Graphics.FontReference or Graphics.FontDefinition,
                :focusFont as Graphics.FontReference or Graphics.FontDefinition, :requestReloadCallback as Callback
        }) {
            /*
            A simple menu item that displays text and returns a view (either a sub-menu or a markdown page) when selected

            :param text: Text to show. Can be a string or resource symbol of a string to load.
            :param intent: Intent to load when selected
                [WatchUi.View] OR [WatchUi.View, WatchUi.InputDelegate] OR 
                Dictionary containing :method and :options, i.e. a factory OR 
                Callback OR
                null -> an unselectable menu item
            :param theme: Styling options
            :param options:
                :isVisible: Whether MenuItem is visible (default: true)
                :isEnabled: Whether MenuItem is enabled (default: true if `intent` is not Null). Cannot be true if `intent` is Null
                :isFocused: Whether MenuItem is focused (default: false)
                :drawTopBorder: If true, drop top border as well as bottom border (default: false)
                :drawBottomBorder: If true, drop buttom border (default: true)
                :multiline: Whether to allow multiline text (default: false)
                :font: Text font (default to theme)
                :focusFont: Text font when focused (default to theme)
                :requestReloadCallback: Callback to parent to request reload when this MenuOption changes size
            */
            var isCurrentlyEnabled;
            if (options.hasKey(:isEnabled)) {
                var isEnabled = options[:isEnabled];
                if (isEnabled instanceof Ref) {
                    isCurrentlyEnabled = isEnabled.value();
                } else {
                    isCurrentlyEnabled = isEnabled;
                }
            } else if (intent != null) {
                isCurrentlyEnabled = true;
            } else {
                isCurrentlyEnabled = false;
            }

            var isCurrentlyFocused = Utils.getDefault(options, :isFocused, false) as BooleanRef;
            if (isCurrentlyFocused instanceof Ref) {
                isCurrentlyFocused = isCurrentlyFocused.value();
            }

            var textOptions =  {
                :isEnabled => isCurrentlyEnabled,
                :isFocused => isCurrentlyFocused,
            };
            if (options.hasKey(:multiline)) {
                textOptions[:multiline] = options[:multiline];
            }
            if (options.hasKey(:font)) {
                textOptions[:font] = options[:font];
            }
            if (options.hasKey(:focusFont)) {
                textOptions[:focusFont] = options[:focusFont];
            }
            _text = new _MenuOptionText(text, theme, textOptions);

            MenuOption.initialize(_text, intent, theme, options);
        }

        function setText(text as String or ResourceId) as Void {
            // Update text of this MenuOption
            text = Utils.resolveString(text);
            _text.setStr(text);
            // Need to reload text element and layout
            _requestReload();
        }

        function setIsEnabled(value as BooleanRef) as Void {
            MenuOption.setIsEnabled(value);
            _text.setIsEnabled(_isEnabled);
        }

        function setIsFocused(value as BooleanRef) as Void {
            MenuOption.setIsFocused(value);
            _text.setIsFocused(_isFocused);
            // Need to reload text element and layout
            _requestReload();
        }
    }
}