import Toybox.Lang;
using Toybox.Graphics;
using Toybox.Attention;


module Codex {

    class TextMenuButton extends TextMenuOption {

        protected var _tone as Attention.Tone?;

        function initialize(text as String or ResourceId, intent as Intent?, theme as Theme, options as {
                :isVisible as BooleanRef, :isEnabled as BooleanRef, :isFocused as BooleanRef,
                :font as Graphics.FontReference or Graphics.FontDefinition,
        }) {
            /*
            A simple button that displays text and performs an action when selected. Implemented as a MenuItem.

            :param text: Text to show. Can be a string or resource symbol of a string to load.
            :param intent: Intent to load when selected
                [WatchUi.View] OR [WatchUi.View, WatchUi.InputDelegate] OR 
                Dictionary containing :method and :options, i.e. a factory OR 
                Callback OR
                null -> an unselectable button
            :param theme: Styling options
            :param options: MenuItem options, plus:
                :tone: Tone to play when button is pressed(default: Attention.TONE_KEY)
            */
            _tone = Utils.getDefault(options, :tone, Attention has :playTone ? Attention.TONE_KEY : null) as Attention.Tone?;

            if (!options.hasKey(:drawTopBorder)) {
                options[:drawTopBorder] = true;
            }
            if (!options.hasKey(:drawBottomBorder)) {
                options[:drawBottomBorder] = true;
            }

            TextMenuOption.initialize(text, intent, theme, options);
        }

        function select() as Boolean {
            _playTone();
            return MenuOption.select();
        }

        function _playTone() {
            // Play tone if device supports
            if (_tone != null) {
                Attention.playTone(_tone);
            }
        }
    }
}