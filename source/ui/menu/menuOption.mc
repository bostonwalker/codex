import Toybox.Lang;
using Toybox.Graphics;
using Toybox.System;


module Codex {

    typedef MenuOptionContent as interface {
        // Content functionality necessary for display within a menu option
        /* Enable / disable */
        function setIsEnabled(value as Boolean) as Void;
        /* Focusable (partial implementation) */
        function setIsFocused(value as Boolean) as Void;
        /* Loadable (optional) */
        // function load(dc as Graphics.Dc) as Void;
        // function unload() as Void;
        /* Drawable (partial implementation) */
        function getWidth() as Number;
        function getHeight() as Number;
        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void;
    };

    class MenuOption {  // implements MenuItem

        protected var _content as MenuOptionContent;
        protected var _intent as Intent?;

        protected var _isVisible as Boolean;
        protected var _isEnabled as Boolean;
        protected var _isFocused as Boolean;
        protected var _drawTopBorder as Boolean;
        protected var _drawBottomBorder as Boolean;
        protected var _requestReloadCallback as Callback?;

        protected var _backgroundColor as Number;
        protected var _backgroundColorFocused as Number;
        protected var _borderColor as Number;
        protected var _topBottomPadding as Number;
        protected var _topBottomPaddingFocused as Number;

        protected var _width as Number;

        // Layout state variables
        protected var _contentX as Number?;
        protected var _isLoaded = false;

        function initialize(content as MenuOptionContent, intent as Intent?, theme as Theme, options as {
                :isVisible as BooleanRef, :isEnabled as BooleanRef, :isFocused as BooleanRef,
                :drawTopBorder as Boolean, :drawBottomBorder as Boolean, :requestReloadCallback as Callback
        }) {
            /*
            A simple menu item that displays some type of content and returns a view (either a sub-menu or a markdown page) when selected

            :param content: Content to show. See `TextMenuOption` and `_MenuOptionText` for a simple implementation.
            :param intent: Intent to load when selected
                [WatchUi.View] OR [WatchUi.View, WatchUi.InputDelegate] OR 
                Dictionary containing :method and :options, i.e. a factory OR 
                Callback OR
                null -> an unselectable menu item
            :param theme: Styling options
            :param options:
                :isVisible: Whether MenuItem is visible (default: true)
                :isEnabled: Whether MenuItem is enabled (default: true if `intent` is not Null). Cannot be true if `intent` is Null
                :isFocused: Whether MenuItem is focused (default: false)
                :drawTopBorder: If true, drop top border as well as bottom border (default: false)
                :drawBottomBorder: If true, drop buttom border (default: true)
                :requestReloadCallback: Callback to parent to request reload when this MenuOption changes size
            */
            _content = content;
            _intent = intent;

            _isVisible = Ref.linkBoolean(self, :setIsVisible, Utils.getDefault(options, :isVisible, true) as BooleanRef);
            if (options.hasKey(:isEnabled)) {
                _isEnabled = Ref.linkBoolean(self, :setIsEnabled, options[:isEnabled]);
                if (intent == null && _isEnabled) {
                    throw new ValueError(
                        "MenuItem cannot be enabled if `intent` is null");
                }
            } else if (intent != null) {
                _isEnabled = true;
            } else {
                _isEnabled = false;
            }
            _isFocused = Ref.linkBoolean(self, :setIsFocused, Utils.getDefault(options, :isFocused, false) as BooleanRef);
            _drawTopBorder = Utils.getDefault(options, :drawTopBorder, false) as Boolean;
            _drawBottomBorder = Utils.getDefault(options, :drawBottomBorder, true) as Boolean;
            _requestReloadCallback = Utils.getDefault(options, :requestReloadCallback, null) as Callback?;

            _width = System.getDeviceSettings().screenWidth;

            _topBottomPadding = theme.menu.itemPadding;
            _topBottomPaddingFocused = Utils.multiplyCeiling(theme.menu.itemPadding, theme.menu.focusFactor);
            
            _backgroundColor = theme.color.background;
            _backgroundColorFocused = theme.color.text;
            _borderColor = theme.color.majorAccent;
            
            _isLoaded = false;
        }

        function isEnabled() as Boolean {
            // Get whether this menu item is enabled
            return _isEnabled;
        }

        function setIsEnabled(value as BooleanRef) as Void {
            _isEnabled = Ref.linkBoolean(self, :setIsEnabled, value);
            if (_intent == null && _isEnabled) {
                throw new ValueError(
                    "MenuItem cannot be enabled if `intent` is null");
            }
        }

        /* Focusable */
        function isFocusable() as Boolean {
            return true;
        }

        function isFocused() as Boolean {
            // Get whether this menu item is focused
            return _isFocused;
        }

        function setIsFocused(value as BooleanRef) as Void {
            // Set whether this menu item is focused
            _isFocused = Ref.linkBoolean(self, :setIsFocused, value);
        }

        /* Loadable */
        function load(dc as Graphics.Dc) as Void {
            // Load wrapped strings
            if (_content has :load) {
                // Content implements loadable
                _content.load(dc);
            }
            _contentX = (_width - _content.getWidth()) / 2;
            _isLoaded = true;
        }

        function unload() as Void {
            if (_content has :unload) {
                // Content implements loadable
                _content.unload();
            }
            _isLoaded = false;
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        function setRequestReloadCallback(value as Callback) as Void {
            _requestReloadCallback = value;
        }

        protected function _requestReload() as Void {
            _isLoaded = false;
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        protected function _requestParentReload() as Void {
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        /* Drawable */
        function getWidth() as Number {
            return _width;
        }

        function getHeight() as Number {
            // Get height of this menu item
            return _content.getHeight() + 2 * _topBottomPadding;
        }

        function isVisible() as Boolean {
            // Get whether this menu item is visible
            return _isVisible;
        }

        function setIsVisible(value as BooleanRef) as Void {
            // Set whether this menu item is visible
            _isVisible = Ref.linkBoolean(self, :setIsVisible, value);
            _requestParentReload();
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            // Draw this menu item with top-left corner at absolute screen position
            if (!_isLoaded) {
                load(dc);
            }

            var height = getHeight();

            if (_isFocused) {
                // Fill background
                dc.setColor(_backgroundColorFocused, _backgroundColor);
                dc.fillRectangle(x, y, _width, height + 1);

                // Draw content
                _content.draw(dc, _contentX, y + _topBottomPaddingFocused);
            } else {
                // Draw top border
                if (_drawTopBorder) {
                    dc.setColor(_borderColor, _backgroundColor);
                    dc.setPenWidth(1);
                    dc.drawLine(0, y, _width, y);
                }

                // Draw content
                _content.draw(dc, _contentX, y + _topBottomPadding);

                // Draw bottom border
                if (_drawBottomBorder) {
                    dc.setColor(_borderColor, _backgroundColor);
                    dc.setPenWidth(1);
                    dc.drawLine(0, y + height, _width, y + height);
                }
            }
        }

        /* Controllable */
        function select() as Boolean {
            return getApp().executeIntent(_intent);
        }

        function onEnter() as Boolean {
            // Press on this menu item
            if (_isEnabled) {
                return select();
            }
            return false;
        }

        function onPreviousPage() as Boolean {
            // Not handled
            return false;
        }

        function onNextPage() as Boolean {
            // Not handled
            return false;
        }

        function onBack() as Boolean {
            // Not handled
            return false;
        }
        
        function onTapAt(x as Number, y as Number) as Boolean {
            // Press on this menu item
            if (_isEnabled) {
                return select();
            }
            return false;
        }

        function onTapElsewhere() as Void {
            // No action required
        }
    }
}