import Toybox.Lang;
using Toybox.System;


module Codex {

    class ConfirmDialog extends Menu {
        // Toast that you have to agree to to perform an action
        // Used for exit nav, zeroize, etc.

        protected var _intent as Intent;

        function initialize(text as String or ResourceId, intent as Intent, theme as Theme, options as {
                :confirmText as String or ResourceId, :cancelText as String or ResourceId, :showConfirmFirst as Boolean,
                :height as Number}) {
            /*
            :param text: Text to display to user (e.g. "Really do this thing?")
            :param intent: Intent to execute on confirmation
            :param options: 
                :confirmText: Text to display for option to confirm action (default: "Yes")
                :cancelText: Text to display for menu option to cancel action (default: "Cancel")
                :showConfirmFirst: If true, show the confirm option first. If false, show the cancel option first (default: true).
                :height: Height of confirm dialog (default: screen height)
            */
            _intent = intent;

            var confirmText = Utils.getDefault(options, :confirmText, Rez.Strings.Codex_ConfirmDialog_Confirm) as String or ResourceId;
            var cancelText = Utils.getDefault(options, :cancelText, Rez.Strings.Codex_ConfirmDialog_Cancel) as String or ResourceId;
            var showConfirmFirst = Utils.getDefault(options, :showConfirmFirst, true) as Boolean;
            var height = Utils.getDefault(options, :height, System.getDeviceSettings().screenHeight) as Number;

            var title = new TextMenuTitle(text, theme, {});
            var cancelMenuItem = new TextMenuOption(cancelText, method(:_cancel), theme, {});
            var confirmMenuItem = new TextMenuOption(confirmText, method(:_confirm), theme, {});

            Menu.initialize(
                title,
                [
                    showConfirmFirst ? confirmMenuItem : cancelMenuItem,
                    showConfirmFirst ? cancelMenuItem : confirmMenuItem,
                ],
                theme,
                {
                    :height => height,
                    :verticalAlign => Container.VERTICAL_ALIGN_CENTER,
                    :scrollMode => Menu.SCROLL_MODE_FIXED,
                }
            );
        }

        function _confirm() as Void {
            // Exit dialog and execute intent
            var app = getApp();
            app.exitView();
            app.executeIntent(_intent);
        }

        function _cancel() as Void {
            // Just exit dialog
            getApp().exitView();
        }

        static function launch(text as String or ResourceId, intent as Intent, theme as Theme, options as Dictionary) as Void {
            // Launch a new instance of this view from any context
            var view = new ConfirmDialog(text, intent, theme, options);
            var delegate = new ControllableDelegate(view);
            getApp().selectView([view, delegate]);
        }
    }
}
