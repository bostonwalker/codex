import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    class TextMenuTitle extends TextMenuOption {  // implements MenuItem
        /* A disabled, unfocusable MenuOption */
        function initialize(text as String or ResourceId or Null, theme as Theme, options as {
                :isVisible as BooleanRef, :drawTopBorder as Boolean, :drawBottomBorder as Boolean,
                :font as Graphics.FontReference or Graphics.FontDefinition, :multiline as Boolean,
        }) {
            /*
            :param text: Title text to show
            :param theme: Styling options
            :param options:
                :isVisible: Whether MenuItem is visible (default: true)
                :drawTopBorder: If true, drop top border as well as bottom border (default: false)
                :drawBottomBorder: If true, drop buttom border (default: true)
                :font: Text font (default to theme)
                :multiline: Whether to allow multiline text (default: false)
            */
            TextMenuOption.initialize(text, null, theme, options);
        }

        function isFocusable() as Boolean {
            return false;
        }
    }
}
