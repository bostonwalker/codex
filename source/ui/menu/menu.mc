import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Lang;
import Toybox.System;


module Codex {

    typedef MenuItem as interface {  // implements Drawable (optional: Loadable, Focusable, Scrollable, Controllable)
        /*
        Menu item functionality needed to work with ScrollContainer
        */
        // (optional) function isEnabled() as Boolean;
        /* Focusable (optional) */
        // function isFocusable() as Boolean;
        // function isFocused() as Boolean;
        // function setIsFocused(value as BooleanRef) as Void;
        /* Loadable (optional) */
        // function load(dc as Graphics.Dc) as Void;
        // function unload() as Void;
        /* Drawable */
        function getWidth() as Number;
        function getHeight() as Number;
        function isVisible() as Boolean;
        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void;
        /* Scrollable (optional) */
        // function goToStart() as Void;
        // function goToEnd() as Void;
        // function goToNext() as Void;
        // function goToPrevious() as Void;
        // function isAtStart() as Boolean;
        // function isAtEnd() as Boolean;
        /* Controllable (optional) */
        // function onEnter() as Boolean;
        // function onPreviousPage() as Boolean;
        // function onNextPage() as Boolean;
        // function onBack() as Boolean;
        // function onTapAt(x as Number, y as Number) as Boolean;
        // function onTapElsewhere() as Void;
    };

    class Menu extends ScrollContainerView {

        enum ScrollMode {
            SCROLL_MODE_SCROLL,  // Scroll menu content so focused item is always in same place
            SCROLL_MODE_FIXED,  // Do not scroll menu content
        }

        protected var _title as MenuItem?;

        protected var _scrollMode as ScrollMode;
        protected var _containerLocY as Number;

        function initialize(title as MenuItem?, items as Array<MenuItem>, theme as Theme, options as {
            :locY as Number, :height as Number, :verticalAlign as Container.VerticalAlign, :scrollMode as ScrollMode,
            :isFocused as BooleanRef, :focus as NumberRef, :isWrapEnabled as Boolean}) {
            /*
            Base menu class

            Handles selecting elements and pushing and popping views from the stack

            :param title: Menu title (should not be focusable)
            :param items: Array of menu items
            :param theme: Theme
            :param options:
                :locY: y coordinate of origin
                :height: Menu height (default: fit contents)
                :verticalAlign: Vertical alignment of menu (default: Container.VERTICAL_ALIGN_TOP)
                :scrollMode: Controls how content is repositioned when menu focus changes (default: Menu.SCROLL_MODE_SCROLL)
                :isFocused: Whether menu is focused (default: true)
                :focus: Focus of menu
                :isWrapEnabled: If true, wrap to bottom when scrolling up from top and vice versa (default: true)
            */
            _title = title;

            _scrollMode = Utils.getDefault(options, :scrollMode, SCROLL_MODE_SCROLL) as ScrollMode;

            _containerLocY = 0;

            var content = [] as Array<Drawable>;
            if (title != null) {
                // Add title, if provided
                content.add(title);
            }
            content.addAll(items as Array<Drawable>);  // redundant cast, bug with type checker

            options[:layout] = Container.LAYOUT_VERTICAL;
            if (!options.hasKey(:verticalAlign)) {
                options[:verticalAlign] = Container.VERTICAL_ALIGN_TOP;
            }
            options[:horizontalAlign] = Container.HORIZONTAL_ALIGN_CENTER;
            options[:backgroundColor] = theme.color.background;
            options[:spacing] = 0;

            ScrollContainerView.initialize(content, theme, options);
        }

        function setTitle(value as MenuItem?) as Void {
            _title = value;
        }

        function setContents(value as Array<Drawable>) as Void {
            var menuContents = [] as Array<Drawable>;
            if (_title != null) {
                menuContents.add(_title);
            }
            menuContents.addAll(value as Array<Drawable>);
            _container.setContents(menuContents);
        }

        function setHeight(value as Number) as Void {
            // Do nothing, keep height undefined
        }

        /* WatchUi.View */
        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            if (!_container.isLoaded()) {
                _container.load(dc);
            }
            // Compute and adjust for menu scroll
            _containerLocY = _calculateContainerLocY();
            ScrollContainerView.draw(dc, x, y + _containerLocY);
        }

        /* Codex.Controllable */
        function onTapAt(x as Number, y as Number) as Boolean {
            // Adjust for menu scroll
            return _container.onTapAt(x, y - _containerLocY);
        }

        protected function _calculateContainerLocY() as Number {
            // Shift menu down to focus current item
            var focus = _container.getFocus();
            if (_scrollMode == SCROLL_MODE_SCROLL && focus != null) {
                var focusedItemPos = _container.getChildPos(focus);
                var y = -focusedItemPos[1];
                if (_title != null) {
                    y += _title.getHeight();
                }
                return y;
            } else {
                return 0;
            }
        }
    }
}
