import Toybox.Lang;
using Toybox.WatchUi;


module Codex {

    typedef Callback as Method();

    typedef BooleanCallback as Method(value as Boolean);
    typedef NumberCallback as Method(value as Number);
    typedef LongCallback as Method(value as Long);
    typedef FloatCallback as Method(value as Float);
    typedef DoubleCallback as Method(value as Double);
    typedef NumericCallback as Method(value as Numeric);
    typedef SymbolCallback as Method(value as Symbol);
    typedef CharCallback as Method(value as Char);
    typedef StringCallback as Method(value as String);
    typedef ArrayCallback as Method(value as Array);
    typedef ByteArrayCallback as Method(value as ByteArray);
    typedef DictionaryCallback as Method(value as Dictionary);
    typedef ObjectCallback as Method(value as Object?);
    
    // Match type of AppBase.getInitialView()
    typedef ViewTuple as [WatchUi.Views] or [WatchUi.Views, WatchUi.InputDelegates];

    typedef ViewFactoryMethod as Method(options as Dictionary) as ViewTuple;

    typedef ViewFactory as {:method as ViewFactoryMethod} or {:method as ViewFactoryMethod, :options as Dictionary};

    typedef Intent as ViewTuple or ViewFactory or Callback;
}

