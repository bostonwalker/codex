import Toybox.Lang;
import Toybox.Graphics;
import Toybox.System;


module Codex {

    class Theme {
        // Determine if device has AMOLED-level resolution
        // 360 px seems like the min res for an AMOLED device https://developer.garmin.com/connect-iq/compatible-devices/
        // Would like a better way to determine this though
        static const IS_AMOLED = System.getDeviceSettings().screenHeight >= 360;

        public var color as Theme.ColorTheme;
        public var markdown as Theme.MarkdownTheme;
        public var menu as Theme.MenuTheme;
        public var layout as Theme.LayoutTheme;
        public var field as Theme.FieldTheme;

        function initialize(color as Theme.ColorTheme, options as {
            :markdown as Theme.MarkdownTheme,
            :menu as Theme.MenuTheme,
            }) {
            /*
            Color and style information for rendering content
            */
            self.color = color;
            markdown = Utils.getDefault(options, :markdown, new Theme.MarkdownTheme({})) as Theme.MarkdownTheme;
            menu = Utils.getDefault(options, :menu, new Theme.MenuTheme({})) as Theme.MenuTheme;
            layout = Utils.getDefault(options, :layout, new Theme.LayoutTheme({})) as Theme.LayoutTheme;
            field = Utils.getDefault(options, :field, new Theme.FieldTheme({})) as Theme.FieldTheme;
        }

        class ColorTheme {

            static const INDICATOR_ENABLED_DEFAULT = 0x12E649;
            static const INDICATOR_DISABLED_DEFAULT = 0xE61218;
            static const INDICATOR_CAUTION_DEFAULT = 0xEBCB01;

            public var background as Number;
            public var text as Number;
            public var majorAccent as Number;
            public var minorAccent as Number;
            public var indicatorEnabled as Number;
            public var indicatorDisabled as Number;
            public var indicatorCaution as Number;

            function initialize(
                background as Number,
                text as Number,
                majorAccent as Number, 
                minorAccent as Number,
                options as {
                    :indicatorEnabled as Number,
                    :indicatorDisabled as Number,
                    :indicatorCaution as Number,
                }) {

                self.background = background;
                self.text = text;
                self.majorAccent = majorAccent;
                self.minorAccent = minorAccent;

                self.indicatorEnabled = Utils.getDefault(options, :indicatorEnabled, INDICATOR_ENABLED_DEFAULT) as Number;
                self.indicatorDisabled = Utils.getDefault(options, :indicatorDisabled, INDICATOR_DISABLED_DEFAULT) as Number;
                self.indicatorCaution = Utils.getDefault(options, :indicatorCaution, INDICATOR_CAUTION_DEFAULT) as Number;
            }
        }

        class MarkdownTheme {

            static const BODY_SPACING_BEFORE_DEFAULT = Theme.IS_AMOLED ? 10 : 8;

            static const HEADING1_FONT_DEFAULT = Graphics.FONT_MEDIUM;
            static const HEADING1_SPACING_AFTER_DEFAULT = Theme.IS_AMOLED ? 20 : 16;
            static const HEADING1_HORIZONTAL_RULE_DEFAULT = true;
            static const HEADING2_FONT_DEFAULT = Graphics.FONT_SMALL;
            static const HEADING2_SPACING_AFTER_DEFAULT = Theme.IS_AMOLED ? 20 : 16;
            static const HEADING2_HORIZONTAL_RULE_DEFAULT = true;
            static const HEADING3_FONT_DEFAULT = Graphics.FONT_TINY;
            static const HEADING3_SPACING_AFTER_DEFAULT = Theme.IS_AMOLED ? 10 : 8;
            static const HEADING3_HORIZONTAL_RULE_DEFAULT = false;
            static const HEADING4_FONT_DEFAULT = Graphics.FONT_XTINY;
            static const HEADING4_SPACING_AFTER_DEFAULT = Theme.IS_AMOLED ? 10 : 8;
            static const HEADING4_HORIZONTAL_RULE_DEFAULT = false;

            static const IMAGE_SPACING_AFTER_DEFAULT = Theme.IS_AMOLED ? 10 : 8;
            
            static const LIST_LABEL_FONT_DEFAULT = Graphics.FONT_XTINY;
            static const LIST_SPACING_AFTER_DEFAULT = Theme.IS_AMOLED ? 20 : 16;
            static const LIST_ITEM_FONT_DEFAULT = Graphics.FONT_XTINY;
            static const LIST_ITEM_SPACING_AFTER_DEFAULT = Theme.IS_AMOLED ? 10 : 8;

            static const PARAGRAPH_FONT_DEFAULT = Graphics.FONT_XTINY;
            static const PARAGRAPH_SPACING_AFTER_DEFAULT = Theme.IS_AMOLED ? 20 : 16;

            static const TABLE_SPACING_AFTER_DEFAULT = Theme.IS_AMOLED ? 20 : 16;
            static const TABLE_CELL_PADDING_TOP_BOTTOM_DEFAULT = Theme.IS_AMOLED ? 10 : 8;
            static const TABLE_CELL_PADDING_SIDES_DEFAULT = Theme.IS_AMOLED ? 10 : 8;

            static const TABLE_HEADER_BOTTOM_LINE_THICKNESS_DEFAULT = Theme.IS_AMOLED ? 1 : 1;

            // Header options
            public var bodySpacingBefore as Number;

            // Header options
            public var heading1Font as Graphics.FontReference or Graphics.FontDefinition;
            public var heading1SpacingAfter as Number;
            public var heading1HorizontalRule as Boolean;
            public var heading2Font as Graphics.FontReference or Graphics.FontDefinition;
            public var heading2SpacingAfter as Number;
            public var heading2HorizontalRule as Boolean;
            public var heading3Font as Graphics.FontReference or Graphics.FontDefinition;
            public var heading3SpacingAfter as Number;
            public var heading3HorizontalRule as Boolean;
            public var heading4Font as Graphics.FontReference or Graphics.FontDefinition;
            public var heading4SpacingAfter as Number;
            public var heading4HorizontalRule as Boolean;

            // Image options
            public var imageSpacingAfter as Number;
            
            // List options
            public var listLabelFont as Graphics.FontReference or Graphics.FontDefinition;
            public var listSpacingAfter as Number;
            public var listItemFont as Graphics.FontReference or Graphics.FontDefinition;
            public var listItemSpacingAfter as Number;

            // Paragraph options
            public var paragraphFont as Graphics.FontReference or Graphics.FontDefinition;
            public var paragraphSpacingAfter as Number;

            // Table options
            public var tableSpacingAfter as Number;
            public var tableCellPaddingTopBottom as Number;
            public var tableCellPaddingSides as Number;
            public var tableHeaderBottomLineThickness as Number;

            function initialize(options as {
                :bodySpacingBefore as Number,
                :heading1Font as Graphics.FontReference or Graphics.FontDefinition,
                :heading1SpacingAfter as Number,
                :heading1HorizontalRule as Boolean,
                :heading2Font as Graphics.FontReference or Graphics.FontDefinition,
                :heading2SpacingAfter as Number,
                :heading2HorizontalRule as Boolean,
                :heading3Font as Graphics.FontReference or Graphics.FontDefinition,
                :heading3SpacingAfter as Number,
                :heading3HorizontalRule as Boolean,
                :heading4Font as Graphics.FontReference or Graphics.FontDefinition,
                :heading4SpacingAfter as Number,
                :heading4HorizontalRule as Boolean,
                :imageSpacingAfter as Number,
                :listLabelFont as Graphics.FontReference or Graphics.FontDefinition,
                :listSpacingAfter as Number,
                :listItemFont as Graphics.FontReference or Graphics.FontDefinition,
                :listItemSpacingAfter as Number,
                :paragraphFont as Graphics.FontReference or Graphics.FontDefinition,
                :paragraphSpacingAfter as Number,
                :tableSpacingAfter as Number,
                :tableCellPaddingTopBottom as Number,
                :tableCellPaddingSides as Number, 
                :tableHeaderBottomLineThickness as Number,
            }) {
                
                bodySpacingBefore = Utils.getDefault(options, :bodySpacingBefore, BODY_SPACING_BEFORE_DEFAULT) as Number;

                heading1Font = Utils.getDefault(options, :heading1Font, HEADING1_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                heading1SpacingAfter = Utils.getDefault(options, :heading1SpacingAfter, HEADING1_SPACING_AFTER_DEFAULT) as Number;
                heading1HorizontalRule = Utils.getDefault(options, :heading1HorizontalRule, HEADING1_HORIZONTAL_RULE_DEFAULT) as Boolean;
                heading2Font = Utils.getDefault(options, :heading2Font, HEADING2_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                heading2SpacingAfter = Utils.getDefault(options, :heading2SpacingAfter, HEADING2_SPACING_AFTER_DEFAULT) as Number;
                heading2HorizontalRule = Utils.getDefault(options, :heading2HorizontalRule, HEADING2_HORIZONTAL_RULE_DEFAULT) as Boolean;
                heading3Font = Utils.getDefault(options, :heading3Font, HEADING3_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                heading3SpacingAfter = Utils.getDefault(options, :heading3SpacingAfter, HEADING3_SPACING_AFTER_DEFAULT) as Number;
                heading3HorizontalRule = Utils.getDefault(options, :heading3HorizontalRule, HEADING3_HORIZONTAL_RULE_DEFAULT) as Boolean;
                heading4Font = Utils.getDefault(options, :heading4Font, HEADING4_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                heading4SpacingAfter = Utils.getDefault(options, :heading4SpacingAfter, HEADING4_SPACING_AFTER_DEFAULT) as Number;
                heading4HorizontalRule = Utils.getDefault(options, :heading4HorizontalRule, HEADING4_HORIZONTAL_RULE_DEFAULT) as Boolean;

                imageSpacingAfter = Utils.getDefault(options, :imageSpacingAfter, IMAGE_SPACING_AFTER_DEFAULT) as Number;

                listLabelFont = Utils.getDefault(options, :listLabelFont, LIST_LABEL_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                listSpacingAfter = Utils.getDefault(options, :listSpacingAfter, LIST_SPACING_AFTER_DEFAULT) as Number;
                listItemFont = Utils.getDefault(options, :listItemFont, LIST_ITEM_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                listItemSpacingAfter = Utils.getDefault(options, :listItemSpacingAfter, LIST_ITEM_SPACING_AFTER_DEFAULT) as Number;

                paragraphFont = Utils.getDefault(options, :paragraphFont, PARAGRAPH_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                paragraphSpacingAfter = Utils.getDefault(options, :paragraphSpacingAfter, PARAGRAPH_SPACING_AFTER_DEFAULT) as Number;

                tableSpacingAfter = Utils.getDefault(options, :tableSpacingAfter, TABLE_SPACING_AFTER_DEFAULT) as Number;
                tableCellPaddingTopBottom = Utils.getDefault(options, :tableCellPaddingTopBottom,
                    TABLE_CELL_PADDING_TOP_BOTTOM_DEFAULT) as Number;
                tableCellPaddingSides = Utils.getDefault(options, :tableCellPaddingSides, TABLE_CELL_PADDING_SIDES_DEFAULT) as Number;
                tableHeaderBottomLineThickness = Utils.getDefault(options, :tableHeaderBottomLineThickness,
                    TABLE_HEADER_BOTTOM_LINE_THICKNESS_DEFAULT) as Number;
            }
        }

        class MenuTheme {

            static const TITLE_FONT_DEFAULT = Graphics.FONT_TINY;
            static const TITLE_PADDING_DEFAULT = Theme.IS_AMOLED ? 10 : 8;

            static const ITEM_FONT_DEFAULT = Graphics.FONT_TINY;
            static const ITEM_FONT_FOCUSED_DEFAULT = Graphics.FONT_SMALL;
            static const ITEM_PADDING_DEFAULT = Theme.IS_AMOLED ? 4 : 3;
            static const ITEM_SIDE_PADDING_DEFAULT = Theme.IS_AMOLED ? 6 : 4;

            static const TOGGLE_KNOB_RADIUS_DEFAULT = Theme.IS_AMOLED ? 16 : 10;
            static const TOGGLE_TRAVEL_DEFAULT = Theme.IS_AMOLED ? 21 : 14;
            static const TOGGLE_SIDE_PADDING_DEFAULT = Theme.IS_AMOLED ? 21 : 14;

            static const FOCUS_FACTOR_DEFAULT = 1.12;

            public var titleFont as Graphics.FontReference or Graphics.FontDefinition;
            public var titlePadding as Number;

            public var itemFont as Graphics.FontReference or Graphics.FontDefinition;
            public var itemPadding as Number;
            public var itemFontFocused as Graphics.FontReference or Graphics.FontDefinition;
            public var itemSidePadding as Number;

            public var toggleKnobRadius as Number;
            public var toggleKnobTravel as Number;
            public var toggleSidePadding as Number;

            public var focusFactor as Number;

            function initialize(options as {
                :titleFont as Graphics.FontReference or Graphics.FontDefinitioner,
                :titlePadding as Number,
                :itemFont as Graphics.FontReference or Graphics.FontDefinition,
                :itemFontFocused as Graphics.FontReference or Graphics.FontDefinition,
                :itemPadding as Number,
                :itemSidePadding as Number,
                :toggleKnobRadius as Number,
                :toggleKnobTravel as Number,
                :toggleSidePadding as Number,
                :focusFactor as Number,
            }) {

                titleFont = Utils.getDefault(options, :titleFont, TITLE_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                titlePadding = Utils.getDefault(options, :titlePadding, TITLE_PADDING_DEFAULT) as Number;
                
                itemFont = Utils.getDefault(options, :itemFont, ITEM_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                itemFontFocused = Utils.getDefault(options, :itemFontFocused, ITEM_FONT_FOCUSED_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
                itemPadding = Utils.getDefault(options, :itemPadding, ITEM_PADDING_DEFAULT) as Number;
                itemSidePadding = Utils.getDefault(options, :itemSidePadding, ITEM_SIDE_PADDING_DEFAULT) as Number;

                toggleKnobRadius = Utils.getDefault(options, :toggleKnobRadius, TOGGLE_KNOB_RADIUS_DEFAULT) as Number;
                toggleKnobTravel = Utils.getDefault(options, :toggleKnobTravel, TOGGLE_TRAVEL_DEFAULT) as Number;
                toggleSidePadding = Utils.getDefault(options, :toggleSidePadding, TOGGLE_SIDE_PADDING_DEFAULT) as Number;

                focusFactor = Utils.getDefault(options, :focusFactor, FOCUS_FACTOR_DEFAULT) as Number;
            }
        }

        class LayoutTheme {

            static const CONTAINER_SPACING_HORIZONTAL_DEFAULT = Theme.IS_AMOLED ? 6 : 4;
            static const CONTAINER_SPACING_VERTICAL_DEFAULT = Theme.IS_AMOLED ? 12 : 8;
            static const CONTAINER_PADDING_TOP_BOTTOM_DEFAULT = Theme.IS_AMOLED ? 6 : 4;
            static const CONTAINER_PADDING_SIDES_DEFAULT = 0;

            public var containerSpacingHorizontal as Number;
            public var containerSpacingVertical as Number;
            public var containerPaddingTopBottom as Number;
            public var containerPaddingSides as Number;

            function initialize(options as {
                :containerSpacingHorizontal as Number,
                :containerSpacingVertical as Number,
                :containerPaddingTopBottom as Number,
                :containerPaddingSides as Number,
            }) {
                containerSpacingHorizontal = Utils.getDefault(options, :containerSpacingHorizontal, CONTAINER_SPACING_HORIZONTAL_DEFAULT) as Number;
                containerSpacingVertical = Utils.getDefault(options, :containerSpacingVertical, CONTAINER_SPACING_VERTICAL_DEFAULT) as Number;
                containerPaddingTopBottom = Utils.getDefault(options, :containerPaddingTopBottom, CONTAINER_PADDING_TOP_BOTTOM_DEFAULT) as Number;
                containerPaddingSides = Utils.getDefault(options, :containerPaddingSides, CONTAINER_PADDING_SIDES_DEFAULT) as Number;
            }
        }

        class FieldTheme {

            static const FIELD_FONT_DEFAULT = Graphics.FONT_TINY;

            public var fieldFont as Graphics.FontReference or Graphics.FontDefinition;

            function initialize(options as {
                :fieldFont as Graphics.FontReference or Graphics.FontDefinition,
            }) {

                fieldFont = Utils.getDefault(options, :fieldFont, FIELD_FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
            }
        }
    }
}
