import Toybox.Lang;
using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.Timer;
using Toybox.System;


module Codex {

    class Toast extends WatchUi.View {
        /*
        Popup notification
        */
        static const DURATION_DEFAULT = 2500;
        static const FONT_DEFAULT = Graphics.FONT_TINY;

        protected var _text as TextElement;

        protected var _duration as Number;
        protected var _backgroundColor as Number;

        protected var _textX as Number;
        protected var _textY as Number;
        protected var _popTimer as Codex.Timer;

        function initialize(text as String or ResourceId, theme as Theme, options as {
                :duration as Number, :font as Graphics.FontReference or Graphics.FontDefinition,
                :textColor as Number, :backgroundColor as Number, :justification as Graphics.TextJustification}) {
            /*

            :param text: Text to show
            :param theme: Styling options
            :param options:
                :duration: Duration to show toast in ms (default: 2500)
                :font: Text font (default: use theme)
                :textColor: Text color (default: use theme)
                :backgroundColor: Background color (default: use theme)
                :justification: Text justification (default: Graphics.TEXT_JUSTIFY_CENTER).
            */
            WatchUi.View.initialize();

            text = Utils.resolveString(text);

            _duration = Utils.getDefault(options, :duration, DURATION_DEFAULT) as Number;
            var textColor = Utils.getDefault(options, :textColor, theme.color.text) as Number;
            _backgroundColor = Utils.getDefault(options, :backgroundColor, theme.color.background) as Number;
            var font = Utils.getDefault(options, :font, FONT_DEFAULT) as Graphics.FontReference or Graphics.FontDefinition;
            var justification = Utils.getDefault(options, :justification, Graphics.TEXT_JUSTIFY_CENTER) as Graphics.TextJustification;

            _text = new TextElement(
                text,
                font,
                textColor,
                _backgroundColor,
                {
                    :multiline => true,
                    :width => Math.floor(CIRCULAR_DISPLAY_READABLE_WIDTH * System.getDeviceSettings().screenWidth),
                    :justification => justification,
                }
            );

            _textX = Math.ceil((1 - CIRCULAR_DISPLAY_READABLE_WIDTH) * System.getDeviceSettings().screenWidth / 2);
            _textY = Math.ceil((1 - CIRCULAR_DISPLAY_READABLE_HEIGHT) * System.getDeviceSettings().screenHeight / 2);

            _popTimer = new Codex.Timer(method(:pop));
        }

        function onShow() as Void {
            _popTimer.startFor(_duration);
        }

        function onHide() as Void {
            if (_popTimer.isRunning()) {
                _popTimer.stop();
            }
        }

        function onLayout(dc) as Void {
            // Load
            _text.load(dc);
        }

        function onUpdate(dc) as Void {
            // Draw
            dc.setColor(_backgroundColor, _backgroundColor);
            dc.clear();
            
            _text.draw(dc, _textX, _textY);
        }

        function pop(currentTime as Number) as Void {
            var app = Codex.getApp();
            if (app.getCurrentView()[0] == self) {
                app.exitView();
            }
        }

        static function launch(text as String or ResourceId, theme as Theme, options as Dictionary) as Void {
            // Launch a new instance of this view from any context
            var view = new Toast(text, theme, options);
            // No delegate (view does not accept input)
            Codex.getApp().selectView([view]);
        }
    }
}
