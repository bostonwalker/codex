import Toybox.Lang;
using Toybox.Graphics;
using Toybox.WatchUi;
using Toybox.System;

using Codex;


module Codex {

    class ScrollContainerView extends WatchUi.View {  // implements Positionable, Resizeable, Drawable, Loadable, Focusable, Scrollable, Controllable
        /*
        View wrapping content in a ScrollContainer with adjustable y pos
        */
        protected var _locY as Number;
        protected var _backgroundColor as Number?;

        protected var _container as ScrollContainer;
        
        function initialize(contents as Array<Drawable>, theme as Theme, options as {
                :isFocused as BooleanRef, :focus as NumberRef, :isWrapEnabled as Boolean,
                :layout as Container.Layout, :width as Number, :height as Number, :spacing as Number,
                :horizontalAlign as Container.HorizontalAlign, :verticalAlign as Container.VerticalAlign, :isVisible as BooleanRef,
                :locY as Number, :backgroundColor as Number}) {
            /*
            :param contents: View contents
            :param theme: Styling options
            :param options: 
                :isFocused: Whether ScrollContainer is focused (default: true)
                :focus: Focus of ScrollContainer (default: index of first visible item)
                :isWrapEnabled: If true, wrap to bottom when scrolling up from top and vice versa (default: true)
                :layout: Whether container is laid out horizontally or vertically (default: LAYOUT_HORIZONTAL)
                :width: Width of container (default: screen width)
                :height: Height of container. If not provided, will be set automatically to the height of its contents.
                :spacing: Spacing in between child components (default: 0)
                :horizontalAlign: How to align elements horizontally (default: HORIZONTAL_ALIGN_LEFT)
                :verticalAlign: How to align elements vertically (default: VERTICAL_ALIGN_TOP)
                :isVisible: Whether or not to draw container & contents (default: true)
                :locY: Y coordinate of view
                :backgroundColor: If provided, background color to draw behind container
            */
            WatchUi.View.initialize();

            _locY = Utils.getDefault(options, :locY, 0) as Number;
            _backgroundColor = Utils.getDefault(options, :backgroundColor, null) as Number?;

            if (!options.hasKey(:width)) {
                options[:width] = System.getDeviceSettings().screenWidth;
            }
            if (!options.hasKey(:isFocused)) {
                // Focus ScrollContainer by default. Contents should be initialized unfocused.
                options[:isFocused] = true;
            }

            _container = new Codex.ScrollContainer(contents, theme, options);
        }

        function setContents(contents as Array<Drawable>) as Void {
            _container.setContents(contents as Array<Drawable>);
        }

        /* Positionable */
        function setLocY(value as Number) as Void {
            _locY = value;
        }

        /* Resizeable */
        function setWidth(value as Number) as Void {
            _container.setWidth(value);
        }

        function setHeight(value as Number) as Void {
            _container.setHeight(value);
        }

        /* Drawable */
        function getWidth() as Number {
            return _container.getWidth();
        }

        function getHeight() as Number {
            return _container.getHeight();
        }

        function isVisible() as Boolean {
            return true;
        }

        function setIsVisible(value as BooleanRef) as Void {
            throw new NotImplementedError();
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            // Clear screen
            if (_backgroundColor != null) {
                dc.setColor(_backgroundColor, _backgroundColor);
                dc.clear();
            }
            // Draw contents
            _container.draw(dc, x, y);
        }

        /* Loadable */
        function load(dc as Graphics.Dc) as Void {
            _container.load(dc);
        }

        function unload() as Void {
            _container.unload();
        }

        /* Focusable */
        function isFocusable() as Boolean {
            return _container.isFocusable();
        }

        function isFocused() as Boolean {
            return _container.isFocused();
        }

        function setIsFocused(value as BooleanRef) as Void {
            _container.setIsFocused(value);
        }

        /* Scrollable */
        function goToStart() as Void {
            _container.goToStart();
        }

        function goToEnd() as Void {
            _container.goToEnd();
        }

        function goToNext() as Void {
            _container.goToNext();
        }

        function goToPrevious() as Void {
            _container.goToPrevious();
        }

        function isAtStart() as Boolean {
            return _container.isAtStart();
        }

        function isAtEnd() as Boolean {
            return _container.isAtEnd();
        }

        /* Controllable */
        function onEnter() as Boolean {
            return _container.onEnter();
        }

        function onPreviousPage() as Boolean {
            return _container.onPreviousPage();
        }

        function onNextPage() as Boolean {
            return _container.onNextPage();
        }

        function onBack() as Boolean {
            return _container.onBack();
        }

        function onTapAt(x as Number, y as Number) as Boolean {
            return _container.onTapAt(x, y);
        }

        function onTapElsewhere() as Void {
            _container.onTapElsewhere();
        }

        /* WatchUi.View */
        function onLayout(dc as Graphics.Dc) as Void {
            load(dc);
        }

        function onUpdate(dc as Graphics.Dc) as Void {
            draw(dc, 0, _locY);
        }
    }
}
