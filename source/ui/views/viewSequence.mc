import Toybox.Lang;
using Toybox.WatchUi;
using Toybox.Graphics;


module Codex {

    typedef SequenceableView as interface {
        /* WatchUi.View */
        function onLayout(dc as Graphics.Dc) as Void;
        function onShow() as Void;
        function onUpdate(dc as Graphics.Dc) as Void;
        function onHide() as Void;
        /* Positionable */
        function setLocY(value as Number) as Void;
        /* Resizeable */
        function setWidth(value as Number) as Void;
        function setHeight(value as Number) as Void;
        /* Focusable */
        function isFocusable() as Boolean;
        function isFocused() as Boolean;
        function setIsFocused(value as BooleanRef) as Void;
        /* Controllable */
        function onEnter() as Boolean;
        function onPreviousPage() as Boolean;
        function onNextPage() as Boolean;
        function onBack() as Boolean;
        function onTapAt(x as Number, y as Number) as Boolean;
        function onTapElsewhere() as Void;
        /* optional: Scrollable  */
    };

    class ViewSequence extends WatchUi.View {  // implements Positionable, Focusable, Scrollable, Controllable
        /*
        A View containing a sequence of other views that can be scrolled through
        */
        protected var _views as Array<SequenceableView>;

        protected var _goToNextOnEnter as Boolean;
        protected var _goToPreviousOnBack as Boolean;

        protected var _isFocused as Boolean;
        protected var _currentViewIndex as Number;

        function initialize(views as Array<SequenceableView>, options as {
                :locY as Number, :isFocused as BooleanRef, :goToNextOnEnter as Boolean, :goToPreviousOnBack as Boolean}) {
            /*
            :param views: Sequence of views to display
            :param options:
                :locY: Y coordinate of view
                :isFocused: Whether ViewSequence is focused (default: true)
                :goToNextOnEnter: If true, go to next view if possible when user presses enter and 
                    input is not handled by current view (default: false).
                :goToPreviousOnBack: If true, return to previous view if possible when user presses back and 
                    input is not handled by current view. If false, just exit view (default: false).
            */
            _views = views;
            for (var i = 0; i < views.size(); i++) {
                if (!views[i].isFocusable()) {
                    throw new ValueError("View not focusable: " + views[i].toString());
                }
            }

            _isFocused = Ref.linkBoolean(self, :setIsFocused, Utils.getDefault(options, :isFocused, true) as BooleanRef);
            _goToNextOnEnter = Utils.getDefault(options, :goToNextOnEnter, false) as Boolean;
            _goToPreviousOnBack = Utils.getDefault(options, :goToPreviousOnBack, false) as Boolean;

            _currentViewIndex = 0;

            WatchUi.View.initialize();
        }

        /* Page navigation */
        function getCurrentViewIndex() as Number {
            // Get the current view index
            return _currentViewIndex;
        }

        function goToViewIndex(index as Number) as Void {
            // Change the current view index
            _views[_currentViewIndex].onHide();
            _views[index].onShow();
            if (_views[index].isFocused() != _isFocused) {
                _views[index].setIsFocused(_isFocused);
            }
            _currentViewIndex = index;
        }

        function goToFirstView() as Void {
            goToViewIndex(0);
        }

        function goToLastView() as Void {
            goToViewIndex(_views.size() - 1);
        }

        function goToNextView() as Void {
            goToViewIndex(_currentViewIndex + 1);
        }

        function goToPreviousView() as Void {
            goToViewIndex(_currentViewIndex - 1);
        }

        function isOnFirstView() as Boolean {
            return _currentViewIndex == 0;
        }

        function isOnLastView() as Boolean {
            return _currentViewIndex == _views.size();
        }

        /* WatchUi.View */
        function onLayout(dc as Graphics.Dc) as Void {
            // Layout all child views
            for (var i = 0; i < _views.size(); i++) {
                _views[i].onLayout(dc);
            }
        }

        function onShow() as Void {
            // Show current view
            var currentView = _views[_currentViewIndex];
            currentView.onShow();
        }

        function onUpdate(dc as Graphics.Dc) as Void {
            // Draw current view
            var currentView = _views[_currentViewIndex];
            currentView.onUpdate(dc);
        }

        function onHide() as Void {
            // Hide current view
            var currentView = _views[_currentViewIndex];
            currentView.onHide();
        }

        /* Positionable */
        function setLocY(value as Number) as Void {
            for (var i = 0; i < _views.size(); i++) {
                _views[i].setLocY(value);
            }
        }

        function setWidth(value as Number) as Void {
            for (var i = 0; i < _views.size(); i++) {
                _views[i].setWidth(value);
            }
        }

        function setHeight(value as Number) as Void {
            for (var i = 0; i < _views.size(); i++) {
                _views[i].setHeight(value);
            }
        }

        /* Focusable */
        function isFocusable() as Boolean {
            return true;
        }

        function isFocused() as Boolean {
            return _isFocused;
        }

        function setIsFocused(value as BooleanRef) as Void {
            _isFocused = Ref.linkBoolean(self, :setIsFocused, value);
            var currentView = _views[_currentViewIndex];
            currentView.setIsFocused(_isFocused);
        }

        /* Scrollable */
        function goToStart() as Void {
            var currentView = _views[_currentViewIndex];
            if (currentView has :goToStart) {
                (currentView as Scrollable).goToStart();
            }
        }

        function goToEnd() as Void {
            var currentView = _views[_currentViewIndex];
            if (currentView has :goToStart) {
                (currentView as Scrollable).goToEnd();
            }
        }

        function goToNext() as Void {
            var currentView = _views[_currentViewIndex];
            if (currentView has :goToStart) {
                (currentView as Scrollable).goToNext();
            }
        }

        function goToPrevious() as Void {
            var currentView = _views[_currentViewIndex];
            if (currentView has :goToStart) {
                (currentView as Scrollable).goToPrevious();
            }
        }

        function isAtStart() as Boolean {
            var currentView = _views[_currentViewIndex];
            if (currentView has :goToStart) {
                return (currentView as Scrollable).isAtStart();
            }
            return true;
        }

        function isAtEnd() as Boolean {
            var currentView = _views[_currentViewIndex];
            if (currentView has :goToStart) {
                return (currentView as Scrollable).isAtEnd();
            }
            return true;
        }

        /* Controllable */
        function onEnter() as Boolean {
            var currentView = _views[_currentViewIndex];
            var isHandled = currentView.onEnter();
            if (!isHandled && _goToNextOnEnter && !isAtEnd()) {
                goToNextView();
                return true;
            }
            return isHandled;
        }

        function onPreviousPage() as Boolean {
            var currentView = _views[_currentViewIndex];
            return currentView.onPreviousPage();
        }

        function onNextPage() as Boolean {
            var currentView = _views[_currentViewIndex];
            return currentView.onNextPage();
        }

        function onBack() as Boolean {
            var currentView = _views[_currentViewIndex];
            var isHandled = currentView.onBack();
            if (!isHandled && _goToPreviousOnBack && !isOnFirstView()) {
                // Go to previous view instead of exiting
                goToPreviousView();
                return true;
            }
            return isHandled;
        }

        function onTapAt(x as Number, y as Number) as Boolean {
            var currentView = _views[_currentViewIndex];
            return currentView.onTapAt(x, y);
        }

        function onTapElsewhere() as Void {
            var currentView = _views[_currentViewIndex];
            currentView.onTapElsewhere();
        }
    }
}
