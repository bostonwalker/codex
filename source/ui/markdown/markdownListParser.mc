import Toybox.Lang;
import Toybox.Graphics;


module Codex {

    class MarkdownListParser extends MemoIterator {

        static enum {
            STATE_SOF,
            STATE_LISTITEM,
            STATE_NESTED_LIST,
            STATE_EOF,
        }

        private var _lines as Iterator;
        private var _level as Number;
        private var _enumType as Number;
        private var _width as Number;
        private var _theme as Theme;

        private var _state as Number;
        private var _subParser as Iterator?;

        function initialize(lines as Iterator, level as Number, enumType as Number, width as Number,
            theme as Theme) {
            /*
            An iterator for parsing a markdown list into a series of list elements

            :param lines: Markdown lines
            :param level: List level (0 - 3)
            :param enumType: List enumeration type
            :param width: Element width in pixels
            :param theme: Codex theme
            */
            MemoIterator.initialize();

            if (level > 2) {
                throw new ValueError(
                    "Cannot nest lists more than 2 level(s) deep");
            }

            _lines = lines;
            _level = level;
            _enumType = enumType;
            _width = width;
            _theme = theme;

            _state = STATE_SOF;
            _subParser = null;
        }

        protected function nextImpl() as Object? {

            while (_state != STATE_EOF) {
                    
                if (_subParser != null) {
                    // Must capture all lines within sub-parser before moving on to next element
                    _subParser.exploit();
                    _subParser = null;
                }

                var nextLine = _lines.peek() as String;
                var nextState = getState(nextLine, _level, _enumType);

                var result = null;
                switch(nextState) {
                    case STATE_LISTITEM:
                        result = parseListItem(_lines.next() as String, _level, _enumType, _width, _theme);
                        break;
                    case STATE_NESTED_LIST:
                        if (_state == STATE_SOF) {
                            throw new ValueError(
                                "First item in list is over-indented");
                        }
                        _subParser = getIndentedLines(_lines, _level + 1);
                        result = parseNestedList(_subParser, _level + 1, _width, _theme);
                        break;
                    case STATE_EOF:
                        // End of file, do nothing
                        _lines.next();
                        break;
                }

                _state = nextState;

                if (result != null) {
                    return result;
                }
            }

            return null;
        }

        static function getState(line as String?, level as Number, enumType as Number) as Number {
            // Get state from line which will be used to determine how to parse line
            if (line == null) {
                return STATE_EOF;
            }

            // Strip leading tabs
            line = line.substring(level, line.length());

            var chars = line.toCharArray();

            if (chars[0] == '\t') {
                return STATE_NESTED_LIST;
            } else if (enumType == List.ENUMTYPE_BULLET) {
                // Case: unordered list
                if ((chars[0] == '-' || chars[0] == '+' || chars[0] == '*') &&
                    chars[1] == ' ') {
                    return STATE_LISTITEM;
                } else {
                    throw new ValueError(
                        "Unordered list item cannot start with '" + chars[0] + "'");
                }
            } else {
                // Case: ordered list
                var dotIndex = line.find(".");
                if (dotIndex != null && line.length() >= dotIndex + 4 &&
                    StringUtils.isAlphanumeric(line.substring(0, dotIndex)) &&
                    line.substring(dotIndex + 1, dotIndex + 3).equals("  ")) {
                    return STATE_LISTITEM;
                } else {
                    throw new ValueError(
                        "Invalid ordered list item: \"" + line + "\"");
                }
            }
        }

        function isIndented(options as {:item as String, :level as Number}) as Boolean {
            // Check that first `_level` chars are all tabs
            var line = options[:item];
            var level = options[:level];

            var chars = line.toCharArray();
            if (chars.size() < level) {
                return false;
            }
            for (var i = 0; i < level; i++) {
                if (chars[i] != '\t') {
                    return false;
                }
            }
            return true;
        }

        (:typecheck(false))
        function getIndentedLines(lines as Iterator, level as Number) as Iterator {
            // Return iterator over all remaining lines that are indented by current level + 1
            var remainingLines = new Range(lines, lines.getSeek(), null);
            return new While(remainingLines, {:method => method(:isIndented), :options => {:level => level}});
        }

        static function stripEnum(line as String, level as Number, enumType as Number) as String {
            // String enumeration characters from beginning of list item

            // Strip tabs
            line = line.substring(level, line.length());

            var result;

            if (enumType == List.ENUMTYPE_BULLET) {
                // List is unodered
                if (line.length() < 3) {
                    throw new ValueError(
                        "Line in bulleted list must be at least 3 characters long");
                }
                var firstChar = line.substring(0, 1).toCharArray()[0];
                if (!(firstChar == '*' || firstChar == '+' || firstChar == '-')) {
                    throw new ValueError(
                        "Unordered list item cannot start with '" + firstChar + "'");
                }
                if (!(line.substring(1, 2).equals(" ") && !line.substring(2, 3).equals(" "))) {
                    throw new ValueError(
                        "Bullet must be followed by a single space");
                }

                result = line.substring(2, line.length());

            } else {
                // List is ordered

                // Seperate enumeration group from list item
                if (line.length() < 5) {
                    throw new ValueError(
                        "Line in ordered list must be at least 5 characters long");
                }
                var dotIndex = line.find(".  ");
                if (dotIndex == -1 || line.length() < dotIndex + 4 ||
                    line.substring(dotIndex + 3, dotIndex + 4).equals(" ")) {
                    throw new ValueError(
                        "Ordered list labels must be followed by \".  \" (a period and two spaces)");
                }

                var label = line.substring(0, dotIndex);
                var chars = label.toCharArray();

                // Parse enumeration group
                switch(enumType) {
                    case List.ENUMTYPE_ARABIC:
                        if (label.toNumber() == null) {
                            throw new ValueError(
                                "Invalid arabic numeral: \"" + label + "\"");
                        }
                        break;
                    case List.ENUMTYPE_ROMANLOWER:
                        if (!label.equals(label.toLower())) {
                            throw new ValueError(
                                "Invalid lowercase roman numeral: \"" + label + "\"");
                        }
                        // Convert to int to raise parsing errors, if any
                        List.romanToInt(label.toUpper());
                        break;
                    case List.ENUMTYPE_ROMANUPPER:
                        if (!label.equals(label.toUpper())) {
                            throw new ValueError(
                                "Invalid uppercase roman numeral: \"" + label + "\"");
                        }
                        // Convert to int to raise parsing errors, if any
                        List.romanToInt(label);
                        break;
                    case List.ENUMTYPE_ALPHALOWER:
                        if (!label.equals(label.toLower())) {
                            throw new ValueError(
                                "Invalid lowercase alphabetical numeral: \"" + label + "\"");
                        }
                        for (var i = 0; i < chars.size(); i++) {
                            if (!(chars[i] >= 'a' && chars[i] <= 'z')) {
                                throw new ValueError(
                                    "Invalid lowercase alphabetical numeral: \"" + label + "\"");
                            }
                        }
                        break;
                    case List.ENUMTYPE_ALPHAUPPER:
                        if (!label.equals(label.toUpper())) {
                            throw new ValueError(
                                "Invalid uppercase alphabetical numeral: \"" + label + "\"");
                        }
                        for (var i = 0; i < chars.size(); i++) {
                            if (!(chars[i] >= 'A' && chars[i] <= 'Z')) {
                                throw new ValueError(
                                    "Invalid uppercase alphabetical numeral: \"" + label + "\"");
                            }
                        }
                        break;
                    default:
                        throw new ValueError(
                            "Invalid list enumeration type: " + enumType.toString());

                } 

                result = line.substring(dotIndex + 3, line.length());
            }
            return result;
        }

        static function parseListItem(line as String, level as Number, enumType as Number, width as Number,
            theme as Codex.Theme) as ListItem {
        
            var itemStr = stripEnum(line, level, enumType);

            var labelWidth = List.calculateLabelWidth(theme, enumType);
            if (labelWidth >= width) {
                throw new ValueError(
                    "ListItem cannot be laid out within provided width");
            }

            return new ListItem(itemStr, width - labelWidth, theme, {});
        }

        (:typecheck(false))
        static function parseNestedList(lines as Iterator, level as Number, width as Number,
            theme as Codex.Theme) as List {
            // Get enum type of nexted list
            var enumType = getListEnumType(lines.peek() as String, level);

            var indent = List.calculateIndent(theme);
            if (indent >= width) {
                throw new ValueError(
                    "Sublist cannot be laid out within provided width");
            }

            if (enumType == List.ENUMTYPE_BULLET) {
                return new UnorderedList(
                    new MarkdownListParser(lines, level, List.ENUMTYPE_BULLET, width - indent, theme),
                    level, width, theme, {});
            } else {
                return new OrderedList(
                    new MarkdownListParser(lines, level, enumType, width - indent, theme),
                    level, enumType, width, theme, {});
            }
        }

        static function getListEnumType(line as String, level as Number) {
            // Detect the enumeration type of an ordered list based on the first char of the first line

            var chars = line.toCharArray();

            // Check indentation
            // Pre-processor will convert leading spaces to tabs
            for (var i = 0; i < level; i++) {
                if (chars[i] != '\t') {
                    throw new ValueError(
                        "List item of nesting level " + level.toString() + 
                        " must begin with " + level.toString() + " tab character(s)");
                }
            }
    
            // Check first character of label
            var firstChar = chars[level];
            switch (firstChar) {
                case '-':
                case '+':
                case '*':
                    return List.ENUMTYPE_BULLET;
                case '1':
                    return List.ENUMTYPE_ARABIC;
                case 'i':
                    return List.ENUMTYPE_ROMANLOWER;
                case 'I':
                    return List.ENUMTYPE_ROMANUPPER;
                case 'a':
                    return List.ENUMTYPE_ALPHALOWER;
                case 'A':
                    return List.ENUMTYPE_ALPHAUPPER;
                case '\t':
                    throw new ValueError(
                        "List item over-indented");
                default:
                    var dotIndex = line.find(".");
                    if (dotIndex != null) {
                        throw new ValueError(
                            "Invalid list label: cannot start an ordered list with \"" + 
                            line.substring(0, dotIndex) + "\"");
                    } else {
                        throw new ValueError(
                            "Line contains invalid list label: \"" + line + "\"");
                    }
            }
        }
    }
}
