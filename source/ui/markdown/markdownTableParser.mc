import Toybox.Lang;
import Toybox.Math;


module Codex {

    class MarkdownTableParser extends MemoIterator {

        enum {
            STATE_SOF,
            STATE_HEADER,
            STATE_HYPHEN,
            STATE_CONTENT,
            STATE_EOF,
        }

        private var _lines as Iterator;
        private var _width as Number;
        private var _theme as Theme;

        private var _state as Number;

        function initialize(lines as Iterator, width as Number, theme as Theme) {
            /*
            An iterator for parsing a markdown table into a series of table rows

            :param lines: Iterator of Markdown lines
            :param width: Table width in pixels
            :param theme: Styling options
            */
            MemoIterator.initialize();

            _lines = lines;
            _width = width;
            _theme = theme;

            _state = STATE_SOF;
        }

        protected function nextImpl() as Object? {
            // Implementation of next()
            while(_state != STATE_EOF) {

                var nextLine = _lines.next() as String?;
                var nextState;

                var result;

                if (nextLine == null) {
                    // No more table lines left
                    nextState = STATE_EOF;
                    result = null;
                } else {
                    switch(_state) {
                        case STATE_SOF:
                            nextState = STATE_HEADER;
                            result = parseContentRow(nextLine, true, _width, _theme);
                            break;
                        case STATE_HEADER:
                            nextState = STATE_HYPHEN;
                            result = parseHyphenRow(nextLine);
                            break;
                        default:
                            nextState = STATE_CONTENT;
                            result = parseContentRow(nextLine, false, _width, _theme);
                            break;
                    }
                }

                _state = nextState;

                if (result != null) {
                    return result;
                }
            }

            return null;
        }

        static function parseHyphenRow(str as String) as Null {
            // TODO: use this information to determine content alignment
            // Return: number of cells
            // TODO: implement
            return null;
        }

        (:typecheck(false))
        static function parseContentRow(str as String, isHeaderRow as Boolean, width as Number,
            theme as Codex.Theme) as TableRow {

            var cells = [];
            var cellStrs = (new Splitter(str, "|")).asArray() as Array<String>;
            var avgCellWidth = (width - 1).toFloat() / (cellStrs.size() - 2);
            var state = :startOfRow;

            for (var i = 0; i < cellStrs.size(); i++) {
                if (state == :endOfRow) {
                    // Needed to escape the loop since the logic for detecting state is inside a case clause
                    break;
                }
                var cellStr = cellStrs[i];
                var cellWidth = (Math.round((i + 1) * avgCellWidth).toNumber() - Math.round(i * avgCellWidth).toNumber()) - 1;
                switch(state) {
                    case :startOfRow:
                        if (cellStr.length() > 0) {
                            throw new ValueError(
                                "Each table row must begin with a leading '|' character");
                        }
                        state = :cell;
                        break;
                    case :cell:
                        if (i != cellStrs.size() - 1) {
                            var contentWidth = cellWidth - 2 * theme.markdown.tableCellPaddingSides;
                            var content = parseCellContent(cellStr, contentWidth, theme);
                            if (isHeaderRow) {
                                cells.add(new TableHeader(content, cellWidth, theme, {}));
                            } else {
                                cells.add(new TableCell(content, cellWidth, theme, {}));
                            }
                        } else {
                            if (cellStr.length() == 0) {
                                state = :endOfRow;
                            } else {
                                throw new ValueError(
                                    "Each table row must end with a trailing '|' character");
                            }
                        }
                        break;
                }
            }

            if (cells.size() == 0) {
                throw new ValueError(
                    "Each table row must have at least one cell");
            }

            return new TableRow(new ArrayIterator(cells), width, theme, {});
        }

        static function parseCellContent(str as String, width as Number, theme as Codex.Theme) as MarkdownElement {
            str = StringUtils.trim(str, false);
            var state = MarkdownParser.getState(str);
            switch (state) {
                case MarkdownParser.STATE_WHITESPACE:
                case MarkdownParser.STATE_PARAGRAPH:
                    return MarkdownParser.parseParagraph(str, width, theme);
                case MarkdownParser.STATE_HEADING:
                    return MarkdownParser.parseHeading(str, width, theme);
                case MarkdownParser.STATE_IMAGE:
                    return MarkdownParser.parseImage(str, width, theme);
                default:
                    throw new ValueError(
                        "Unable to parse table cell content: \"" + str + "\"");
            }
        }
    }
}
