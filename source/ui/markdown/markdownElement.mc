import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    typedef MarkdownElement as interface {  // implements Loadable, Drawable

        function loadYRange(dc as Graphics.Dc, yMin as Number, yMax as Number?) as Void;

        function isHeightGreaterThanOrEqualTo(threshold as Number) as Boolean;

        function isHeightKnown() as Boolean;

        function getSpacingAfter() as Number;

        function hasScrollPoints() as Boolean;

        function getScrollPoint(y as Number) as Number;

        function isIdenticalTo(object as Object) as Boolean;

        /* Loadable */
        function load(dc as Graphics.Dc) as Void;

        function unload() as Void;

        /* Drawable */
        function getWidth() as Number;

        function getHeight() as Number;

        function isVisible() as Boolean;

        function setIsVisible(value as BooleanRef) as Void;

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void;
    };
}
