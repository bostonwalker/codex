import Toybox.Math;
import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.System;
import Toybox.Lang;


// Scroll request states
enum {
    SCROLL_PREVIOUS,
    SCROLL_NEXT,
    SCROLL_TO_TOP,
    SCROLL_TO_BOTTOM,
}


/*
CIRCULAR_DISPLAY_READABLE_WIDTH / CIRCULAR_DISPLAY_READABLE_HEIGHT
Define the readable area of a circular display
   ______
  /______\
 /|      |\
| |      | |
 \|______|/
  \______/

Rectangle above is the fully-readable area of a circular display
CIRCULAR_DISPLAY_READABLE_WIDTH is the ratio of the top/bottom edges to the diameter of the display
CIRCULAR_DISPLAY_READABLE_HEIGHT is the ratio of the sides to the diameter
*/
const CIRCULAR_DISPLAY_READABLE_WIDTH = Math.cos(30 * (Math.PI / 180.0));  // sqrt(3) / 2
const CIRCULAR_DISPLAY_READABLE_HEIGHT = Math.sin(30 * (Math.PI / 180.0));  // 1/2


module Codex {

    class MarkdownView extends WatchUi.View {  // implements Codex.Resizeable, Codex.Scrollable, Codex.Controllable

        // State variables 
        private var _scrollLocation as Number;
        private var _scrollRequest as Number?;
        private var _isLoaded as Boolean?;

        // Non-state variables
        private var _markdown as String or ResourceId;
        private var _theme as Theme;

        private var _locY as Number;
        private var _width as Number;
        private var _height as Number;
        private var _xOrigin as Number?;
        private var _yOrigin as Number?;
        private var _scrollAmount as Number?;

        private var _parser as MarkdownParser;
        private var _body as Body;

        function initialize(markdown as String or ResourceId, theme as Theme, options as {:locY as Number, :height as Number}) {
            /*
            :param markdown: Markdown formatted text value to render
            :param theme: Styling options
            :param options:
                :locY: Initial Y position to render at (default: 0)
                :height: Initial height (default: device screen height)
            */
            WatchUi.View.initialize();
            
            _markdown = markdown;
            _theme = theme;

            // Validate screen shape
            var screenShape = System.getDeviceSettings().screenShape;
            var screenWidth = System.getDeviceSettings().screenWidth;
            var screenHeight = System.getDeviceSettings().screenHeight;

            _locY = Utils.getDefault(options, :locY, 0) as Number;
            _width = screenWidth;
            _height = Utils.getDefault(options, :height, screenHeight) as Number;

            // Assume circular screen  (all supported devices currently have circular screens)
            if (System.getDeviceSettings().screenShape != System.SCREEN_SHAPE_ROUND) {
                throw new ValueError(
                    "Unsupported screen shape detected: " + screenShape.toString());
            }
            if (screenWidth != screenHeight) {
                throw new ValueError(
                    "Unsupported screen dimensions detected: width != height");
            }

            // Calculate readable width using CIRCULAR_DISPLAY_READABLE_WIDTH and round to nearest 2 px
            var readableWidth = 2 * (screenWidth * CIRCULAR_DISPLAY_READABLE_WIDTH / 2.0).toNumber();
            // Start displaying text at the top edge of the circle
            _xOrigin = Math.ceil((screenWidth - readableWidth) / 2).toNumber();

            _parser = new MarkdownParser(_markdown, readableWidth, theme);
            _body = new Body(_parser, readableWidth, {});

            // Initial state: scrolled to top, no scroll request, not loaded
            _scrollLocation = 0;
            _scrollRequest = null;
            _isLoaded = false;
        }

        function getMarkdown() as String or ResourceId {
            return _markdown;
        }

        function getTheme() as Codex.Theme {
            return _theme;
        }

        function setLocY(value as Number) as Void {
            _locY = value;
            // Cause layout update on next onUpdate()
            _isLoaded = false;
        }

        function getWidth() as Number {
            return _width;
        }

        function setWidth(value as Number) as Void {
            throw new NotImplementedError();
        }

        function getHeight() as Number {
            return _height;
        }

        function setHeight(value as Number) as Void {
            _height = value;
            // Cause layout update on next onUpdate()
            _isLoaded = false;
        }

        function goToStart() as Void {
            _scrollRequest = SCROLL_TO_TOP;
        }

        function goToEnd() as Void {
            _scrollRequest = SCROLL_TO_BOTTOM;
        }

        function goToNext() as Void {
            _scrollRequest = SCROLL_NEXT;
        }

        function goToPrevious() as Void {
            _scrollRequest = SCROLL_PREVIOUS;
        }

        function isAtStart() as Boolean {
            return _scrollLocation <= 0;
        }

        function isAtEnd() as Boolean {
            return _body.isHeightKnown() && (_body.getHeight() - _scrollLocation <= _scrollAmount);
        }

        function isFocused() as Boolean {
            // Always treat as focused
            return true;
        }

        function setIsFocused(value as BooleanRef) as Void {
            // Do nothing
        }

        private function _reload(dc as Graphics.Dc) {
            // Recalculate vertical layout
            var screenDiameter = System.getDeviceSettings().screenHeight;
            // Round to nearest 2 px
            var readableHeight = 2 * (screenDiameter * CIRCULAR_DISPLAY_READABLE_HEIGHT / 2.0).toNumber();

            // Start displaying text at the top edge of the readable rectangle, or locY + some margin, whichever is lower
            var minYOrigin = _locY + _theme.markdown.bodySpacingBefore;
            _yOrigin = Math.ceil((screenDiameter - readableHeight) / 2).toNumber();
            if (_yOrigin < minYOrigin) {
                readableHeight -= minYOrigin - _yOrigin;
                _yOrigin = minYOrigin;
            }

            _height = screenDiameter;
            _scrollAmount = readableHeight;

            // Load content
            var y = _scrollLocation - _yOrigin;
            _body.loadYRange(dc, y, y + _height);
        }

        function onUpdate(dc as Graphics.Dc) as Void {
            /*
            - Load elements if not loaded
            - Perform progressive loading if scroll requested
            - Flush screen buffer, then draw markdown elements to screen recursively
            */
            if (!_isLoaded) {
                _reload(dc);
                _isLoaded = true;
            }

            if (_scrollRequest != null) {
                if (_scrollRequest == SCROLL_NEXT) {
                    _scrollNext(dc);
                } else if (_scrollRequest == SCROLL_PREVIOUS) {
                    _scrollPrevious(dc);
                } else if (_scrollRequest == SCROLL_TO_TOP) {
                    _scrollToTop(dc);
                } else if (_scrollRequest == SCROLL_TO_BOTTOM) {
                    _scrollToBottom(dc);
                }
                _scrollRequest = null;
            }

            dc.setColor(_theme.color.text, _theme.color.background);
            dc.clear();

            _body.draw(dc, _xOrigin, _yOrigin - _scrollLocation);
        }

        function onHide() as Void {
            // Unload resources recursively
            _body.unload();
            _parser.unload();
            _isLoaded = false;
        }

        private function _scrollPrevious(dc as Graphics.Dc) as Void {

            var y = _scrollLocation - _yOrigin;

            // Pre-load content
            _body.loadYRange(dc, y - (2 * _scrollAmount), y + _height);

            // Get scroll point within element
            var scrollPoint = _body.getScrollPoint(
                _scrollLocation > _scrollAmount ? _scrollLocation - _scrollAmount : 0);

            // Compute scroll amount
            if (scrollPoint >= _scrollLocation) {
                throw new ValueError(
                    "Computed scrollPoint was zero or in wrong direction");
            }
            _scrollLocation = scrollPoint;
        }

        private function _scrollNext(dc as Graphics.Dc) as Void {
            var y = _scrollLocation - _yOrigin;

            // Pre-load content
            _body.loadYRange(dc, y, y + _height + _scrollAmount + 1);

            // Get scroll point within element
            var scrollPoint = _body.getScrollPoint(
                -_scrollLocation < _scrollAmount ? _scrollAmount + _scrollLocation : 0);

            // Test if scroll point overruns end of last element
            if (!_body.isHeightGreaterThanOrEqualTo(scrollPoint + _scrollAmount)) {
                // Set scroll point to end
                scrollPoint = _body.getHeight() - _scrollAmount;
            }

            // Compute scroll amount
            if (scrollPoint <= _scrollLocation) {
                throw new ValueError(
                    "Computed scrollPoint was zero or in wrong direction");
            }
            _scrollLocation = scrollPoint;
        }

        private function _scrollToTop(dc as Graphics.Dc) as Void {
            // Reset scroll to top
            _scrollLocation = 0;
            var y = -_yOrigin;
            _body.loadYRange(dc, y, y + _height);
        }

        private function _scrollToBottom(dc as Graphics.Dc) as Void {
            // Scroll all the way to bottom
            _body.loadYRange(dc, -_yOrigin, null);
            _scrollLocation = _body.getHeight() - _scrollAmount;
        }

        /* Codex.Controllable */
        function onEnter() as Boolean {
            return false;
        }

        function onPreviousPage() as Boolean {
            if (!isAtStart()) {
                goToPrevious();
                return true;
            }
            return false;
        }

        function onNextPage() as Boolean {
            if (!isAtEnd()) {
                goToNext();
                return true;
            }
            return false;
        }

        function onBack() as Boolean {
            return false;
        }

        function onTapAt(x as Number, y as Number) as Boolean {
            return false;
        }

        function onTapElsewhere() as Void {}
    }
}