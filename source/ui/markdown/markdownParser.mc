import Toybox.Test;
import Toybox.System;
import Toybox.Lang;
import Toybox.Math;


module Codex {

    class MarkdownParser extends MemoIterator {

        static enum {
            STATE_SOF,
            STATE_PARAGRAPH,
            STATE_HEADING,
            STATE_LIST,
            STATE_IMAGE,
            STATE_TABLE,
            STATE_WHITESPACE,
            STATE_EOF,
        }

        // Non-state variables
        private var _width as Number;
        private var _theme as Theme;

        // State variables
        private var _preprocessor as MarkdownPreprocessor;
        private var _state as Number;
        private var _subParser as Iterator?;

        function initialize(str as String or ResourceId, width as Number, theme as Theme) {
            /*
            An iterator for parsing markdown into a sequence of elements

            :param str: Markdown string or resource ID
            :param width: Display width in pixels
            :param theme: Styling options
            */
            MemoIterator.initialize();

            _width = width;
            _theme = theme;

            _preprocessor = new MarkdownPreprocessor(str);

            _state = STATE_SOF;
            _subParser = null;
        }

        protected function nextImpl() as Object? {
            
            while (_state != STATE_EOF) {

                if (_subParser != null) {
                    // Must capture all lines within sub-parser before moving on to next element
                    _subParser.exploit();
                    _subParser = null;
                }

                var nextLine = _preprocessor.peek() as String;
                var nextState = getState(nextLine);

                var result = null;
                switch(nextState) {
                    case STATE_WHITESPACE:
                        // No element present
                        _preprocessor.next();
                        break;
                    case STATE_HEADING:
                        // Headings always break on newline
                        result = parseHeading(_preprocessor.next() as String, _width, _theme);
                        break;
                    case STATE_PARAGRAPH:
                        // Paragraphs break when a new element encountered
                        _subParser = getElementLines(_preprocessor, STATE_PARAGRAPH);
                        result = parseParagraph(_subParser, _width, _theme);
                        break;
                    case STATE_LIST:
                        // Lists break when a new element encountered
                        _subParser = getElementLines(_preprocessor, STATE_LIST);
                        result = parseList(_subParser, _width, _theme);
                        break;
                    case STATE_IMAGE:
                        // Images are always one line
                        result = parseImage(_preprocessor.next() as String, _width, _theme);
                        break;
                    case STATE_TABLE:
                        // Tables break when a new element encountered
                        _subParser = getElementLines(_preprocessor, STATE_TABLE);
                        result = parseTable(_subParser, _width, _theme);
                        break;
                    case STATE_EOF:
                        // End of file, do nothing
                        _preprocessor.next();
                        break;
                    default:
                        throw new ValueError(
                            "Unhandled case: state=" + _state.toString() + 
                            ", nextState=" + nextState.toString());
                }

                _state = nextState;

                if (result != null) {
                    return result;
                }
            }

            return null;
        }

        function unload() as Void {
            // Unload string resources
            _preprocessor.unload();
        }

        static function getState(line as String?) as Number {
            // Get state from line which will be used to determine how to parse line
            if (line == null) {
                return STATE_EOF;
            }

            var chars = line.toCharArray();
            if (chars.size() == 0) {
                return STATE_WHITESPACE;
            }
            if (chars[0] == '#') {
                return STATE_HEADING;
            }
            if (chars[0] == '|') {
                return STATE_TABLE;
            } 
            if (chars[0] == '!') {
                return STATE_IMAGE;
            }

            var unindented = StringUtils.replaceAll("\t", "", line);
            var unindentedChars = unindented.toCharArray();
            if (unindentedChars.size() == 0) {
                // Needed in case line is just tabs
                return STATE_WHITESPACE;
            } 
            if ((unindentedChars[0] == '-' || unindentedChars[0] == '+' || unindentedChars[0] == '*') &&
                unindentedChars[1] == ' ') {
                return STATE_LIST;
            }

            var dotIndex = unindented.find(".");
            if (dotIndex != null && unindented.length() >= dotIndex + 4 &&
                StringUtils.isAlphanumeric(unindented.substring(0, dotIndex)) &&
                unindented.substring(dotIndex + 1, dotIndex + 3).equals("  ")) {
                return STATE_LIST;
            }

            return STATE_PARAGRAPH;
        }

        function stateEquals(options as {:item as String, :state as Number}) as Boolean {
            // Condition for While iterator
            var line = options[:item];
            var state = options[:state];
            return getState(line) == state;
        }

        (:typecheck(false))
        function getElementLines(lines as Iterator, state as Number) as Iterator {
            // Get block of lines corresponding to a markdown state (element type)
            var remainingLines = new Range(lines, lines.getSeek(), null);
            return new While(remainingLines, {:method => method(:stateEquals), :options => {:state => state}});
        }

        static function parseHeading(line as String, width as Number, theme as Theme) as TextElement {
            // Parse heading line

            var chars = line.toCharArray();
            var headingLevel = 0;
            var state = :startOfLine;
            var exception = false;

            for (var i = 0; i < chars.size() && state != :nonWhitespace; i++) {
                var char = chars[i];
                var nextState;
                if (char == '#') {
                    nextState = :hashtag;
                } else if (char == ' ') {
                    nextState = :whitespace;
                } else if (char == '\t') {
                    nextState = :tab;
                } else {
                    nextState = :nonWhitespace;
                }


                switch (nextState) {
                    case :hashtag:
                        if (!(state == :startOfLine || state == :hashtag)) {
                            exception = true;
                        }
                        headingLevel ++;
                        break;
                    case :whitespace:
                        if (state != :hashtag) {
                            exception = true;
                        }
                        break;
                    case :nonWhitespace:
                        if (state != :whitespace) {
                            exception = true;
                        }
                        break;
                    case :tab:
                        exception = true;
                        break;
                }

                if (exception) {
                    break;
                }

                state = nextState;
            }

            if (exception || headingLevel == 0 || state != :nonWhitespace) {
                throw new ValueError(
                    "Invalid heading definition: \"" + line + "\"");
            }

            line = line.substring(headingLevel + 1, line.length());

            switch (headingLevel) {
                case 1:
                    return new Heading1(line, width, theme, {});
                case 2:
                    return new Heading2(line, width, theme, {});
                case 3:
                    return new Heading3(line, width, theme, {});
                case 4:
                    return new Heading4(line, width, theme, {});
                default:
                    throw new ValueError(
                        "Invalid or unsupported heading level: " + headingLevel.toString());
            }
        }

        static function parseImage(line as String, width as Number, theme as Theme) as Image {

            var altText = null as String?;
            var resourceId = null as String?;

            var state = :startOfLine;
            var lineRemaining = line.toString();  // Copy string

            while (state != :endOfLine && lineRemaining.length() > 0) {

                var trimIndex;

                switch (state) {
                    case :startOfLine:
                        if (!lineRemaining.substring(0, 1).equals("!")) {
                            throw new ValueError(
                                "Image specifier must start with '!'");
                        }
                        trimIndex = 1;
                        state = :altText;
                        break;
                    case :altText:
                        if (!lineRemaining.substring(0, 1).equals("[")) {
                            throw new ValueError(
                                "Brackets '[]' must follow '!' in image specifier");
                        }
                        var rightBracketIndex = lineRemaining.find("]");
                        if (rightBracketIndex == null) {
                            throw new ValueError(
                                "Left bracket '[' not followed by right bracket ']' in image specifier");
                        }
                        altText = lineRemaining.substring(1, rightBracketIndex);
                        trimIndex = rightBracketIndex + 1;
                        state = :resourceId;
                        break;
                    case :resourceId:
                        if (!lineRemaining.substring(0, 1).equals("(")) {
                            throw new ValueError(
                                "Parentheses '()' must follow brackets '[]' in image specifier");
                        }
                        var rightParenIndex = lineRemaining.find(")");
                        if (rightParenIndex == null) {
                            throw new ValueError(
                                "Left parenthesis '(' must be followed by right parenthesis ')' in image specifier");
                        }
                        resourceId = lineRemaining.substring(1, rightParenIndex);
                        trimIndex = rightParenIndex + 1;
                        state = :endOfLine;
                        break;
                    default:
                        throw new ValueError(
                            "Invalid state: " + state.toString());
                }

                lineRemaining = lineRemaining.substring(trimIndex, lineRemaining.length());
            }

            if (state != :endOfLine || !lineRemaining.equals("")) {
                throw new ValueError(
                    "Invalid image specifier: \"" + line + "\"");
            }

            return new Image(resourceId, altText, theme, {});
        }

        static function parseList(lines as Iterator, width as Number, theme as Theme) as List {
            var enumType = MarkdownListParser.getListEnumType(lines.peek() as String, 0);
            if (enumType == List.ENUMTYPE_BULLET) {
                return new UnorderedList(new MarkdownListParser(lines, 0, List.ENUMTYPE_BULLET, width, theme), 0, width, theme, {});
            } else {
                return new OrderedList(new MarkdownListParser(lines, 0, enumType, width, theme), 0, enumType, width, theme, {});
            }
        }

        static function parseParagraph(lines as String or Iterator, width as Number, theme as Theme
            ) as Paragraph {
            return new Paragraph(lines, width, theme, {});
        }

        static function parseTable(lines as Iterator, width as Number, theme as Theme) as Table {
            return new Table(new MarkdownTableParser(lines, width, theme), width, theme, {});
        }
    }


    (:test, :typecheck(false))
    function testMarkdownParserHelloWorld(logger as Logger) as Boolean {
        var theme = Codex.App.THEME_LIGHT;
        var testStr = "## Message\n### Hello\nHello, World!\n";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, 1000, theme);
        return Utils.testMarkdownEquals(obj, [
            new Heading2("Message", 1000, theme, {}),
            new Heading3("Hello", 1000, theme, {}),
            new Paragraph(new ArrayIterator(["Hello, World!"]), 1000, theme, {}),
        ]);
    }


    (:test, :typecheck(false))
    function testMarkdownParserList(logger as Logger) as Boolean {
        var theme = Codex.App.THEME_LIGHT;
        var testStr = "## My list\n1.  Take out the trash\n2.  ...\n3.  Profit.\n";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, 1000, theme);
        return Utils.testMarkdownEquals(obj, [
            new Heading2("My list", 1000, theme),
            new OrderedList(
                new ArrayIterator([
                    new ListItem("Take out the trash", 1000, theme, {}),
                    new ListItem("...", 1000, theme, {}),
                    new ListItem("Profit.", 1000, theme, {}),
                ]),
                0,
                List.ENUMTYPE_ARABIC,
                1000,
                theme,
                {}
            ),
        ]);
    }


    (:test, :typecheck(false))
    function testMarkdownParserListRomanNumerals(logger as Logger) as Boolean {
        var theme = Codex.App.THEME_LIGHT;
        var testStr = "## MEUMALBUM\nI.  EXIMITOQUISQUILIAS\nII.  ...\nIII.  PROFICIO\n";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, 1000, theme);
        return Utils.testMarkdownEquals(obj, [
            new Heading2("MEUMALBUM", 1000, theme, {}),
            new OrderedList(
                new ArrayIterator([
                    new ListItem("EXIMITOQUISQUILIAS", 1000, theme, {}),
                    new ListItem("...", 1000, theme, {}),
                    new ListItem("PROFICIO", 1000, theme, {}),
                ]),
                0,
                List.ENUMTYPE_ROMANUPPER,
                1000,
                theme,
                {}
            ),
        ]);
    }


    (:test)
    function testMarkdownParserSublists(logger as Logger) as Boolean {
        var theme = Codex.App.THEME_LIGHT;

        var testStr;
        var obj;
        var expectedOutput;

        // Top-level list: unordered
        testStr = "- Item\n    + Sub-item\n        * Sub-sub-item\n";
        System.println("Test string: \"" + testStr + "\"");
        expectedOutput = new UnorderedList([
            new ListItem("Item", 1000, theme, {}),
            new UnorderedList([
                new ListItem("Sub-item", 1000, theme, {}),
                new UnorderedList([
                    new ListItem("Sub-sub-item", 1000, theme, {}),
                ], 2, 1000, theme, {}),
            ], 1, 1000, theme, {}),
        ], 0, 1000, theme, {});
        obj = new MarkdownParser(testStr, 1000, theme);
        if (!Utils.testMarkdownEquals(obj, [expectedOutput])) {
            return false;
        }

        // Top-level list: ordered
        testStr = "1.  Item\n    a.  Sub-item\n    b.  Sub-item\n        i.  Sub-sub-item\n        ii.  Sub-sub-item\n    c.  Sub-item\n2.  Item\n    - Sub-item\n    - Sub-item\n";
        System.println("Test string: \"" + testStr + "\"");
        expectedOutput = new OrderedList([
            new ListItem("Item", 1000, theme, {}),
            new OrderedList([
                new ListItem("Sub-item", 1000, theme, {}),
                new ListItem("Sub-item", 1000, theme, {}),
                new OrderedList([
                    new ListItem("Sub-sub-item", 1000, theme, {}),
                    new ListItem("Sub-sub-item", 1000, theme, {}),
                ], 2, List.ENUMTYPE_ROMANLOWER, 1000, theme, {}),
                new ListItem("Sub-item", 1000, theme, {}),
            ], 1, List.ENUMTYPE_ALPHALOWER, 1000, theme, {}),
            new ListItem("Item", 1000, theme, {}),
            new UnorderedList([
                new ListItem("Sub-item", 1000, theme, {}),
                new ListItem("Sub-item", 1000, theme, {}),
            ], 1, 1000, theme, {}),
        ], 0, List.ENUMTYPE_ARABIC, 1000, theme, {});
        obj = new MarkdownParser(testStr, 1000, theme);
        if (!Utils.testMarkdownEquals(obj, [expectedOutput])) {
            return false;
        }

        return true;
    }

    (:test)
    function testMarkdownParserImage(logger as Logger) as Boolean {
        var theme = Codex.App.THEME_LIGHT;
        var testStr = "![Never gonna give you up...](images/rick_roll.png)\n";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, 1000, theme);
        return Utils.testMarkdownEquals(obj, [
            new Image("images/rick_roll.png", "Never gonna give you up...", theme, {}),
        ]);
    }


    (:test)
    function testMarkdownParserListAfterParagraph(logger as Logger) as Boolean {
        var theme = Codex.App.THEME_LIGHT;
        var testStr = "Paragraph\nA.  List item 1\nB.  List item 2";
        System.println("Test string: \"" + testStr + "\"");
        var obj = new MarkdownParser(testStr, 1000, theme);

        var para = obj.next() as Paragraph;
        Test.assert(para instanceof Paragraph);

        // Simulate a load
        // para.getStr().exploit();

        var list = obj.next() as OrderedList;
        Test.assert(list instanceof OrderedList);

        Test.assert(!obj.hasNext());

        var listItems = list.getChildren();
        Test.assert(listItems.next() instanceof ListItem);
        Test.assert(listItems.next() instanceof ListItem);
        Test.assert(!listItems.hasNext());

        return true;
    }
}
