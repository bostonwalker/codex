import Toybox.Lang;


module Codex {

    class MarkdownPreprocessor extends MemoIterator {

        private var _lines as Splitter;

        function initialize(str as String or ResourceId) {
            /*
            Markdown pre-processor. Separates string into lines and normalizes whitespace in each line.

            :param str: Markdown string or resource ID
            */
            MemoIterator.initialize();
            
            _lines = new Splitter(str, "\n");
        }

        protected function nextImpl() as Object? {
            if (_lines.hasNext()) {
                return normalizeWhitespace(_lines.next());
            } else {
                return null;
            }
        }

        function unload() as Void {
            // Unload string resources
            _lines.unload();
        }

        static function normalizeWhitespace(line as String?) as String? {
            // Pre-processing method
            // Removes trailing whitespace and groups spaces into tabs in leading whitespace

            // Replace tabs with 4 spaces
            line = StringUtils.replaceAll("\t", "    ", line);

            // Remove leading and trailing whitespace
            var unindented = StringUtils.removeLeadingWhitespace(line, false);
            var numLeadingWhitespace = line.length() - unindented.length();
            line = StringUtils.removeTrailingWhitespace(unindented, false);

            // Add in tabs as leading whitespace
            var numTabs = (numLeadingWhitespace / 4).toNumber();
            for (var i = 0; i < numTabs; i++) {
                line = '\t' + line;
            }

            return line;
        }
    }
}
