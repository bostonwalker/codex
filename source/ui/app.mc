import Toybox.Lang;
import Toybox.Application;
import Toybox.WatchUi;
import Toybox.Graphics;


module Codex {
    
    var _appInstance as Codex.App? = null;
    function getApp() as Codex.App? {
        return _appInstance;
    }

    class App extends Application.AppBase {
        /*
        Codex App object. Adds view stack management and partial update functionality to Application.AppBase class.
        */

        enum UpdateRequestState {
            UPDATE_REQUEST_STATE_NONE,  // 0 - no update requested
            UPDATE_REQUEST_STATE_PARTIAL,  // 1 - partial UI refresh requested
            UPDATE_REQUEST_STATE_FULL,  // 2 - full UI refresh requested
        }

        // Pre-defined color schemes
        static const THEME_DARK = new Theme(
            new Theme.ColorTheme(
                0x000000,   // Background color
                0xFFFFFF,   // Font color
                0xCFCFCF,   // Accent color (major)
                0x2F2F2F,   // Accent color (minor)
                {}          // Options
            ),
            {}  // Options
        );
        static const THEME_LIGHT = new Theme(
            new Theme.ColorTheme(
                0xFFFFFF,   // Background color
                0x000000,   // Font color
                0x2F2F2F,   // Accent color (major)
                0xCFCFCF,   // Accent color (minor)
                {}          // Options
            ),
            {}  // Options
        );

        private var _viewStack as Array<ViewTuple>;
        private var _updateRequestState as UpdateRequestState;

        function initialize(rootViewTuple as ViewTuple, options as {:resourceSymbolTable as Dictionary}) {
            /*
            :param rootViewTuple: Root view/delegate of Codex app (usually a Menu). Used as initial view for App.
            :param options:
                :resourceSymbolTable: Mapping of String to ResourceId. For specifying Markdown image elements.
            */

            AppBase.initialize();

            _viewStack = [rootViewTuple];
            _updateRequestState = UPDATE_REQUEST_STATE_NONE;

            var resourceSymbolTable = Utils.getDefault(options, :resourceSymbolTable, null) as Dictionary?;
            if (resourceSymbolTable != null) {
                Image.setResourceSymbolTable(resourceSymbolTable);
            }

            Codex._appInstance = self;
        }

        function requestUpdate() as Void {
            if (_updateRequestState < UPDATE_REQUEST_STATE_FULL) {
                _updateRequestState = UPDATE_REQUEST_STATE_FULL;
            }
            WatchUi.requestUpdate();
        }

        function cancelPartialUpdate() as Void {
            if (_updateRequestState == UPDATE_REQUEST_STATE_PARTIAL) {
                _updateRequestState = UPDATE_REQUEST_STATE_NONE;
            }
        }

        function requestPartialUpdate() as Void {
            if (_updateRequestState < UPDATE_REQUEST_STATE_PARTIAL) {
                _updateRequestState = UPDATE_REQUEST_STATE_PARTIAL;
            }
            WatchUi.requestUpdate();
        }

        function isPartialUpdate() as Boolean {
            var result;
            if (_updateRequestState == UPDATE_REQUEST_STATE_PARTIAL) {
                result = true;
            } else {
                result = false;
            }
            // Clear flag
            _updateRequestState = UPDATE_REQUEST_STATE_NONE;
            // Disable partial update mechanism for touchscreens (Vivoactive 4/4S)
            return result && !System.getDeviceSettings().isTouchScreen;  // (TODO: is this still needed??)
        }

        function getInitialView() {
            // Return root view
            return _viewStack[0];
        }

        function getCurrentView() as ViewTuple {
            // Peek into view stack
            return _viewStack[_viewStack.size() - 1];
        }

        function isAppAtRoot() as Boolean {
            // Check if app is at top-level menu
            return _viewStack.size() == 1;
        }

        function returnToRoot() as Void {
            // Return app immediately to top-level menu
            while (!isAppAtRoot()) {
                popView(WatchUi.SLIDE_IMMEDIATE);
            }
        }

        function popView(transition as WatchUi.SlideType) as Void {
            // Wrap WatchUi.popView()
            _viewStack = _viewStack.slice(0, _viewStack.size() - 1);
            WatchUi.popView(transition);
        }

        function pushView(viewTuple as ViewTuple, transition as WatchUi.SlideType) as Boolean {
            // Wrap WatchUi.pushView()
            _viewStack.add(viewTuple);
            if (viewTuple.size() == 2) {
                viewTuple = viewTuple as [WatchUi.Views, WatchUi.InputDelegates];
                WatchUi.pushView(viewTuple[0], viewTuple[1], transition);
            } else {
                WatchUi.pushView(viewTuple[0], null, transition);
            }
            return true;
        }

        function switchToView(viewTuple as ViewTuple, transition as WatchUi.SlideType) as Boolean {
            // Wrap WatchUi.switchToView()
            _viewStack[_viewStack.size() - 1] = viewTuple;
            if (viewTuple.size() == 2) {
                viewTuple = viewTuple as [WatchUi.Views, WatchUi.InputDelegates];
                WatchUi.switchToView(viewTuple[0], viewTuple[1], transition);
            } else {
                WatchUi.switchToView(viewTuple[0], null, transition);
            }
            return false;
        }

        function selectView(viewTuple as ViewTuple) as Boolean {
            /*
            Select view from menu

            :param view: View tuple
            */
            if (viewTuple[0] instanceof Codex.Menu) {
                return pushView(viewTuple, WatchUi.SLIDE_LEFT);
            } else {
                return pushView(viewTuple, WatchUi.SLIDE_IMMEDIATE);
            }
        }

        function exitView() as Boolean {
            // Return to menu from view
            // If view is main menu, exit app
            if (isAppAtRoot()) {
                return _exitApp();
            } else {
                return _exitView();
            }
        }

        private function _exitView() as Boolean {
            // Return to menu from sub-menu or leaf view
            var currentViewTuple = getCurrentView();
            if (currentViewTuple[0] instanceof Codex.Menu) {
                popView(WatchUi.SLIDE_RIGHT);
            } else {
                popView(WatchUi.SLIDE_IMMEDIATE);
            }
            return true;
        }

        private function _exitApp() as Boolean {
            // Exit app using back button
            popView(WatchUi.SLIDE_IMMEDIATE);
            return true;
        }

        function executeIntent(intent as Intent) as Boolean {
            // Execute an intent within the app (do something or go somewhere)
            if (intent instanceof Dictionary) {
                // Construct view dynamically using method and arguments
                var method = (intent as ViewFactory)[:method] as ViewFactoryMethod;
                var options = Utils.getDefault(intent, :options, {}) as Dictionary;
                var viewTuple = method.invoke(options);
                return selectView(viewTuple);
            } else if (intent instanceof Array) {
                return selectView(intent);
            } else if (intent instanceof Method) {
                intent.invoke();
                return true;
            } else {
                throw new ValueError(
                    "`intent` must be a ViewTuple or Dictionary or Method");
            }
        }
    }
}
