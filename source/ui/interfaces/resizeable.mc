import Toybox.Lang;


module Codex {

    typedef Resizeable as interface {
        
        function getWidth() as Number;

        function setWidth(value as Number) as Void;

        function getHeight() as Number;

        function setHeight(value as Number) as Void;
    };
} 