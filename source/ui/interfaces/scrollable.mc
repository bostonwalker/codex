import Toybox.Lang;


module Codex {

    typedef Scrollable as interface {

        function goToStart() as Void;

        function goToEnd() as Void;

        function goToNext() as Void;

        function goToPrevious() as Void;

        function isAtStart() as Boolean;

        function isAtEnd() as Boolean;
    };
} 