import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    typedef Drawable as interface {

        function getWidth() as Number;

        function getHeight() as Number;

        function isVisible() as Boolean;

        // (optional) function setIsVisible(value as BooleanRef) as Void;

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void;
    };
}
