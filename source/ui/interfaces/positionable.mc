import Toybox.Lang;


module Codex {

    typedef Positionable as interface {
        // Object that is positionable on screen (usually, top-level View object)
        function setLocY(value as Number) as Void;
    };
} 