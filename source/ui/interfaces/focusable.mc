import Toybox.Lang;


module Codex {

    typedef Focusable as interface {

        function isFocusable() as Boolean;
        
        function isFocused() as Boolean;

        function setIsFocused(value as BooleanRef) as Void;
    };
}
