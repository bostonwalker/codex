import Toybox.Lang;
using Toybox.Graphics;


module Codex {

    typedef Loadable as interface {

        function load(dc as Graphics.Dc) as Void;

        function unload() as Void;

        // optional: function isLoaded() as Boolean;
    };
}
