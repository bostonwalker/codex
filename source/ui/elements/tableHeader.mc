import Toybox.Lang;


module Codex {

    class TableHeader extends TableCell {

        private var _bottomLineThickness as Number;
        
        function initialize(content as MarkdownElement, width as Number, theme as Theme, options as {
                :isVisible as BooleanRef}) {
            /*
            Markdown table header element

            :param content: Table header content
            :param width: Element width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether ot not to draw this element (default: true)
            */
            TableCell.initialize(content, width, theme, options);

            _bottomLineThickness = theme.markdown.tableHeaderBottomLineThickness;
        }

        function isHeightGreaterThanOrEqualTo(threshold as Number) as Boolean {
            // Extra pixel for thicker bottom line
            return _content.isHeightGreaterThanOrEqualTo(threshold - (_bottomLineThickness + 2 * _paddingTopBottom));
        }

        function getHeight() as Number {
            // Extra pixel for thicker bottom line
            return _content.getHeight() + (_bottomLineThickness + 2 * _paddingTopBottom);
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof TableHeader &&
                TableCell.equals(object);
        }

        function toString() as String {
            return "TableHeader{" +
                "content=" + _content + "}";
        }
    }
}
