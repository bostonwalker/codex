import Toybox.Graphics;
import Toybox.Lang;


module Codex {

    class Table extends BaseElement {

        private var _width as Number;
        private var _borderColor as Number;
        private var _backgroundColor as Number;
        private var _spacingAfter as Number;

        function initialize(rows as Array<TableRow> or Iterator, width as Number, theme as Theme, options as {
            :isVisible as BooleanRef}) {
            /*
            Markdown table element

            :param rows: Table row elements
            :param width: Table width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether ot not to draw this element (default: true)
            */
            if ((rows instanceof Array && rows.size() == 0) ||
                (rows has :hasNext && !rows.hasNext())) {
                throw new ValueError(
                    "Cannot create Table with zero rows");
            }
            BaseElement.initialize(rows, options);

            _width = width;
            _borderColor = theme.color.majorAccent;
            _backgroundColor = theme.color.background;
            _spacingAfter = theme.markdown.tableSpacingAfter;
        }

        function getWidth() as Number {
            return _width;
        }

        function getSpacingAfter() as Number {
            return _spacingAfter;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {

            if (y >= 0 && y < dc.getHeight()) {
                // Draw top line
                dc.setColor(_borderColor, _backgroundColor);
                dc.setPenWidth(1);
                dc.drawLine(x, y, x + getWidth(), y);
            }

            // Add one pixel for border thickness
            BaseElement.draw(dc, x, y + 1);
        }

        protected function isHeightGreaterThanOrEqualToImpl(threshold as Number) as Boolean {
            // Add one pixel for border thickness
            return BaseElement.isHeightGreaterThanOrEqualToImpl(threshold - 1);
        }

        protected function getHeightImpl() as Number {
            // Add one pixel for border thickness
            return BaseElement.getHeightImpl() + 1;
        }

        function isIdenticalTo(object as Object) as Boolean {
            return
                object instanceof Table && 
                BaseElement.equals(object);
        }

        function toString() as String {
            var result = "Table{rows=[";
            var rows = getChildren();
            while (rows.hasNext()) {
                result += rows.toString() + ",";
            }
            result += "]}";
            return result;
        }
    }
}
