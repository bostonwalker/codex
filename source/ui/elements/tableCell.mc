import Toybox.Lang;
import Toybox.Graphics;


module Codex {

    class TableCell {  // implements MarkdownELement

        protected var _content as MarkdownElement;
        protected var _width as Number;
        protected var _isVisible as Boolean;
        protected var _requestReloadCallback as Callback?;

        protected var _paddingTopBottom as Number;
        protected var _paddingSides as Number;

        function initialize(content as MarkdownElement, width as Number, theme as Theme, options as {
                :isVisible as BooleanRef, :requestReloadCallback as Callback}) {
            /*
            Markdown table cell element

            :param content: Table cell content
            :param width: Cell width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether ot not to draw this element (default: true)
                :requestReloadCallback: Callback to parent to request reload when this TableCell changes size
            */
            _content = content;
            _width = width;

            _isVisible = Ref.linkBoolean(self, :setIsVisible, Utils.getDefault(options, :isVisible, true) as BooleanRef);
            _requestReloadCallback = Utils.getDefault(options, :requestReloadCallback, null) as Callback?;

            _paddingTopBottom = theme.markdown.tableCellPaddingTopBottom;
            _paddingSides = theme.markdown.tableCellPaddingSides;
        }

        function getContent() as MarkdownElement {
            return _content;
        }

        function isVisible() as Boolean {
            return _isVisible;
        }

        function setIsVisible(value as BooleanRef) as Void {
            _isVisible = Ref.linkBoolean(self, :setIsVisible, value);
            _requestParentReload();
        }

        function setRequestReloadCallback(value as Callback) as Void {
            _requestReloadCallback = value;
        }

        function load(dc as Graphics.Dc) as Void {
            _content.load(dc);
        }

        function loadYRange(dc as Graphics.Dc, yMin as Number, yMax as Number?) as Void {
            _content.loadYRange(dc, yMin, yMax != null ? yMax - 2 * _paddingTopBottom : null);
        }

        function unload() as Void {
            _content.unload();
            _requestParentReload();
        }

        protected function _requestParentReload() as Void {
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            _content.draw(dc, x + _paddingSides, y + _paddingTopBottom);
        }

        function getWidth() as Number {
            return _width;
        }

        function getHeight() as Number {
            return _content.getHeight() + _paddingTopBottom * 2;
        }

        function isHeightGreaterThanOrEqualTo(threshold as Number) as Boolean {
            return _content.isHeightGreaterThanOrEqualTo(threshold - _paddingTopBottom * 2);
        }

        function isHeightKnown() as Boolean {
            return _content.isHeightKnown();
        }

        function getSpacingAfter() as Number {
            return 0;
        }

        function hasScrollPoints() as Boolean {
            return _content.hasScrollPoints();
        }

        function getScrollPoint(y as Number) as Number {
            return _content.getScrollPoint(y - _paddingTopBottom) + _paddingTopBottom;
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof TableCell &&
                (_content as Object).equals(object.getContent() as Object);
        }

        function toString() as String {
            return "TableCell{" +
                "content=" + _content + "}";
        }
    }
}
