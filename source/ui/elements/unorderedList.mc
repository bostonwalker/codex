import Toybox.Lang;


module Codex {

    class UnorderedList extends List {

        function initialize(items as Array<MarkdownElement> or Iterator, level as Number,
            width as Number, theme as Theme, options as {:isVisible as BooleanRef}) {
            /*
            An unordered list. Items are enumerated by bullets.

            :param items: An array or iterator of ListItem objects
            :param level: List nesting level
            :param width: Element width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether ot not to draw this element (default: true)
            */
            List.initialize(items, level, List.ENUMTYPE_BULLET, width, theme, options);
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof UnorderedList &&
                List.equals(object);
        }

        function toString() as String {
            return "UnorderedList{" +
                "children=" + getChildren().toString() + "," +
                "level=" + getLevel().toString() + "}";
        }
    }
}
