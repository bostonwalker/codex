import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Lang;


module Codex {

    class Image {  // implements MarkdownElement

        static var _resourceSymbolTable as Dictionary<String, ResourceId> = {};

        protected var _resourceId as ResourceId;
        protected var _altText as String?;
        protected var _spacingAfter as Number;

        protected var _isVisible as Boolean;
        protected var _requestReloadCallback as Callback?;

        protected var _bitmapResource as WatchUi.BitmapResource?;
        protected var _bitmap as WatchUi.Bitmap?;
        protected var _dims as [Number, Number]?;

        function initialize(resource as ResourceId or String, altText as String?, theme as Theme, options as {
                :isVisible as BooleanRef, :requestReloadCallback as Callback}) {
            /*
            A Markdown image element

            :param resource: Bitmap resource (if string format, will be looked up in resource symbol table)
            :param altText: Alternative text
            :param theme: Styling options
            :param options:
                :isVisible: Whether or not to draw this element (default: true)
                :requestReloadCallback: Callback to parent to request reload when this Image changes size
            */
            if (resource instanceof String) {
                // Resolve string resource ID lookup
                if (!_resourceSymbolTable.hasKey(resource)) {
                    throw new KeyError(
                        "String \"" + resource + "\" not present in resource table");

                }
                resource = _resourceSymbolTable[resource];
            }
            _resourceId = resource;
            _altText = altText;

            _isVisible = Ref.linkBoolean(self, :setIsVisible, Utils.getDefault(options, :isVisible, true) as BooleanRef);
            _requestReloadCallback = Utils.getDefault(options, :requestReloadCallback, null) as Callback?;

            _spacingAfter = theme.markdown.imageSpacingAfter;

            _bitmap = null;
        }

        function getResourceId() as ResourceId {
            return _resourceId;
        }

        function getAltText() as String? {
            return _altText;
        }

        function load(dc as Graphics.Dc) as Void {
            // Load bitmap (if not already loaded)
            if (_bitmap == null) {
                _bitmapResource = WatchUi.loadResource(_resourceId);
                _bitmap = new WatchUi.Bitmap({:bitmap => _bitmapResource});
                _dims = _bitmap.getDimensions();
            }
        }

        function loadYRange(dc as Graphics.Dc, yMin as Number, yMax as Number?) as Void {
            load(dc);
        }

        function unload() as Void {
            // Unload bitmap content
            _bitmapResource = null;
            _bitmap = null;
            _requestParentReload();
        }

        protected function _requestParentReload() as Void {
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            // TODO: implement alt text when bitmap not loaded
            dc.drawBitmap(x, y, _bitmapResource);
        }

        function getWidth() as Number {
            if (_dims == null) {
                throw new ValueError(
                    "Image not loaded");
            } else { 
                return _dims[0];
            }
        }

        function getHeight() as Number {
            if (_dims == null) {
                throw new ValueError(
                    "Image not loaded");
            } else { 
                return _dims[1];
            }
        }

        function isHeightGreaterThanOrEqualTo(threshold as Number) as Boolean {
            return _dims[1] >= threshold;
        }

        function isHeightKnown() as Boolean {
            return _dims != null;
        }

        function getSpacingAfter() as Number {
            return _spacingAfter;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function getScrollPoint(y as Number) as Number {
            // Get point 0 < scollPoint <= y <= height that will be aligned with top edge of screen
            if (_dims == null) {
                throw new ValueError(
                    "Image not loaded");
            } 
            if (y <= 0) {
                return 0;
            } else {
                return y <= _dims[1] ? y : _dims[1];
            }
        }

        function isVisible() as Boolean {
            return _isVisible;
        }

        function setIsVisible(value as BooleanRef) as Void {
            _isVisible = Ref.linkBoolean(self, :setIsVisible, value);
            _requestParentReload();
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof Image &&
                _resourceId.equals(object.getResourceId());
        }

        function toString() as String {
            return "Image{" +
                "resourceId=\"" + getResourceId() + "\"}";
        }

        // Garmin... I shouldn't need something like this just to refer to resources
        static function setResourceSymbolTable(resourceSymbolTable as Dictionary) as Void {
            _resourceSymbolTable = resourceSymbolTable;
        }
    }
}
