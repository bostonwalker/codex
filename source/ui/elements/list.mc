import Toybox.Lang;
import Toybox.Math;
import Toybox.Graphics;
import Toybox.System;
import Toybox.Test;


module Codex {

    class List extends BaseElement {

        enum {
            ENUMTYPE_BULLET,
            ENUMTYPE_ARABIC,
            ENUMTYPE_ROMANLOWER,
            ENUMTYPE_ROMANUPPER,
            ENUMTYPE_ALPHALOWER,
            ENUMTYPE_ALPHAUPPER
        }

        private var _level as Number;
        private var _enumType as Number;
        private var _width as Number;
        private var _textColor as Number;
        private var _backgroundColor as Number;
        private var _labelFont as Graphics.FontReference or Graphics.FontDefinition;
        private var _listSpacingAfter as Number;
        private var _listItemSpacingAfter as Number;

        private var _indent as Number?;
        private var _labelWidth as Number?;
        private var _labelHeight as Number?;

        function initialize(items as Array<MarkdownElement> or Iterator, level as Number, enumType as Number,
            width as Number, theme as Theme, options as {:isVisible as BooleanRef}) {
            /*
            List of elements

            :param items: List elements (paragraphs)
            :param level: List nesting level
            :param enumType: Enumeration method (bullets, arabic numerals, roman numerals, lowercase alphabet, etc.)
            :param width: Element width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether ot not to draw this element (default: true)
            */
            BaseElement.initialize(items, options);

            if (level > 2) {
                throw new ValueError(
                    "Cannot nest lists more than 2 level(s) deep");
            }

            _level = level;
            _enumType = enumType;
            _width = width;
            _textColor = theme.color.text;
            _backgroundColor = theme.color.background;
            _labelFont = theme.markdown.listLabelFont;
            _listSpacingAfter = theme.markdown.listSpacingAfter;
            _listItemSpacingAfter = theme.markdown.listItemSpacingAfter;

            _indent = calculateIndent(theme);
            _labelWidth = calculateLabelWidth(theme, enumType);
            _labelHeight = calculateLabelHeight(theme);
        }

        function getLevel() as Number {
            return _level;
        }

        function getEnumType() as Number {
            return _enumType;
        }

        function getWidth() as Number {
            return _width;
        }

        static function calculateIndent(theme) {
            var labelFontHeight = calculateLabelHeight(theme);
            return (labelFontHeight * 1.32).toNumber();
        }

        static function calculateLabelWidth(theme as Theme, enumType as Number) as Number {
            var labelFontHeight = calculateLabelHeight(theme);
            if (enumType == List.ENUMTYPE_BULLET) {
                return (labelFontHeight * 0.64).toNumber();
            } else {
                return (labelFontHeight * 1.32).toNumber();
            }
        }

        static function calculateLabelHeight(theme as Theme) as Number {
            return Graphics.getFontHeight(theme.markdown.listLabelFont);
        }

        function loadYRange(dc as Graphics.Dc, yMin as Number, yMax as Number?) as Void {
            if (yMax == null) {
                yMax = 0x7FFFFFFF;  // 32-bit signed max value
            }

            var yTop = 0;
            while (yTop < yMax && _children.hasNext()) {
                
                var child = _children.next() as MarkdownElement;

                if (child.isHeightKnown() && yTop + child.getHeight() < yMin) {
                    // Child is not on screen, ignore
                } else {
                    // Unknown if child is on screen, load up to screen height
                    if (child instanceof ListItem) {
                        // Case: list item
                        child.loadYRange(dc, yMin - yTop, yMax - yTop);
                    } else {
                        // Case: nested list
                        child.loadYRange(dc, yMin - yTop, yMax - yTop);
                    }
                    if (!child.isHeightKnown() && child.isHeightGreaterThanOrEqualTo(yMax - yTop)) {
                        // Content has been loaded up to bottom of screen, no need to continue
                        break;
                    }
                }

                // Accumulate value
                yTop += child.getHeight();
                if (_children.hasNext()) {
                    yTop += child.getSpacingAfter();
                } else {
                    // Full height reached, memoize value
                    _height = yTop;
                    break;
                }
            }

            // Unload remaining off-screen children without generating new items, i.e. triggering Markdown parsing
            var memo = (_children as MemoIterator).getMemo();
            for (var i = 0; i < memo.size(); i++) {
                memo[i].unload();
            }

            _children.reset();
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            
            var itemCounter = 1;

            var yTop = y;
            var yMax = dc.getHeight();

            while (yTop < yMax && _children.hasNext()) {

                var item = _children.next() as MarkdownElement;

                if (!item.isHeightKnown() || yTop + item.getHeight() >= 0) {
                    // Item is on-screen or possibly on-screen

                    if (item instanceof ListItem) {
                        // List item
                        if (yTop + _labelHeight > 0) {
                            // Draw label
                            var label = _label(itemCounter);

                            dc.setColor(_textColor, _backgroundColor);
                            dc.drawText(x, yTop, _labelFont, label, Graphics.TEXT_JUSTIFY_LEFT);
                        }

                        // Draw item
                        item.draw(dc, x + _labelWidth, yTop);
                    } else {
                        // Item is nested list
                        item.draw(dc, x + _indent, yTop);
                    }

                    if (!item.isHeightKnown() && item.isHeightGreaterThanOrEqualTo(yMax - yTop)) {
                        // Content has been drawn up to bottom of screen, no need to continue
                        break;
                    }
                }

                yTop += item.getHeight();
                if (_children.hasNext()) {
                    yTop += item.getSpacingAfter();
                }

                if (item instanceof ListItem) {
                    itemCounter ++;
                }
            }

            _children.reset();
        }

        private function _label(number as Number) as String {
            // Get the nth label for the list
            switch(_enumType) {
                case List.ENUMTYPE_BULLET:
                    switch (_level) {
                        case 0:
                            return "•";
                        case 1:
                            return "•";
                        case 2:
                            return "•";
                        default:
                            throw new ValueError(
                                "Invalid nesting level: " + _level.toString());
                    }
                case List.ENUMTYPE_ARABIC:
                    return number.toString() + ".";
                case List.ENUMTYPE_ROMANLOWER:
                    return intToRoman(number).toLower() + ".";
                case List.ENUMTYPE_ROMANUPPER:
                    return intToRoman(number) + ".";
                case List.ENUMTYPE_ALPHALOWER:
                    return intToAlpha(number).toLower() + ".";
                case List.ENUMTYPE_ALPHAUPPER:
                    return intToAlpha(number) + ".";
                default:
                    throw new ValueError(
                        "Invalid enum type: " + _enumType.toString());
            }
        }

        function getSpacingAfter() as Number {
            // No extra spacing after if list is a nested list
            return _level == 0 ? _listSpacingAfter : _listItemSpacingAfter;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof List &&
                BaseElement.equals(object) &&
                _level == object.getLevel() &&
                _enumType == object.getEnumType();
        }

        function toString() as String {
            return "List{" +
                "children=\"" + _children.toString() + "\"," +
                "level=" + _level.toString() + "," +
                "enumType=" + _enumType.toString() + "}";
        }

        static function intToRoman(number as Number) as String {
            /*
            Convert a natural number to a Roman numeral

            Adapted from: https://www.oreilly.com/library/view/python-cookbook/0596001673/ch03s24.html

            :param number: Natural number (range: [1, 3999])
            */
            if (number < 1 || number > 3999) {
                throw new ValueError(
                    "Input out of range: " + number.toString());
            }

            var ROMAN_INTS = [1000, 900, 500, 400,  100, 90,   50,  40,   10,  9,    5,   4,    1  ];
            var ROMAN_NUMS = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];

            var result = "";
            for (var i = 0; i < ROMAN_INTS.size(); i++) {
                var int = ROMAN_INTS[i];
                var numeral = ROMAN_NUMS[i];
                var count = (number / int).toNumber();
                for (var j = 0; j < count; j++) {
                    result += numeral;
                }
                number -= int * count;
            }

            return result;
        }

        static function romanToInt(numeral as String) as Number {
            /*
            Convert a Roman numeral to a natural number

            :param numeral: Roman numeral (range: [1 ("I"), 3999 ("MMMCMXCIX")])
            */
            var ROMAN_INTS = [1000, 900, 500, 400,  100, 90,   50,  40,   10,  9,    5,   4,    1  ];
            var ROMAN_NUMS = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];

            var result = 0;
            var i = 0;
            while (numeral.length() > 0 && i < ROMAN_INTS.size()) {
                var numGroup = ROMAN_NUMS[i];
                var numGroupLen = numGroup.length();
                if (numGroupLen <= numeral.length() &&
                    numeral.substring(0, numGroupLen).equals(numGroup)) {
                    // Numeral starts with num group
                    result += ROMAN_INTS[i];
                    numeral = numeral.substring(numGroupLen, numeral.length());
                } else {
                    // Move search to next numeral group
                    i++;
                }
            }

            if (numeral.length() > 0) {
                throw new ValueError(
                    "Invalid or out-of-range input");
            }

            return result;
        }

        static function intToAlpha(number as Number) as String {
            /*
            Convert a natural number to an alphabetical numbering

            :param number: Natural number (range: [1, infinity))
            */
            var result = "";
            while (number > 0) {
                var rem = number % 26;
                result = ('A' + (rem - 1)).toString() + result;
                number = (number - rem) / 26;
            }
            return result;
        }
    }

    (:test)
    function testIntToRoman(logger as Logger) as Boolean {
        var testInput =  [1,   3,     99,     554,    3999       ];
        var testOutput = ["I", "III", "XCIX", "DLIV", "MMMCMXCIX"];
        for (var i = 0; i < testInput.size(); i++) {
            var result = List.intToRoman(testInput[i]);
            if (!result.equals(testOutput[i])) {
                System.println("Incorrect test output: got " + result.toString() +
                            ", expected: " + testOutput[i].toString());
                return false;
            }
        }
        return true;
    }

    (:test)
    function testRomanToInt(logger as Logger) as Boolean {
        var testInput =  ["I", "III", "XCIX", "DLIV", "MMMCMXCIX"];
        var testOutput = [1,   3,     99,     554,    3999       ];
        for (var i = 0; i < testInput.size(); i++) {
            var result = List.romanToInt(testInput[i]);
            if (!result.equals(testOutput[i])) {
                System.println("Incorrect test output: got " + result.toString() +
                            ", expected: " + testOutput[i].toString());
                return false;
            }
        }
        return true;
    }

    (:test)
    function testIntToAlpha(logger as Logger) as Boolean {
        var testInput =  [1,   3,   99,   554,  3999];
        var testOutput = ["A", "C", "CU", "UH", "EWU"];
        for (var i = 0; i < testInput.size(); i++) {
            var result = List.intToAlpha(testInput[i]);
            if (!result.equals(testOutput[i])) {
                System.println("Incorrect test output: got " + result.toString() +
                            ", expected: " + testOutput[i].toString());
                return false;
            }
        }
        return true;
    }
}
