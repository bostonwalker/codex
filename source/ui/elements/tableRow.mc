import Toybox.Lang;
import Toybox.Graphics;


module Codex {

    class TableRow extends BaseElement {

        private var _width as Number;
        private var _borderColor as Number;
        private var _backgroundColor as Number;
        private var _headerBottomLineThickness as Number;

        private var _isHeaderRow as Boolean?;

        function initialize(cells as Array<TableCell> or Iterator, width as Number, theme as Theme, options as {
                :isVisible as BooleanRef}) {
            /*
            Markdown table header element

            :param cells: Table cells
            :param width: Element width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether ot not to draw this element (default: true)
            */
            if ((cells instanceof Array && cells.size() == 0) ||
                (cells has :hasNext && !cells.hasNext())) {
                throw new ValueError(
                    "Cannot create TableRow with zero cells");
            }
            BaseElement.initialize(cells, options);

            _width = width;
            _borderColor = theme.color.majorAccent;
            _backgroundColor = theme.color.background;
            _headerBottomLineThickness = theme.markdown.tableHeaderBottomLineThickness;
        }

        function getWidth() as Number {
            return _width;
        }

        function load(dc as Graphics.Dc) as Void {
            var cells = getChildren().asArray();
            for (var i = 0; i < cells.size(); i++) {
                var cell = cells[i] as MarkdownElement;
                cell.load(dc);
            }
        }

        function loadYRange(dc as Graphics.Dc, yMin as Number, yMax as Number?) as Void {

            var cells = getChildren().asArray();

            _isHeaderRow = cells.size() > 0 && cells[0] instanceof TableHeader;
            var bottomLineThickness = _isHeaderRow ? _headerBottomLineThickness : 1;

            _height = 1;

            for (var i = 0; i < cells.size(); i++) {
                var cell = cells[i];
                cell.load(dc);

                var cellHeight = cell.getHeight();
                _height = cellHeight + bottomLineThickness > _height ? cellHeight + bottomLineThickness : _height;
            }
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            
            var cells = _children.asArray();
            var avgCellWidth = (getWidth() - 1).toFloat() / cells.size();

            // Draw cell content
            for (var i = 0; i < cells.size(); i++) {
                var cell = cells[i];
                if (cell.isVisible()) {
                    cell.draw(dc, x + Math.round(i * avgCellWidth).toNumber() + 1, y);
                }
            }

            // Draw cell borders
            dc.setColor(_borderColor, _backgroundColor);
            dc.setPenWidth(1);
            for (var i = 0; i <= cells.size(); i++) {
                var xLine = x + Math.round(i * avgCellWidth).toNumber();
                dc.drawLine(xLine, y, xLine, y + _height - 1);
            }

            // Draw bottom line
            if (_isHeaderRow) {
                dc.setPenWidth(_headerBottomLineThickness);
                dc.drawLine((x + (_headerBottomLineThickness) / 2).toNumber(), y + _height - 1, x + getWidth(), y + _height - 1);
            } else {
                dc.drawLine(x, y + _height - 1, x + getWidth(), y + _height - 1);
            }
        }

        protected function isHeightGreaterThanOrEqualToImpl(threshold as Number) as Boolean {
            // Or over cells
            var result = false;
            while (_children.hasNext()) {
                var cell = _children.next() as MarkdownElement;
                if (cell.isHeightGreaterThanOrEqualTo(threshold)) {
                    result = true;
                    break;
                }
            }
            _children.reset();
            return result;
        }

        protected function getHeightImpl() as Number {
            // Maximum over cells
            var result = 0;
            while (_children.hasNext()) {
                var cell = _children.next() as MarkdownElement;
                var cellHeight = cell.getHeight();
                result = cellHeight > result ? cellHeight : result;
            }
            _children.reset();
            return result;
        }

        function getSpacingAfter() as Number {
            return 0;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function getScrollPoint(y as Number) as Number {
            var scrollPoint = 0;
            while (_children.hasNext()) {
                var cell = _children.next() as MarkdownElement;
                var cellScrollPoint = cell.getScrollPoint(y);
                scrollPoint = cellScrollPoint > scrollPoint ? cellScrollPoint : scrollPoint;
            }
            _children.reset();
            return scrollPoint;
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof TableRow &&
                BaseElement.equals(object);
        }

        function toString() as String {
            var result = "TableRow{cells=[";
            while (_children.hasNext()) {
                var cell = _children.next();
                result += cell.toString() + ",";
            }
            _children.reset();
            result += "]}";
            return result;
        }
    }
}
