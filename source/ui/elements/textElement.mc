import Toybox.Lang;
using Toybox.Graphics;
using Toybox.Math;
using Toybox.System;


/*
Mode of TextWrapper.

NO_WRAP: Display string as-is. Width of element is defined as the width of the string.
WRAP_SINGLE_LINE: Display text up to `maxWidth` pixels wide. Width of element is defined as `maxWidth`.
WRAP_MULTI_LINE: Display text up to `maxWidth` pixels wide. If text is wider, continue on new line. Width of element is defined as `maxWidth`.
*/
enum Mode {
    NO_WRAP,
    WRAP_SINGLE_LINE,
    WRAP_MULTI_LINE,
}


module Codex {

    class TextElement {  // implements MarkdownElement

        // Amount of spacing before a horizontal rule
        static const HORIZONTAL_RULE_Y_SPACING = 3;

        // Configuration variables
        protected var _str as String or Iterator;
        protected var _font as Graphics.FontReference or Graphics.FontDefinition;
        protected var _textColor as Number;
        protected var _backgroundColor as Number;

        protected var _lineHeight as Number?;
        protected var _width as Number?;
        protected var _maxWidth as Number?;
        protected var _horizontalJustification as Graphics.TextJustification;
        protected var _isVerticallyCentered as Boolean;
        protected var _spacingAfter as Number;
        protected var _horizontalRule as Boolean;
        protected var _horizontalRuleColor as Number;
        protected var _fillWidthWithBackgroundColor as Boolean;
        protected var _isVisible as Boolean;
        protected var _requestReloadCallback as Callback?;
        protected var _widthReferenceString as String?;

        // State variables
        private var _mode as Mode;
        private var _height as Number?;
        private var _xOffset as Number?;
        // private var _yOffset as Number?;
        private var _textWidth as Number?;
        private var _wrappedText as String or TextWrapper or Null;
        private var _isLoaded as Boolean;

        function initialize(str as String or ResourceId or Iterator, font as Graphics.FontReference or Graphics.FontDefinition,
            textColor as Number, backgroundColor as Number, options as {
                :multiline as Boolean, :width as Number, :maxWidth as Number, :justification as Graphics.TextJustification or Number,
                :spacingAfter as Number, :horizontalRule as Boolean, :horizontalRuleColor as Number, :fillWidthWithBackgroundColor as Boolean,
                :isVisible as BooleanRef, :requestReloadCallback as Callback, :widthReferenceString as String}) {
            /*
            Generic text element

            :param str: Text. Can be a singular string or a collection of lines.
            :param font: Font to use
            :param textColor: Text color
            :param backgroundColor: Background color
            :param options:
                :width: If provided, fix the width of this text element. Cannot provide along with `maxWidth`.
                :maxWidth: Maximum width of text to display. Cannot provide along with `width`.
                :multiline: If `maxWidth` is provided, whether to allow multiple lines or not (default: false).
                :justification: Text justification (default: Graphics.TEXT_JUSTIFY_LEFT)
                :spacingAfter: Amount of spacing after text (default: 0)
                :horizontalRule: Whether to draw a horizontal rule below text (default: false)
                :horizontalRuleColor: Color of horizontal rule (default: text color)
                :fillWidthWithBackgroundColor: If true, fill the entire width of this text element using the background color
                :isVisible: Whether ot not to draw this element (default: true)
                :requestReloadCallback: Callback to parent to request reload when this TextElement changes size
                :widthReferenceString: If provided, fix the width of this TextElement at the width of this string when loading
            */
            if ((str instanceof String) or (str instanceof ResourceId)) {
                _str = Utils.resolveString(str);
            } else {
                _str = str;
            }
            _font = font;
            _textColor = textColor;
            _backgroundColor = backgroundColor;

            if (options.hasKey(:width)) {
                _width = options[:width];
                if (options.hasKey(:maxWidth)) {
                    throw new ValueError(
                        "Cannot supply both `width` and `maxWidth`");
                }
                if (options.hasKey(:widthReferenceString)) {
                    throw new ValueError(
                        "Cannot supply both `width` and `widthReferenceString`");
                }
                _maxWidth = _width;
            } else if (options.hasKey(:maxWidth)) {
                _maxWidth = options[:maxWidth];
                if (options.hasKey(:widthReferenceString)) {
                    throw new ValueError(
                        "Cannot supply both `maxWidth` and `widthReferenceString`");
                }
            }
            
            var multiline = Utils.getDefault(options, :multiline, false) as Boolean?;
            if (multiline) {
                if (options.hasKey(:widthReferenceString)) {
                    throw new ValueError(
                        "Cannot supply `widthReferenceString` if `multiline` is true");
                }
                if (_maxWidth == null) {
                    throw new ValueError(
                        "Must supply `width` or `maxWidth` for multiline TextElement");
                }
            } else {
                if (str has :hasNext) {
                    throw new ValueError(
                        "Cannot supply Iterator for single-line TextElement");
                }
            }
            var justification = Utils.getDefault(options, :justification, Graphics.TEXT_JUSTIFY_LEFT) as Graphics.TextJustification;
            _horizontalJustification = (justification & ~Graphics.TEXT_JUSTIFY_VCENTER) as Graphics.TextJustification;
            _isVerticallyCentered = (justification & Graphics.TEXT_JUSTIFY_VCENTER) == Graphics.TEXT_JUSTIFY_VCENTER;  // TODO: do something with this value

            _spacingAfter = Utils.getDefault(options, :spacingAfter, 0) as Number;
            _horizontalRule = Utils.getDefault(options, :horizontalRule, false) as Boolean;
            _horizontalRuleColor = Utils.getDefault(options, :horizontalRuleColor, textColor) as Number;
            _fillWidthWithBackgroundColor = Utils.getDefault(options, :fillWidthWithBackgroundColor, false) as Boolean;
            _isVisible = Ref.linkBoolean(self, :setIsVisible, Utils.getDefault(options, :isVisible, true) as BooleanRef);
            _requestReloadCallback = Utils.getDefault(options, :requestReloadCallback, null) as Callback?;
            _widthReferenceString = Utils.getDefault(options, :widthReferenceString, null) as String?;

            if (multiline) {
                _mode = WRAP_MULTI_LINE;
            } else if (_maxWidth != null) {
                _mode = WRAP_SINGLE_LINE;
            } else {
                _mode = NO_WRAP;
            }

            _isLoaded = false;
        }

        function getStr() as String or Iterator {
            return _str;
        }

        function setStr(str as String or Iterator) as Void {
            _str = str;
            unload();
        }

        function getFont() as Graphics.FontReference or Graphics.FontDefinition {
            return _font;
        }

        function setFont(value as Graphics.FontReference or Graphics.FontDefinition) as Void {
            _font = value;
            unload();
        }

        function getFontHeight() as Number {
            return Graphics.getFontHeight(_font);
        }

        function setTextColor(textColor as Number) as Void {
            _textColor = textColor;
        }

        function setBackgroundColor(backgroundColor as Number) as Void {
            _backgroundColor = backgroundColor;
        }

        function isVisible() as Boolean {
            return _isVisible;
        }

        function setIsVisible(value as BooleanRef) as Void {
            _isVisible = Ref.linkBoolean(self, :setIsVisible, value);
            _requestParentReload();
        }

        function setRequestReloadCallback(value as Callback) as Void {
            _requestReloadCallback = value;
        }

        function getWidth() as Number {
            if (_width != null) {
                return _width;
            } else {
                if (!_isLoaded) {
                    throw new ValueError(
                        "TextElement not loaded");
                }
                return _textWidth;
            }
        }

        function setWidth(value as Number) as Void {
            _width = value;
            if (_maxWidth == null) {
                _maxWidth = _width;
                _mode = WRAP_SINGLE_LINE;
            } else if (_maxWidth > _width) {
                _maxWidth = _width;
            }
            unload();
        }

        function getHeight() as Number {
            if (_height == null) {
                if (!_isLoaded) {
                    throw new ValueError(
                        "TextElement not loaded");
                }
                if (_mode == WRAP_MULTI_LINE) {
                    // Need to load all lines to determine height
                    var numLines = _wrappedText.asArray().size();
                    _height = numLines * _lineHeight;
                } else {
                    // Text can only be one line tall
                    _height = _lineHeight;
                }
            }
            return _height;
        }

        function load(dc as Graphics.Dc) as Void {
            // Load all text
            if (_mode == NO_WRAP && _widthReferenceString != null) {
                // Use width reference string to fix width of single-line TextElement
                _lineHeight = Graphics.getFontHeight(_font);
                _textWidth = dc.getTextWidthInPixels(_widthReferenceString, _font) - 1;
                _width = _textWidth;
                _isLoaded = true;
                _xOffset = _calculateXOffset();
            } else {
                loadYRange(dc, 0, null);
            }
        }

        function loadYRange(dc as Graphics.Dc, yMin as Number, yMax as Number?) as Void {
            if (yMax == null) {
                yMax = 0x7FFFFFFF;  // 32-bit signed max value
            }

            _lineHeight = Graphics.getFontHeight(_font);
            
            // Load text (if not already loaded)
            if (_mode == NO_WRAP) {
                // No wrapping necessary, check width though
                _textWidth = dc.getTextWidthInPixels(_str, _font);
            } else if (_mode == WRAP_SINGLE_LINE) {
                // Wrap single line
                _wrappedText = StringUtils.wrapLine(_str, _maxWidth, _font, dc);
                if (_width == null) {
                    // Only track text width if width is not fixed
                    _textWidth = dc.getTextWidthInPixels(_wrappedText, _font);
                }
            } else {
                // _mode == WRAP_MULTI_LINE
                // Initialize text wrapper object
                if (_wrappedText == null) {
                    _wrappedText = new TextWrapper(_str, _maxWidth, _font);
                }
                _wrappedText.setDc(dc);

                // Pre-load lines
                var yTop = 0;
                if (_width == null) {
                    // Only track text width if width is not fixed
                    _textWidth = 0;
                }
                while (yTop < yMax) {
                    var line = _wrappedText.next();
                    if (_width == null) {
                        var lineWidth = dc.getTextWidthInPixels(line, _font);
                        if (lineWidth > _textWidth) {
                            _textWidth = lineWidth;
                        }
                    }
                    yTop += _lineHeight;
                    if (!_wrappedText.hasNext()) {
                        _height = yTop;
                        break;
                    }
                }
                _wrappedText.reset();
            }
            _isLoaded = true;
            _xOffset = _calculateXOffset();
            //_yOffset = _calculateYOffset();
        }

        protected function _calculateXOffset() as Number {
            // Implement justification
            if (_horizontalJustification == Graphics.TEXT_JUSTIFY_LEFT) {
                return 0;
            } else if (_horizontalJustification == Graphics.TEXT_JUSTIFY_CENTER) {
                return getWidth() / 2;
            } else if (_horizontalJustification == Graphics.TEXT_JUSTIFY_RIGHT) {
                return getWidth();
            } else {
                throw new NotImplementedError();
            }
        }

        function unload() as Void {
            if (_isLoaded) {
                _height = null;
                _lineHeight = null;
                _wrappedText = null;
                _textWidth = null;
                _xOffset = null;
                _isLoaded = false;
                _requestParentReload();
            }
        }

        protected function _requestParentReload() as Void {
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {

            if (!_isLoaded) {
                throw new Lang.OperationNotAllowedException(
                    "TextElement not loaded");
            }

            // Range of y values where things can be drawn to screen
            var yMin = -_lineHeight + 1;
            var yMax = dc.getHeight();

            var xLine = x + _xOffset;
            var width = getWidth();

            // Render all lines on-screen
            var yLine = y;
            while (yLine < yMax) {
                var line;
                if (_mode == NO_WRAP) {
                    line = _str as String;
                } else if (_mode == WRAP_SINGLE_LINE) {
                    line = _wrappedText as String;
                } else {
                    line = (_wrappedText as TextWrapper).next() as String;
                }
                if (yLine >= yMin) {
                    // Text is at least partially on-screen
                    if (_fillWidthWithBackgroundColor) {
                        // Fill full line background
                        dc.setColor(_backgroundColor, _backgroundColor);
                        dc.fillRectangle(x, y, width, _lineHeight + 1);
                    }
                    dc.setColor(_textColor, _backgroundColor);
                    dc.drawText(xLine, yLine, _font, line, _horizontalJustification);
                }
                yLine += _lineHeight;
                if ((_mode != WRAP_MULTI_LINE) or (!_wrappedText.hasNext())) {
                    // Draw horizontal rule if applicable
                    if (_horizontalRule) {
                        yLine += HORIZONTAL_RULE_Y_SPACING;
                        if (yLine >= yMin && yLine < yMax) {
                            // Render horizontal rule
                            dc.setColor(_horizontalRuleColor, _backgroundColor);
                            dc.setPenWidth(1);
                            dc.drawLine(x, yLine, x + width, yLine);
                        }
                    }
                    // Stop iterating
                    break;
                }
            }
            if (_mode == WRAP_MULTI_LINE) {
                _wrappedText.reset();
            }
        }

        function isHeightGreaterThanOrEqualTo(threshold as Number) as Boolean {
            // Check if height of element is greater than or equal to value
            if (_height != null) {
                // Full height has already been calculated
                return _height >= threshold;
            } else {
                // mode is WRAP_MULTI_LINE as height is unknown
                // Use partial height calculation method
                if (!_isLoaded) {
                    throw new ValueError(
                        "TextElement not loaded");
                }
                // Count at least as many lines as required
                var thresholdNumLines = Math.ceil(threshold / _lineHeight).toNumber();
                var numLines = 0;
                while (numLines < thresholdNumLines && _wrappedText.next() != null) {
                    numLines ++;
                }
                if (!_wrappedText.hasNext()) {
                    // Lines are exhausted, _height is known
                    _height = numLines * _lineHeight;
                }
                _wrappedText.reset();

                return numLines >= thresholdNumLines;
            }
        }

        function isHeightKnown() as Boolean {
            return _height != null;
        }

        function getSpacingAfter() as Number {
            return _spacingAfter;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function getScrollPoint(y as Number) as Number {
            // Get y of most recent non-empty line
            if (_mode == WRAP_MULTI_LINE) {
                if (!_isLoaded) {
                    throw new ValueError(
                        "TextElement not loaded");
                }
                var yTop = 0;
                var scrollPoint = 0;
                while (yTop <= y) {
                    var line = _wrappedText.next();
                    if (line != null) {
                        if (!line.equals("")) {
                            scrollPoint = yTop;
                        }
                        yTop += _lineHeight;
                    } else {
                        break;
                    }
                }
                _wrappedText.reset();
                return scrollPoint;
            } else {
                return 0;
            }
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof TextElement &&
                _str.equals(object.getStr());
        }

        function toString() as String {
            return "TextElement{" +
                "str=" + _str.toString() + "}";
        }
    }
}
