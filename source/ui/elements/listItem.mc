import Toybox.Lang;


module Codex {

    class ListItem extends TextElement {

        function initialize(str as String or ResourceId, maxWidth as Number, theme as Theme, options as {
                :isVisible as BooleanRef}) {
            /*
            An item in a list

            :param str: String content
            :param maxWidth: Max width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether ot not to draw this element (default: true)
            */
            options[:maxWidth] = maxWidth;
            options[:multiline] = true;
            options[:spacingAfter] = theme.markdown.listItemSpacingAfter;
            TextElement.initialize(str, theme.markdown.listItemFont, theme.color.text, theme.color.background, options);
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof ListItem &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "ListItem{" +
                "str=" + _str.toString() + "}";
        }
    }
}
