import Toybox.Lang;


module Codex {

    class Heading4 extends TextElement {

        function initialize(str as String or ResourceId, maxWidth as Number, theme as Theme, options as {
                :isVisible as BooleanRef}) {
            /*
            Markdown Heading level 4 element

            :param str: Heading text
            :param maxWidth: Maximum width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether or not to draw the element (default: true)
            */
            options[:width] = maxWidth;
            options[:multiline] = true;
            options[:spacingAfter] = theme.markdown.heading4SpacingAfter;
            options[:horizontalRule] = theme.markdown.heading4HorizontalRule;
            options[:horizontalRuleColor] = theme.color.majorAccent;
            TextElement.initialize(str, theme.markdown.heading4Font, theme.color.text, theme.color.background, options);
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof Heading4 &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "Heading4{" +
                "str=" + _str.toString() + "}";
        }
    }
}
