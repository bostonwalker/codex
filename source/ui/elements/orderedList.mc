import Toybox.Lang;


module Codex {

    class OrderedList extends List {

        function initialize(items as Array<MarkdownElement> or Iterator, level as Number, enumType as Number,
                width as Number, theme as Theme, options as {:isVisible as BooleanRef}) {
            /*
            An ordered list. Items are enumerated according to enumType.

            :param items: An array or iterator of ListItem objects
            :param level: List nesting level
            :param enumType: Enumeration method (arabic numerals, roman numerals, lowercase alphabet, etc.)
            :param width: Element width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether ot not to draw this element (default: true)
            */
            if (enumType == List.ENUMTYPE_BULLET) {
                throw new ValueError(
                    "OrderedList cannot be enumerated by bullets");
            }
            List.initialize(items, level, enumType, width, theme, options);
        }

        function isIdenticalTo(object as Object) as Boolean {
            return
                object instanceof OrderedList &&
                List.equals(object);
        }

        function toString() as String {
            return "OrderedList{" +
                "children=" + getChildren().toString() + "," +
                "level=" + getLevel().toString() + "," +
                "enumType=" + getEnumType().toString() + "}";
        }
    }
}
