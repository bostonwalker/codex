import Toybox.Lang;


module Codex {

    class Heading2 extends TextElement {

        function initialize(str as String or ResourceId, maxWidth as Number, theme as Theme, options as {
                :isVisible as BooleanRef}) {
            /*
            Markdown Heading level 2 element

            :param str: Heading text
            :param maxWidth: Maximum width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether or not to draw the element (default: true)
            */
            options[:width] = maxWidth;
            options[:multiline] = true;
            options[:spacingAfter] = theme.markdown.heading2SpacingAfter;
            options[:horizontalRule] = theme.markdown.heading2HorizontalRule;
            options[:horizontalRuleColor] = theme.color.majorAccent;
            TextElement.initialize(str, theme.markdown.heading2Font, theme.color.text, theme.color.background, options);
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof Heading2 &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "Heading2{" +
                "str=" + _str.toString() + "}";
        }
    }
}
