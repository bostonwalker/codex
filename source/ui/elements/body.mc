import Toybox.Lang;


module Codex {

    class Body extends BaseElement {

        protected var _width as Number;

        function initialize(children as Array<MarkdownElement> or MemoIterator, width as Number, options as {
                :isVisible as BooleanRef}) {
            /*
            Container for displaying Markdown elements

            :param children: Child elements
            :param options:
                :isVisible: Whether or not to draw the element (default: true)
            */
            BaseElement.initialize(children, options);
            _width = width;
        }

        function getWidth() as Number {
            return _width;
        }

        function getSpacingAfter() as Number {
            return 0;
        }

        function hasScrollPoints() as Boolean {
            return true;
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof Body &&
                BaseElement.equals(object);
        }

        function toString() as String {
            return "Body{" +
                "children=" + getChildren().toString() + "}";
        }
    }
}
