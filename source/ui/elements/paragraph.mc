import Toybox.Lang;


module Codex {

    class Paragraph extends TextElement {

        function initialize(str as String or ResourceId or Iterator, maxWidth as Number, theme as Theme, options as {
                :isVisible as BooleanRef}) {
            /*
            Markdown paragraph element

            :param str: Paragraph text
            :param maxWidth: Maximum width in pixels
            :param theme: Styling options
            :param options:
                :isVisible: Whether ot not to draw this element (default: true)
            */
            options[:maxWidth] = maxWidth;
            options[:multiline] = true;
            options[:spacingAfter] = theme.markdown.paragraphSpacingAfter;
            TextElement.initialize(str, theme.markdown.paragraphFont, theme.color.text, theme.color.background, options);
        }

        function isIdenticalTo(object as Object) as Boolean {
            return 
                object instanceof Paragraph &&
                TextElement.equals(object);
        }

        function toString() as String {
            return "Paragraph{" +
                "str=" + _str.toString() + "}";
        }
    }
}
