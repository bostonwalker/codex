import Toybox.Graphics;
import Toybox.Lang;


module Codex {

    class BaseElement {  // implements MarkdownElement

        protected var _children as Iterator;
        protected var _isVisible as Boolean;
        protected var _requestReloadCallback as Callback?;

        protected var _height as Number?;

        function initialize(children as Array<MarkdownElement> or Iterator, options as {
                :isVisible as BooleanRef, :requestReloadCallback as Callback}) {
            /*
            Base class for Markdown elements

            :param children: Child elements
            :param options:
                :isVisible: Whether or not to draw the element (default: true)
                :requestReloadCallback: Callback to parent to request reload when this Element changes size
            */
            if (children instanceof Array) {
                children = new ArrayIterator(children);
            }
            _children = children;
            _isVisible = Ref.linkBoolean(self, :setIsVisible, Utils.getDefault(options, :isVisible, true) as Boolean);
            _requestReloadCallback = Utils.getDefault(options, :requestReloadCallback, null) as Callback?;
        }

        function getChildren() as Iterator {
            return _children;
        }

        function isVisible() as Boolean {
            return _isVisible;
        }

        function setIsVisible(value as BooleanRef) as Void {
            _isVisible = Ref.linkBoolean(self, :setIsVisible, value);
            _requestParentReload();
        }

        function setRequestReloadCallback(value as Callback) as Void {
            _requestReloadCallback = value;
        }

        function load(dc as Graphics.Dc) as Void {
            loadYRange(dc, 0, null);
        }

        function loadYRange(dc as Graphics.Dc, yMin as Number, yMax as Number?) as Void {
            /*
            Load content

            :param dc: Display context
            :param yMin: Start of content range
            :param yMax: End of content range (optional)
            */
            if (yMax == null) {
                yMax = 0x7FFFFFFF;  // 32-bit signed max value
            }
            
            var yTop = 0;
            while (yTop < yMax && _children.hasNext()) {
                
                var child = _children.next() as MarkdownElement;

                if (child.isHeightKnown() && yTop + child.getHeight() < yMin) {
                    // Child is not on screen, ignore
                    // child.unload();
                } else {
                    // Unknown if child is on screen, load up to screen height
                    child.loadYRange(dc, yMin - yTop, yMax - yTop);
                    if (!child.isHeightKnown() && child.isHeightGreaterThanOrEqualTo(yMax - yTop)) {
                        // Content has been loaded up to bottom of screen, no need to continue
                        break;
                    }
                }

                // Accumulate value
                yTop += child.getHeight();
                if (_children.hasNext()) {
                    yTop += child.getSpacingAfter();
                } else {
                    // Full height reached, memoize value
                    _height = yTop;
                    break;
                }
            }

            // Unload remaining off-screen children without generating new items, i.e. triggering Markdown parsing
            var memo = (_children as MemoIterator).getMemo();
            for (var i = 0; i < memo.size(); i++) {
                memo[i].unload();
            }

            _children.reset();
        }

        function unload() as Void {
            // Only unload children that have been seen so far, i.e. don't trigger more Markdown parsing
            var memo = _children instanceof MemoIterator ? _children.getMemo() : _children.asArray();
            for (var i = 0; i < memo.size(); i++) {
                memo[i].unload();
            }
            _requestParentReload();
        }

        protected function _requestParentReload() as Void {
            if (_requestReloadCallback != null) {
                // Phone parent to request reload
                _requestReloadCallback.invoke();
            }
        }

        function draw(dc as Graphics.Dc, x as Number, y as Number) as Void {
            /*
            Draw element content

            :param dc: Display context
            :param x: X coordinate to draw at
            :param y: Y coordinate to draw at
            */
            var yTop = y;
            var yMax = dc.getHeight();

            while (yTop < yMax && _children.hasNext()) {

                var child = _children.next() as MarkdownElement;

                if (!child.isHeightKnown() || yTop + child.getHeight() >= 0) {
                    // Child is on-screen or possibly on-screen
                    if (child.isVisible()) {
                        child.draw(dc, x, yTop);
                    }
                    if (!child.isHeightKnown() && child.isHeightGreaterThanOrEqualTo(yMax - yTop)) {
                        // Content has been drawn up to bottom of screen, no need to continue
                        break;
                    }
                }

                // Accumulate value
                yTop += child.getHeight();
                if (_children.hasNext()) {
                    yTop += child.getSpacingAfter();
                }
            }

            _children.reset();
        }

        function getWidth() as Number {
            // Must be implemented by subclass
            throw new NotImplementedError();
        }

        function getHeight() as Number {
            // Get display height of element
            if (_height == null) {
                _height = getHeightImpl();
            }
            return _height;
        }

        protected function getHeightImpl() as Number {
            /*
            Calculate display height of element.

            Element will be loaded for y in [0, infinity) first.
            */
            var totalHeight = 0;
            while (_children.hasNext()) {
                var child = _children.next() as MarkdownElement;
                totalHeight += child.getHeight();
                if (_children.hasNext()) {
                    totalHeight += child.getSpacingAfter();
                }
            }
            _children.reset();
            return totalHeight;
        }

        function isHeightGreaterThanOrEqualTo(threshold as Number) as Boolean {
            // Check if height of element is greater than or equal to value
            if (_height != null) {
                // Full height has already been calculated
                return _height >= threshold;
            } else {
                // Use partial height calculation method
                return isHeightGreaterThanOrEqualToImpl(threshold);
            }
        }

        protected function isHeightGreaterThanOrEqualToImpl(threshold as Number) as Boolean {
            /*
            Test if height is greater than or equal to a threshold. Used to determine if next element should be
            displayed without needing to load full height of this element.

            Element will be loaded for y in [0, threshold] first.
            */
            var totalHeight = 0;
            var result = false;

            while (_children.hasNext()) {

                var child = _children.next() as MarkdownElement;

                if (child.isHeightGreaterThanOrEqualTo(threshold - totalHeight)) {
                    // Threshold reached, break
                    result = true;
                    if (!child.isHeightKnown()) {
                        // Don't load more content by calling getHeight()
                        break;
                    }
                }

                // Accumulate value
                totalHeight += child.getHeight();
                if (_children.hasNext()) {
                    totalHeight += child.getSpacingAfter();
                    if (totalHeight >= threshold) {
                        result = true;
                    }
                } else {
                    // Full height reached, memoize value
                    _height = totalHeight;
                }

                if (result == true) {
                    break;
                }
            }

            _children.reset();

            return result;
        }

        function isHeightKnown() as Boolean {
            // Check if height has already been calculated
            return _height != null;
        }

        function getSpacingAfter() as Number {
            throw new NotImplementedError();
        }

        function hasScrollPoints() as Boolean {
            throw new NotImplementedError();
        }

        function getScrollPoint(y as Number) as Number {
            // Get point 0 < scollPoint <= y <= height that will be aligned with top edge of screen
            var yTop = 0;
            var child = null;
            while (_children.hasNext()) {
                child = _children.next() as MarkdownElement;
                if (child.isHeightGreaterThanOrEqualTo(y - yTop - child.getSpacingAfter())) {
                    // Scroll point is within child or space after child
                    break;
                }
                yTop += child.getHeight();
                if (_children.hasNext()) {
                    yTop += child.getSpacingAfter();
                }
            }
            _children.reset();
            return yTop + (child != null ? child.getScrollPoint(y - yTop) : 0);
        }

        function isIdenticalTo(object as Object) as Boolean {
            return
                object instanceof BaseElement && 
                _children.equals(object.getChildren());
        }

        function toString() as String {
            throw new NotImplementedError();
        }
    }
}