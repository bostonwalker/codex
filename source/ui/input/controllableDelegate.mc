import Toybox.Lang;
using Toybox.WatchUi;
using Toybox.Attention;


module Codex {
    
    class ControllableDelegate extends WatchUi.BehaviorDelegate {
        /*
        A modification of WatchUi.BehaviorDelegate that works with Codex `Controllable` interface and adds some extra features.
        */
        protected var _object as WeakReference<Controllable>;

        function initialize(object as Controllable) {
            /*
            :param object: Underlying controllable object (a weak reference will be kept)
            */
            WatchUi.BehaviorDelegate.initialize();
            _object = object.weak();
        }

        function onEnter() as Boolean {
            // Select button was pressed or screen was swiped left
            var object = _object.get();
            var isHandled = object.onEnter();
            if (isHandled) {
                getApp().requestUpdate();
            }
            return true;
        }

        function onBack() as Boolean {
            // Exit button was pressed or screen was swiped right
            var object = _object.get();
            var isHandled = object.onBack();
            if (!isHandled) {
                Codex.getApp().exitView();
            }
            getApp().requestUpdate();
            return true;
        }

        function onNextPage() as Boolean {
            // Next page button was pressed or screen was swiped up
            var object = _object.get();
            var isHandled = object.onNextPage();
            if (isHandled) {
                getApp().requestUpdate();
            }
            return true;
        }

        function onPreviousPage() as Boolean {
            // Previous page button was pressed or screen was swiped down
            var object = _object.get();
            var isHandled = object.onPreviousPage();
            if (isHandled) {
                getApp().requestUpdate();
            }
            return true;
        }

        function onTap(clickEvent as WatchUi.ClickEvent) as Boolean {
            // View was tapped
            // Default: no effect
            var coords = clickEvent.getCoordinates();
            var object = _object.get();
            var isHandled = object.onTapAt(coords[0], coords[1]);
            if (isHandled) {
                getApp().requestUpdate();
            }
            return true;
        }

        function onKeyPressed(keyEvent as WatchUi.KeyEvent) as Boolean {
            if (keyEvent.getKey() == WatchUi.KEY_ENTER) {
                // Necessary because onSelect() is suppressed
                return onEnter();
            } else {
                return false;
            }
        }

        function onSwipe(swipeEvent as WatchUi.SwipeEvent) as Boolean {
            switch (swipeEvent.getDirection()) {
                case WatchUi.SWIPE_UP:
                    return onNextPage();
                case WatchUi.SWIPE_DOWN:
                    return onPreviousPage();
                case WatchUi.SWIPE_LEFT:
                    return onEnter();
                case WatchUi.SWIPE_RIGHT:
                    return onBack();
                default:
                    throw new ValueError(
                        "Invalid direction: " + swipeEvent.getDirection().toString());
            }
        }

        function onSelect() as Boolean {
            // Suppress so input is handled separately by onKeyPressed() or onTap()
            return false;
        }

        function onMenu() as Boolean {
            // Default: not implemented
            return false;
        }
    }
}
