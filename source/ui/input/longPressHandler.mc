import Toybox.Lang;
using Toybox.WatchUi;


module Codex {

    class LongPressHandler {

        private var _keyHandlers as Dictionary<WatchUi.Key, _LongPressSingleKeyHandler>;

        function initialize(longPressTimeMs as Number, options as {
            :shortPressCallbacks as Dictionary<WatchUi.Key, Callback>,
            :longPressCallbacks as Dictionary<WatchUi.Key, Callback>,
            :fireOnRelease as Boolean,
        }) {
            /*
            Object that handles key events and routes short/long presses to the appropriate callbacks

            :param longPressTimeMs: Cutoff time for short/long presses in ms
            :param shortPressCallbacks: Callbacks for short presses
            :param longPressCallbacks: Callbacks for long presses
            :param fireOnRelease: If true, fire long press callback only when button is released. If false, fire when timer elapses.
            */
            var shortPressCallbacks = Utils.getDefault(options, :shortPressCallbacks, {}) as Dictionary<WatchUi.Key, Callback>;
            var longPressCallbacks = Utils.getDefault(options, :longPressCallbacks, {}) as Dictionary<WatchUi.Key, Callback>;
            var fireOnRelease = Utils.getDefault(options, :fireOnRelease, false) as Boolean;

            // Get set of unique keys that need to be handled
            var keySet = new Set();
            keySet.addAll(shortPressCallbacks.keys());
            keySet.addAll(longPressCallbacks.keys());
            var keys = keySet.toArray() as Array<WatchUi.Key>;

            // Create handler for each key
            _keyHandlers = {};
            for (var i = 0; i < keys.size(); i++) {
                var key = keys[i];
                var keyOptions = {
                    :fireOnRelease => fireOnRelease,
                };
                if (shortPressCallbacks.hasKey(key)) {
                    keyOptions[:shortPressCallback] = shortPressCallbacks[key];
                }
                if (longPressCallbacks.hasKey(key)) {
                    keyOptions[:longPressCallback] = longPressCallbacks[key];
                }
                _keyHandlers[key] = new _LongPressSingleKeyHandler(longPressTimeMs, keyOptions);
            }
        }

        function onKeyPressed(keyEvent as WatchUi.KeyEvent) as Lang.Boolean {
            var key = keyEvent.getKey();
            var pressType = keyEvent.getType();
            if (pressType != WatchUi.PRESS_TYPE_DOWN) {
                throw new ValueError(
                    "Unexpected press type: " + pressType.toString());
            }
            if (_keyHandlers.hasKey(key)) {
                return _keyHandlers[key].onKeyPressed();
            } else {
                return false;
            }
        }

        function onKeyReleased(keyEvent as WatchUi.KeyEvent) as Lang.Boolean {
            var key = keyEvent.getKey();
            var pressType = keyEvent.getType();
            if (pressType != WatchUi.PRESS_TYPE_UP) {
                throw new ValueError(
                    "Unexpected press type: " + pressType.toString());
            }
            if (_keyHandlers.hasKey(key)) {
                return _keyHandlers[key].onKeyReleased();
            } else {
                return false;
            }
        }
    }


    class _LongPressSingleKeyHandler {

        private var _longPressTimeMs as Number;
        private var _shortPressCallback as Callback?;
        private var _longPressCallback as Callback?;
        private var _fireOnRelease as Boolean;

        private var _isPressed = false;
        private var _longPressTimer as Codex.Timer;

        function initialize(longPressTimeMs as Number, options as {
            :shortPressCallback as Callback,
            :longPressCallback as Callback,
            :fireOnRelease as Boolean,
        }) {
            _longPressTimeMs = longPressTimeMs;
            _shortPressCallback = Utils.getDefault(options, :shortPressCallback, null) as Callback?;
            _longPressCallback = Utils.getDefault(options, :longPressCallback, null) as Callback?;
            _fireOnRelease = Utils.getDefault(options, :fireOnRelease, false) as Boolean;

            _longPressTimer = new Codex.Timer(method(:_onLongPressTimeReached));
        }

        function onKeyPressed() as Lang.Boolean {
            // Set timer for long press
            _isPressed = true;
            _longPressTimer.startFor(_longPressTimeMs);
            return true;
        }

        function onKeyReleased() as Lang.Boolean {
            if (_isPressed) {
                _isPressed = false;
                if (_longPressTimer.isRunning()) {
                    // Prevent long press callback from firing
                    _longPressTimer.stop();
                    if (_shortPressCallback != null) {
                        // Fire short press callback
                        _shortPressCallback.invoke();
                        return true;
                    }
                } else if (_fireOnRelease && _longPressCallback != null) {
                    // Fire long press callback
                    _longPressCallback.invoke();
                    return true;
                }
            }
            return false;
        }

        function _onLongPressTimeReached(currentTime as Number) as Void {
            // Prevent short press callback from firing
            if (!_fireOnRelease && _longPressCallback != null) {
                // Invoke callback
                _longPressCallback.invoke();
            }
            _isPressed = false;
        }
    }
}
