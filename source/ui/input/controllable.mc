import Toybox.Lang;


module Codex {

    typedef Controllable as interface {
        /*
        Functionality for control by Codex.BehaviorDelegate.

        Return values of methods indicate whether they have an effect. If they do, an optional tone will be played.
        */
        // Called when user presses enter/select button or swipes left
        function onEnter() as Boolean;

        // Called when user presses previous/up button or swipes down
        function onPreviousPage() as Boolean;

        // Called when user presses next/down button or swipes up
        function onNextPage() as Boolean;

        // Called when user presses exit button or swipes right
        function onBack() as Boolean;
        
        // Signal that user tapped at relative coordinates x, y within object
        function onTapAt(x as Number, y as Number) as Boolean;

        // Signal that user tapped elsewhere. Used to create hierarchies of `Controllable` objects.
        function onTapElsewhere() as Void;
    };
}
