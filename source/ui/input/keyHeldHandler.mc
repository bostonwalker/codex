import Toybox.Lang;
using Toybox.WatchUi;


module Codex {

    class KeyHeldHandler {

        protected static const HOLD_TIME_MS_DEFAULT = 700;
        protected static const CALLBACK_INTERVAL_MS_DEFAULT = 140;

        protected var _keyHandlers as Dictionary<WatchUi.Key, _KeyHeldSingleKeyHandler>;

        function initialize(callbacks as Dictionary<WatchUi.Key, Callback>, options as {
                :holdTimeMs as Number, :callbackIntervalMs as Number}) {
            /*
            Object that handles key events and routes key held events to the appropriate callbacks

            :param callbacks: Mapping of keys to callback
            :param options:
                :holdTimeMs: Time key held until first callback (default: 1500 ms)
                :callbackIntervalMs: Time between subsequent callbacks (default: 250 ms)
            */
            // Create handler for each key
            var holdTimeMs = Utils.getDefault(options, :holdTimeMs, HOLD_TIME_MS_DEFAULT) as Number;
            var callbackIntervalMs = Utils.getDefault(options, :callbackIntervalMs, CALLBACK_INTERVAL_MS_DEFAULT) as Number;

            _keyHandlers = {};
            var keys = callbacks.keys();
            for (var i = 0; i < keys.size(); i++) {
                var key = keys[i];
                var callback = callbacks[key];
                _keyHandlers[key] = new _KeyHeldSingleKeyHandler(callback, holdTimeMs, callbackIntervalMs);
            }
        }

        function onKeyPressed(keyEvent as WatchUi.KeyEvent) as Lang.Boolean {
            var key = keyEvent.getKey();
            var pressType = keyEvent.getType();
            if (pressType != WatchUi.PRESS_TYPE_DOWN) {
                throw new ValueError(
                    "Unexpected press type: " + pressType.toString());
            }
            if (_keyHandlers.hasKey(key)) {
                return _keyHandlers[key].onKeyPressed();
            } else {
                return false;
            }
        }

        function onKeyReleased(keyEvent as WatchUi.KeyEvent) as Lang.Boolean {
            var key = keyEvent.getKey();
            var pressType = keyEvent.getType();
            if (pressType != WatchUi.PRESS_TYPE_UP) {
                throw new ValueError(
                    "Unexpected press type: " + pressType.toString());
            }
            if (_keyHandlers.hasKey(key)) {
                return _keyHandlers[key].onKeyReleased();
            } else {
                return false;
            }
        }
    }


    class _KeyHeldSingleKeyHandler {

        private var _callback as Callback;
        private var _holdTimeMs as Number;
        private var _callbackIntervalMs as Number;

        private var _isPressed = false;
        private var _keyHeldTimer as Codex.Timer;

        function initialize(callback as Callback, holdTimeMs as Number, callbackIntervalMs as Number) {
            /*
            :param callback: Callback
            :param holdTimeMs: Time key held until first callback
            :param callbackIntervalMs: Time between subsequent callbacks
            */
            _callback = callback;
            _holdTimeMs = holdTimeMs;
            _callbackIntervalMs = callbackIntervalMs;

            _keyHeldTimer = new Codex.Timer(method(:_onKeyHeld));
        }

        function onKeyPressed() as Lang.Boolean {
            // Set timer for long press
            _isPressed = true;
            if (_keyHeldTimer.isRunning()) {
                _keyHeldTimer.stop();
            }
            _keyHeldTimer.startFor(_holdTimeMs);
            return true;
        }

        function onKeyReleased() as Lang.Boolean {
            if (_isPressed) {
                _isPressed = false;
                if (_keyHeldTimer.isRunning()) {
                    _keyHeldTimer.stop();
                }
                return true;
            } else {
                return false;
            }
        }

        function _onKeyHeld(currentTime as Number) as Void {
            // Called first time after _holdTimeMs, then every _callbackIntervalMs
            _callback.invoke();
            _keyHeldTimer.startFor(_callbackIntervalMs);
        }
    }
}
