import Toybox.Lang;
using Toybox.System;
using Toybox.Timer as ToyboxTimer;


enum State {
    STATE_STOPPED,  // 0
    STATE_FIRE_ONCE,  // 1
    STATE_FIRE_AT_INTERVALS,  // 2
}


module Codex {

    class _MasterTimer {
        /*
        Object for scheduling Timer callbacks using System clock
        */
        // Priority queue of upcoming timer events
        protected var _toyboxTimer = new ToyboxTimer.Timer();
        protected var _timerQueue = [] as Array<Timer>;

        (:typecheck(false))
        function startTimer(timer as Timer) as Void {
            // Put a timer in the timer queue and adjust master timer as appropriate
            // Insert copy of last item
            var timestamp = timer._getTimestamp();
            var n = _timerQueue.size();
            if (n > 0) {
                _timerQueue.add(_timerQueue[n - 1]);
                // Find index to insert, start search at end of queue
                for (var i = n; i > 0; i--) {
                    var previousItem = _timerQueue[i - 1];
                    if (timestamp > previousItem._getTimestamp()) {
                        // Item has lower priority, insert behind
                        _timerQueue[i] = timer;
                        return;
                    }
                    _timerQueue[i] = previousItem;
                }
                // Got all the way to the start of the queue
                _timerQueue[0] = timer;
            } else {
                // Only callback in the queue
                _timerQueue.add(timer);
            }
            // Timer is next in the queue
            var timeRemaining = timestamp - System.getTimer();
            if (timeRemaining < 0) {
                timeRemaining = 0;
            }
            _toyboxTimer.stop();
            _toyboxTimer.start(method(:_tick), timeRemaining, false);
        }

        (:typecheck(false))
        function stopTimer(timer as Timer) as Void {
            // Remove timer from the timer queue and adjust master time as appropriate
            var overwriteIndex = 0;
            var removedFromStart = false;
            for (var i = 0; i < _timerQueue.size(); i++) {
                var item = _timerQueue[i];
                if (item == timer) {
                    removedFromStart = i == 0;
                    continue;
                }
                _timerQueue[overwriteIndex] = item;
                overwriteIndex ++;
            }
            if (overwriteIndex == _timerQueue.size()) {
                throw new KeyError(
                    "Callback not found for timer: " + timer.toString());
            }
            _timerQueue = _timerQueue.slice(0, overwriteIndex);

            if (removedFromStart) {
                _toyboxTimer.stop();
                if (_timerQueue.size() > 0) {
                    var nextItem = _timerQueue[0];
                    var timeRemaining = nextItem._getTimestamp() - System.getTimer();
                    if (timeRemaining < 0) {
                        timeRemaining = 0;
                    }
                    _toyboxTimer.start(method(:_tick), timeRemaining, false);
                }
            }
        }

        (:typecheck(false))
        function _tick() as Void {
            var currentTime = System.getTimer();
            var timeUntilNext = null;
            while (_timerQueue.size() > 0) {
                // TODO: consider limiting number of timers that can fire at once (e.g. 3 timers max)
                // Firing timers at the same time economizes on updates -> battery life, but firing too many timers at once risks WatchdogTrippedError
                var nextTimer = _timerQueue[0];
                timeUntilNext = nextTimer._getTimestamp() - currentTime;
                if (timeUntilNext <= 0) {
                    // Next timer on the queue has expired, take action
                    var timerState = nextTimer._getState();
                    if (timerState == $.STATE_FIRE_AT_INTERVALS) {
                        // Update timestamp to fire at next interval
                        var newTimestamp = nextTimer._getNextTimestamp();
                        nextTimer._setTimestamp(newTimestamp);
                        if (_timerQueue.size() > 1) {
                            // If there are other timers, reinsert timer in queue at new place
                            var insertIndex = 0;
                            for (var i = 1; i < _timerQueue.size(); i++) {
                                var item = _timerQueue[i];
                                if (item._getTimestamp() > newTimestamp) {
                                    break;
                                }
                                _timerQueue[i - 1] = item;
                                insertIndex ++;
                            }
                            if (insertIndex > 0) {
                                _timerQueue[insertIndex] = nextTimer;
                            }
                        }
                    } else if (timerState == $.STATE_FIRE_ONCE) {
                        // Mark timer as stopped
                        nextTimer._markStopped();
                        // Remove from queue
                        _timerQueue = _timerQueue.slice(1, _timerQueue.size());
                    } else {
                        throw new ValueError(
                            "Unexpected timer state: " + timerState.toString());
                    }
                    var callback = nextTimer._getCallback();
                    callback.invoke(nextTimer._getTimestamp());
                } else {
                    // Timers left are all expiring in the future
                    break;
                }
            }
            if (timeUntilNext != null && _timerQueue.size() > 0) {
                _toyboxTimer.start(method(:_tick), timeUntilNext, false);
            }
        }

        protected static var _instance as _MasterTimer?;

        static function getInstance() as _MasterTimer {
            if (_instance == null) {
                _instance = new _MasterTimer();
            }
            return _instance;
        }
    }

    class Timer {
        /*
        Object that expands upon Timer.Timer functionality and allows unlimited instances of timers
        */
        protected var _callback as NumberCallback;
        protected var _state as State;
        protected var _timestamp as Number?;
        protected var _intervalDuration as Number?;
        protected var _masterTimer as _MasterTimer;

        function initialize(callback as NumberCallback) {
            /*
            :param callback: Method to be invoked when this timer fires
            */
            _callback = callback;
            _state = $.STATE_STOPPED;
            _masterTimer = _MasterTimer.getInstance();
        }

        function _getCallback() as NumberCallback {
            return _callback;
        }

        function _getState() as State {
            return _state;
        }

        function _getTimestamp() as Number? {
            return _timestamp;
        }

        function _setTimestamp(value as Number) {
            _timestamp = value;
        }

        function _getNextTimestamp() as Number {
            return _timestamp + _intervalDuration;
        }

        function _markStopped() as Void {
            _state = $.STATE_STOPPED;
            _timestamp = null;
            _intervalDuration = null;
        }

        function startFor(duration as Number) as Void {
            if (duration < 50) {
                throw new ValueError(
                    "Duration was too short. Minimum supported duration is 50 ms");
            }
            if (_state != $.STATE_STOPPED) {
                throw new OperationNotAllowedException(
                    "Timer already running");
            }
            var currentTime = System.getTimer();
            _state = $.STATE_FIRE_ONCE;
            _timestamp = currentTime + duration;
            _masterTimer.startTimer(self);
        }

        function startAtIntervals(duration as Number) as Void {
            if (duration < 50) {
                throw new ValueError(
                    "Duration was too short. Minimum supported duration is 50 ms");
            }
            if (_state != $.STATE_STOPPED) {
                throw new OperationNotAllowedException(
                    "Timer already running");
            }
            var currentTime = System.getTimer();
            var intervalRemaining = duration - (currentTime % duration);
            _state = $.STATE_FIRE_AT_INTERVALS;
            _timestamp = currentTime + intervalRemaining;
            _intervalDuration = duration;
            _masterTimer.startTimer(self);
        }

        function stop() as Void {
            if (_state == $.STATE_STOPPED) {
                throw new OperationNotAllowedException(
                    "Timer not running");
            }
            _state = $.STATE_STOPPED;
            _timestamp = null;
            _intervalDuration = null;
            _masterTimer.stopTimer(self);
        }

        function isRunning() as Boolean {
            return _state != $.STATE_STOPPED;
        }
    }
}
