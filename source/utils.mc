import Toybox.Lang;
using Toybox.WatchUi;
using Toybox.Math;
using Toybox.Test;


module Codex {

    class Utils {

        static function getDefault(dict as Dictionary, key as Object, defaultValue as Object?) as Object? {
            /*
            Get a value from a dictionary, returning a default value if the key does not exist
            */
            var value = dict[key];
            if (value == null) {
                value = defaultValue;
            }
            return value;
        }

        static function resolveString(arg as ResourceId or String) as String {
            /*
            Resolve a string or resource symbol representing a string
            */
            if (arg instanceof String) {
                return arg;
            } else {
                var result = WatchUi.loadResource(arg as ResourceId);
                if (result instanceof String) {
                    return result;
                } else {
                    throw new ValueError(
                        "Unable to resolve symbol: " + arg.toString());
                }
            }
        }

        static function repeat(object as Object?, n as Number) as Array<Object?> {
            /*
            Repeat object n times
            */
            var result = new [n];
            for (var i = 0; i < n; i++) {
                result[i] = object;
            }
            return result;
        }

        static function copyArray(array as Array) as Array {
            /*
            Shallow array copy
            */
            var n = array.size();
            var copy = new [n];
            for (var i = 0; i < n; i++) {
                copy[i] = array[i];
            }
            return copy;
        }

        static function multiplyCeiling(x as Numeric, a as Numeric) as Number {
            return Math.ceil(x * a);
        }

        static function calculateDigitsFromValue(value as Number, numDigits as Number) as Array<Number> {
            // Decompose value into its digits
            if (value < 0) {
                throw new InvalidValueException("Value cannot be negative");
            }
            var digits = [];
            for (var i = 0; i < numDigits; i++) {
                digits.add(value % 10);
                value /= 10;
            }
            if (value > 0) {
                throw new InvalidValueException("Value was too large to decompose into " + numDigits.toString() + " digits");
            }
            digits = digits.reverse();
            return digits;
        }

        static function calculateValueFromDigits(digits as Array<Number>) as Number {
            var value = 0;
            for (var i = 0; i < digits.size(); i++) {
                value *= 10;
                value += digits[i];
            }
            return value;
        }

        static function layoutCentered(contentSizes as Array<Numeric>, containerSize as Numeric, options as {
                :spacing as Numeric or Array<Numeric>}) as Array<Numeric> {
            /*
            Calculate object positions when centered in available space

            :param contentSizes: Sizes of content in pixels (widths or heights). Array of size n.
            :param containerSize: Available size to layout objects within (usually device screen width or screen height)
            :param options:
                :spacing: Spacing between objects (default: 0). Can either be uniform value or array of values of size n-1
            :return: Object positions. Array of size n.
            */
            
            var n = contentSizes.size();
            if (n == 0) {
                throw new ValueError(
                    "contentSizes cannot be an empty array");
            }

            var spacing = getDefault(options, :spacing, 0) as Numeric or [Numeric];
            if (!(spacing instanceof Array)) {
                spacing = repeat(spacing, n - 1);
            }

            var contentSizeTotal = 0;
            for (var i = 0; i < n; i++) {
                contentSizeTotal += contentSizes[i];
                if (i < n - 1) {
                    contentSizeTotal += spacing[i];
                }
            }

            var result = [];
            result.add((containerSize - contentSizeTotal) / 2);
            for (var i = 0; i < n - 1; i++) {
                result.add(result[i] + contentSizes[i] + spacing[i]);
            }

            return result;
        }

        static function testMarkdownEquals(actual as BaseIterator, expected as Array<MarkdownElement>) {
            /*
            Test an iterator of markdown elements versus an array of expected items. Iterator should produce the expected items in
            order and then produce null once the array is exhausted.

            :param iterator: Iterator of MarkdownElements to test
            :param expectedItems: Array of expected MarkdownElements
            :return: true if no exceptions raised
            */
            for (var i = 0; i < expected.size(); i++) {
                var actualItem = actual.next() as MarkdownElement?;
                if (actualItem == null) {
                    Test.assertMessage(false, "Iterator exhausted with items remaining: " + 
                                            expected.slice(i, null).toString());
                }
                var expectedItem = expected[i] as Object;
                if (!actualItem.isIdenticalTo(expectedItem)) {
                    throw new ValueError(
                        "Comparison failure, item: " + actualItem.toString() + 
                        ", expected item: " + expectedItem.toString());
                }
            }
            var remainingItem = actual.next() as MarkdownElement?;
            if (remainingItem != null) {
                Test.assertMessage(false, "Iterator had excess items remaining: " + remainingItem.toString());
            }
            return true;
        }
    }
}