import Toybox.Lang;
import Toybox.Test;


module Codex {

    class Set {
        /*
        A basic Python-like set object based on a Dictionary
        */
        private var dict as Dictionary<Object, Boolean> = {};

        function add(item as Object?) as Void {
            /*
            Add an item to the set
            */
            self.dict.put(item, true);
        }

        function addAll(items as Array or Set) as Void {
            /*
            Add all items to the set from a source array or set
            */
            if (items instanceof Set) {
                items = items.toArray();
            }
            for (var i = 0; i < items.size(); i++) {
                var item = items[i];
                self.dict.put(item, true);
            }
        }

        function copy() as Set {
            /*
            Return a shallow copy of the Set
            */
            var result = new Set();
            result.addAll(dict.keys());
            return result;
        }

        function contains(item as Object?) as Boolean {
            /*
            Check if set contains an item
            */
            return self.dict.hasKey(item);
        }

        function difference(other as Set) as Set {
            /*
            Return the difference of the Set with another Set
            */
            var result = new Set();
            var items = self.toArray();
            for (var i = 0; i < items.size(); i++) {
                var item = items[i];
                if (!other.contains(item)) {
                    result.add(item);
                }
            }
            return result;
        }

        function equals(object) as Boolean {
            /*
            Check if two sets are equal (have the same elements)
            */
            if (!(object instanceof Set)) {
                return false;
            }
            var other = object as Set;
            return self.size() == other.size() && self.isSubset(other);
        }

        function intersection(other as Set) as Set {
            /*
            Return the intersection of the Set with another Set
            */
            var result = new Set();
            var items = self.toArray();
            for (var i = 0; i < items.size(); i++) {
                var item = items[i];
                if (other.contains(item)) {
                    result.add(item);
                }
            }
            return result;
        }

        function isEmpty() as Boolean {
            /*
            Check if the set is empty
            */
            return self.dict.isEmpty();
        }

        function isStrictSubset(other as Set) as Boolean {
            /* 
            Check if the set is a strict subset of another
            */
            return self.size() < other.size() && self.isSubset(other);
        }

        function isSubset(other as Set) as Boolean {
            /* 
            Check if the set is a subset of another
            */
            var items = self.toArray();
            for (var i = 0; i < items.size(); i++) {
                var item = items[i];
                if (!other.contains(item)) {
                    return false;
                }
            }
            return true;
        }

        function isStrictSuperset(other as Set) as Boolean {
            /* 
            Check if the set is a strict superset of another
            */
            return self.size() > other.size() && other.isSubset(self);
        }

        function isSuperset(other as Set) as Boolean {
            /* 
            Check if the set is a superset of another
            */
            return other.isSubset(self);
        }

        function remove(item as Object?) as Void {
            /*
            Remove an item from the set
            */
            self.dict.remove(item);
        }

        function removeAll(items as Array<Object?> or Set) as Void {
            /*
            Remove an item from the set
            */
            if (items instanceof Set) {
                items = items.toArray();
            }
            for (var i = 0; i < items.size(); i++) {
                dict.remove(items[i]);
            }
        }

        function size() as Lang.Number {
            /*
            Get the size of the set
            */
            return self.dict.size();
        }

        function symmetricDifference(other as Set) as Set {
            /*
            Return a Set containing items present in this Set or another, but not in both
            */
            var result = self.difference(other);
            result.addAll(other.difference(self));
            return result;
        }

        function toArray() as Array<Object?> {
            /*
            Get an array with all items in the set
            */
            return self.dict.keys();
        }

        function toString() as String {
            /*
            Convert the Set to a String

            ```
            var mySet = Set.from([1, 2, 2, 3]);
            System.println(mySet.toString());    // "{1, 2, 3}"
            ```
            */
            var str = "{";
            var items = self.dict.keys();
            for (var i = 0; i < items.size(); i++) {
                if (i > 0) {
                    str += ", ";
                }
                var item = items[i];
                if (item != null) {
                    str += item.toString();
                } else {
                    str += "null";
                }
            }
            str += "}";
            return str;
        }

        function union(other as Set) as Set {
            /*
            Return the union of the Set with another Set
            */
            var result = new Set();
            result.addAll(self);
            result.addAll(other);
            return result;
        }

        //static function from(items as Array<Object?> or Set) as Set {
            /*
            Initialize and return a new Set from a source array or set

            :arg items: Array or Set to build new Set from
            */
        //    var set = new Set();
        //    set.addAll(items);
        //    return set;
        //}
    }

    (:test)
    function testSetUnion(logger as Logger) as Boolean {

        var a = new Set();
        a.addAll([1, 2, 2, 3]);

        var b = new Set();
        b.addAll([3, 4]);

        var c = new Set();
        c.addAll([1, 2, 3, 4]);

        // TODO: Probably won't work because the order won't be preserved
        Test.assertEqual(a.union(b), c);

        return true;
    }
}
