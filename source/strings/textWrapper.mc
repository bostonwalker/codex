import Toybox.Graphics;
import Toybox.Test;
import Toybox.Lang;
import Toybox.System;
import Toybox.Math;


module Codex {

    class TextWrapper extends BaseIterator {

        // Non-state variables
        private var _text as BaseIterator;
        private var _width as Number;
        private var _font as Graphics.FontReference or Graphics.FontDefinition;
        private var _dc as Graphics.Dc?;

        // State variables
        private var _textSeek as Number;
        private var _lineSeek as Number;
        private var _breaks as Array<Array<Number?>>;

        function initialize(text as String or BaseIterator, width as Number,
            font as Graphics.FontReference or Graphics.FontDefinition) {
            /*
            An iterator for parsing a string into lines of text

            Break on newline character and wrap text according to maximum width and font size

            :param text: String to wrap. Can be singular string or an iterator of strings.
            :param width: Maximum width
            :param font: Font type
            */
            BaseIterator.initialize();

            if (text instanceof BaseIterator) {
                _text = text;
            } else if (text instanceof String) {
                // Wrap singular string in iterator
                _text = new ArrayIterator([text]);
            } else {
                throw new ValueError(
                    "text must be either a string or an iterator of strings");
            }
            _width = width;
            _font = font;

            _textSeek = 0;
            _lineSeek = 0;
            _breaks = [];

            _dc = null;
        }

        function setDc(dc as Graphics.Dc) as Void {
            _dc = dc;
        }

        function next() {
            var result = peek();
            if (result != null) {
                seek ++;
                // Increment 2-D seek
                if (_breaks[_textSeek][_lineSeek] != null) {
                    _lineSeek ++;
                } else {
                    _text.next();
                    _textSeek ++;
                    _lineSeek = 0;
                }
            }
            return result;
        }

        function peek() {
            if (_dc == null) {
                throw new ValueError("dc not set!");
            }

            if (!_text.hasNext()) {
                // Overran last line
                return null;
            }

            if (_breaks.size() == _textSeek) {
                _breaks.add([]);
            }
            var lineMemo = _breaks[_textSeek];

            var str = _text.peek() as String;
            var start = _lineSeek > 0 ? lineMemo[_lineSeek - 1] : 0;
            var split;
            if (_lineSeek == lineMemo.size()) {
                // Find next split point for line
                var strRemaining = str.substring(start, str.length());
                split = getLineBreak(strRemaining, _width, _font, _dc);
                if (split != null) {
                    split += start;
                    if (str.substring(split, split + 1).equals("\n")) {
                        lineMemo.add(split + 1);
                    } else {
                        lineMemo.add(split);
                    }
                } else {
                    lineMemo.add(null);
                }
            } else {
                split = lineMemo[_lineSeek];
            }

            // Added null check since Garmin broke their own stupid function for some devices with SDK 6.4.0
            var line = str.substring(start, split != null ? split : str.length());
            return StringUtils.removeTrailingWhitespace(line, true);
        }

        function setSeek(seek as Number) as Void {
            if (seek == 0) {
                // Shortcut: reset
                _textSeek = 0;
                _lineSeek = 0;
                self.seek = 0;
                _text.reset();
            } else {
                var memoSize = _memoSize();
                while (seek >= memoSize && next() != null) {
                    memoSize ++;
                }
                if (seek >= 0 && seek < memoSize) {
                    self.seek = seek;
                    // Set 2-D seek
                    _textSeek = 0;
                    _lineSeek = 0;
                    while (seek > 0) {
                        if (_breaks[_textSeek].size() <= seek) {
                            seek -= _breaks[_textSeek].size();
                            _textSeek ++;
                        } else {
                            _lineSeek = seek;
                            seek = 0;
                        }
                    }
                    _text.setSeek(_textSeek);
                } else {
                    throw new ValueError(
                        "Item " + seek.toString() + " of TextWrapper does not exist");
                }
            }
        }

        function size() as Number {
            exploit();
            return _memoSize();
        }

        function toString() as String {
            return "TextWrapper{" +
                "dc=" + _dc.toString() + "," +
                "text=" + _text.toString() + "," +
                "width=" + _width.toString() + "," +
                "font=" + _font.toString() + "}";
        }

        private function _memoSize() as Number {
            // Count number of items in memo, a 2-D array
            var result = 0;
            for (var i = 0; i < _breaks.size(); i++) {
                result += _breaks[i].size();
            }
            return result;
        }
        
        static function getLineBreak(str as String, width as Number, font as Graphics.FontReference or Graphics.FontDefinition,
                dc as Graphics.Dc) as Number? {
            /*
            Get index of first line break in string
            */
            
            // Start with a guess based on the available width and the font height
            var guess = Math.floor(width / (0.45 * dc.getFontHeight(font)));
            if (guess >= str.length()) {
                guess = str.length();
            }

            var guessStr = str.substring(0, guess);

            // If guess contains newline, start search at previous character to newline
            // Guess string will be guaranteed not to contain a newline character
            var newlineIndex = guessStr.find("\n");
            if (newlineIndex != null) {
                guess = newlineIndex;
                guessStr = str.substring(0, guess);
            }

            // Determine need for and direction of search depending on text width
            if (dc.getTextWidthInPixels(guessStr, font) > width) {
                // Case: guess exceeds line width, start reverse search.
                var reverseSearchLineBreak = _wordBreakReverseSearch(str, width, font, dc, guess);
                if (reverseSearchLineBreak > 0) {
                    // Case: found the beginning of a word to break on
                    return reverseSearchLineBreak;
                } else {
                    // Case: did not find the beginning of a word to break on. Reasons:
                    // - Only whitespace seen
                    // - Only one word fit followed by whitespace
                    // - All one long word
                    return _wordBreakForwardSearch(str, width, font, dc, guess);
                }
            } else {
                // Case: guess fits within line width
                if (guess == str.length()) {
                    // Case: entire string fits on one line
                    return null;
                } else if (guess == newlineIndex) {
                    // Case: string up to newline index fits on one line
                    return guess;
                } else {
                    // Case: string has more to run and does not contain newline. Start forward search.
                    if (guess == 0) {
                        throw new ValueError("`guess` cannot be 0");
                    }
                    var forwardSearchLineBreak = _wordBreakForwardSearch(str, width, font, dc, guess);
                    if (forwardSearchLineBreak == null) {
                        // Case: entire string fits on one line
                        return null;
                    } else {
                        var char = str.substring(forwardSearchLineBreak, forwardSearchLineBreak + 1);
                        if (char.equals("\n")) {
                            // Case: break on newline
                            return forwardSearchLineBreak;
                        }
                        var prevChar = str.substring(forwardSearchLineBreak - 1, forwardSearchLineBreak);
                        if (prevChar.equals(" ")) {
                            // Case: found beginning of word
                            return forwardSearchLineBreak;
                        } else {
                            // Case: broke mid-word without finding begining of word
                            // Need to reverse and find the beginning of the line or of a word
                            var reverseSearchLineBreak = _wordBreakReverseSearch(str, width, font, dc, guess);
                            prevChar = str.substring(reverseSearchLineBreak - 1, reverseSearchLineBreak);
                            if (prevChar.equals(" ")) {
                                // Case: found the beginning of a word
                                return reverseSearchLineBreak;
                            } else {
                                // Case: didn't find the beginning of a word
                                // Whole line is a single long word. Break mid-word.
                                return forwardSearchLineBreak;
                            }
                        }
                    }
                }
            }
        }

        private static function _wordBreakForwardSearch(str as String, width as Number, font as Graphics.FontReference or Graphics.FontDefinition,
                dc as Graphics.Dc, guess as Number) as Number? {
            // Forward search
            var hasSeenWhitespace = false;
            var lastWordBegin = -1;

            var charType;
            var prevCharType = guess > 0 ? :unknown : :none;  // Guess should never be zero

            var i = guess;
            
            while (i < str.length()) {

                var char = str.substring(i, i + 1).toCharArray()[0];
                if (char == '\n') {
                    charType = :newline;
                } else if (char == ' ') {
                    charType = :whitespace;
                } else {
                    charType = :word;
                }

                if (charType == :newline) {
                    // Always break on newline
                    return i;
                }

                if (charType == :word) {
                    if (prevCharType == :none || prevCharType == :whitespace) {
                        // Keep track of word beginnings as potential break points
                        lastWordBegin = i;
                    }
                    
                    // Test if width exceeded
                    if (dc.getTextWidthInPixels(str.substring(0, i + 1), font) > width) {
                        if (i == 0) {
                            throw new ValueError(
                                "Could not fit a single character in given width");
                        }

                        if (hasSeenWhitespace) {
                            // Break at beginning of word
                            return lastWordBegin;
                        } else {
                            // Case: started mid-word and hit line end
                            // It's possible you are reading German and a single word won't fit on a line
                            // e.g. Donaudampfschiffahrtsgesellschaftskapitän
                            // Or we started in the middle of a word and need to search backwards now
                            // Either way, we'll mark this point and start a reverse search if not already done
                            return i;
                        }
                    }
                } else if (charType == :whitespace) {
                    hasSeenWhitespace = true;
                }

                prevCharType = charType;
                i++;
            }

            // Case: end forward search without exceeding width, i.e. full string fits on one line, or string is entirely whitespace
            return null;
        }

        private static function _wordBreakReverseSearch(str as String, width as Number, font as Graphics.FontReference or Graphics.FontDefinition,
                dc as Graphics.Dc, guess as Number) as Number {
            // Reverse search
            var widthExceedsPoint = 0;
            var lastWordBegin = -1;
            
            var char = str.substring(guess, guess + 1);
            var charType;
            if (char == '\n') {
                charType = :newline;
            } else if (char == ' ') {
                charType = :whitespace;
            } else {
                charType = :word;
            }

            var i = guess;

            while (i > 0) {
                var prevChar = str.substring(i - 1, i);
                var prevCharType = 
                    prevChar.equals(" ") ? :whitespace :
                    :word;

                if (charType == :word) {
                    if (prevCharType == :whitespace) {
                        // Found the start of a word: potential break point
                        lastWordBegin = i;
                    }
                    if (dc.getTextWidthInPixels(str.substring(0, i), font) <= width) {
                        if (lastWordBegin > -1) {
                            return lastWordBegin;
                        } else if (widthExceedsPoint == 0) {
                            // Store exact point where width no longer exceeded
                            widthExceedsPoint = i;
                        }
                    }
                } else if ((charType == :whitespace || charType == :newline) && prevCharType == :word) {
                    // Found the end of a word
                    if (lastWordBegin > -1) {
                        // If already seen the beginning of a word that doesn't fit, test if previous words fit
                        if (dc.getTextWidthInPixels(str.substring(0, i), font) <= width) {
                            // Include trailing whitespace by breaking at next word beginning
                            return lastWordBegin;
                        }
                    }
                }

                charType = prevCharType;
                i--;
            }

            // Hit start of line
            if (lastWordBegin > -1) {
                // Only leading whitespace on line
                return lastWordBegin;
            } else {
                // Found break point in middle of word only
                return widthExceedsPoint;
            }
        }
    }


    (:test)
    class _MockDc {
        /*
        Mock device context

        getTextWidthInPixels will just return the # of chars in the text
        getFontHeight will just return a fixed number;
        */
        private var _textWidth as Float;
        private var _fontHeight as Float;

        function initialize(options as {:textWidth as Float, :fontHeight as Float}) {
            if (options[:textWidth] != null) {
                _textWidth = options[:textWidth];
            } else {
                _textWidth = 1.0;
            }
            if (options[:textWidth] != null) {
                _fontHeight = options[:fontHeight];
            } else {
                _fontHeight = 3.2;
            }
        }

        function getTextWidthInPixels(str as String, font as Number) as Number {
            return Math.ceil(_textWidth * str.length());
        }

        function getFontHeight(font as Number) as Float {
            return _fontHeight;
        }
    }


    (:test)
    function testTextWrapper(logger as Logger) as Boolean {

        var dc = new _MockDc({});
        var testStr;
        var obj;

        // Hello World! test
        testStr = "Hello\n\nWorld!";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 5, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "Hello",
            "",
            "World",
            "!",
        ]);

        // Ignore trailing space
        testStr = "Hello            You        \n  Guy";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 5, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "Hello",
            "You",
            "  Guy",
        ]);

        // Preserve leading space
        testStr = " A B";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 2, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            " A",
            "B",
        ]);

        // Can have only leading space
        // Will lead to blank lines
        testStr = "            AB";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 5, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "",
            "AB",
        ]);

        // Do not break mid-word
        testStr = "Great Expectations";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 12, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "Great",
            "Expectations",
        ]);

        // Preserve last word before newline
        testStr = "confident, clear orders.\n";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 30, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "confident, clear orders.",
            "",
        ]);

        // Test empty string
        testStr = "";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 30, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "",
        ]);

        // Test entire string fits on one line
        testStr = "howdy pardner how yall doin";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 28, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "howdy pardner how yall doin",
        ]);

        // Test overshoot with reverse search
        dc = new _MockDc({:textWidth => 1.01, :fontHeight => 2.5});
        testStr = "are we going to reimplement a thingy?";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 28, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "are we going to reimplement",
            "a thingy?"
        ]);

        // Test overshoot with reverse search on single long word
        testStr = "abcdefghijklmnopqrstuvwxyzABC?";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 28, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "abcdefghijklmnopqrstuvwxyzA",
            "BC?"
        ]);

        // Test overshoot with reverse search on word with lots of leading whitespace
        testStr = "                              ABC?";
        System.println("Test string: \"" + testStr + "\"");
        obj = new TextWrapper(testStr, 28, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput([
            "",
            "ABC?"
        ]);

        return true;
    }


    (:test)
    function testTextWrapperPeekAndHasNext(logger as Logger) as Boolean {

        var dc = new _MockDc({});

        var testStr = "ABCDEF";
        var obj = new TextWrapper(testStr, 3, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);

        Test.assertEqual(obj.next(), "ABC");
        Test.assert(obj.hasNext());

        Test.assertEqual(obj.peek(), "DEF");
        Test.assertEqual(obj.next(), "DEF");

        Test.assert(!obj.hasNext());
        Test.assert(obj.peek() == null);
        Test.assert(obj.next() == null);

        return true;
    }


    (:test)
    function testTextWrapperReset(logger as Logger) as Boolean {

        var dc = new _MockDc({});

        var testStr = "ABCDEF";
        var expectedOutput = [
            "ABC",
            "DEF",
        ];
        var obj = new TextWrapper(testStr, 3, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput(expectedOutput);
        obj.reset();
        obj.testAgainstOutput(expectedOutput);

        return true;
    }


    (:test)
    function testTextWrapperMultipleLines(logger as Logger) as Boolean {

        var dc = new _MockDc({});

        var testStr = new ArrayIterator(["ABCDE", "FG", "HIJKLMNOP", ""]);
        var expectedOutput = [
            "ABC",
            "DE",
            "FG",
            "HIJ",
            "KLM",
            "NOP",
            ""
        ];
        var obj = new TextWrapper(testStr, 3, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        obj.testAgainstOutput(expectedOutput);
        obj.reset();
        obj.testAgainstOutput(expectedOutput);

        return true;
    }


    (:test)
    function testTextWrapperGetAndSize(logger as Logger) as Boolean {

        var dc = new _MockDc({});

        var testStr = new ArrayIterator(["ABCDE", "FG", "HIJKLMNOP", ""]);
        var obj;
        
        obj = new TextWrapper(testStr, 3, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(0), "ABC");
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.peek(), "DE");
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.get(1), "DE");
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.peek(), "FG");
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.get(2), "FG");
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.peek(), "HIJ");
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.get(3), "HIJ");
        Test.assertEqual(obj.getSeek(), 4);
        Test.assertEqual(obj.peek(), "KLM");
        Test.assertEqual(obj.getSeek(), 4);
        Test.assertEqual(obj.get(4), "KLM");
        Test.assertEqual(obj.getSeek(), 5);
        Test.assertEqual(obj.peek(), "NOP");
        Test.assertEqual(obj.getSeek(), 5);
        Test.assertEqual(obj.get(5), "NOP");
        Test.assertEqual(obj.getSeek(), 6);
        Test.assertEqual(obj.peek(), "");
        Test.assertEqual(obj.getSeek(), 6);
        Test.assertEqual(obj.get(6), "");
        Test.assertEqual(obj.getSeek(), 7);
        Test.assert(obj.peek() == null);
        Test.assertEqual(obj.getSeek(), 7);
        Test.assertEqual(obj.size(), 7);

        testStr.reset();
        obj = new TextWrapper(testStr, 3, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(6), "");
        Test.assertEqual(obj.getSeek(), 7);
        Test.assert(obj.peek() == null);
        Test.assertEqual(obj.getSeek(), 7);
        Test.assertEqual(obj.get(5), "NOP");
        Test.assertEqual(obj.getSeek(), 6);
        Test.assertEqual(obj.peek(), "");
        Test.assertEqual(obj.getSeek(), 6);
        Test.assertEqual(obj.get(4), "KLM");
        Test.assertEqual(obj.getSeek(), 5);
        Test.assertEqual(obj.peek(), "NOP");
        Test.assertEqual(obj.getSeek(), 5);
        Test.assertEqual(obj.get(3), "HIJ");
        Test.assertEqual(obj.getSeek(), 4);
        Test.assertEqual(obj.peek(), "KLM");
        Test.assertEqual(obj.getSeek(), 4);
        Test.assertEqual(obj.get(2), "FG");
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.peek(), "HIJ");
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.get(1), "DE");
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.peek(), "FG");
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.get(0), "ABC");
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.next(), "DE");
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.size(), 7);
        obj.reset();
        Test.assertEqual(obj.getSeek(), 0);

        testStr.reset();
        obj = new TextWrapper(testStr, 3, Graphics.FONT_SMALL);
        obj.setDc(dc as Graphics.Dc);
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.size(), 7);

        return true;
    }
}
