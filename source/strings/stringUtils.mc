import Toybox.Lang;
using Toybox.Graphics;
using Toybox.StringUtil;


module Codex {

    class StringUtils {

        static function removeLeadingWhitespace(str as String, removeNewlines as Boolean) {
            var chars = str.toCharArray();
            var numLeadingWhitespace = 0;
            for (var i = 0; i < chars.size(); i++) {
                var char = chars[i];
                if (char == ' ' || char == '\t' || (removeNewlines && char == '\n')) {
                    numLeadingWhitespace = i + 1;
                } else {
                    break;
                }
            }
            return str.substring(numLeadingWhitespace, str.length());
        }

        static function removeTrailingWhitespace(str as String, removeNewlines as Boolean) {
            var lastNonWhitespace = -1;
            var chars = str.toCharArray();
            for (var i = chars.size() - 1; i >= 0; i--) {
                var char = chars[i];
                if (!(char == ' ' || char == '\t' || (removeNewlines && char == '\n'))) {
                    lastNonWhitespace = i;
                    break;
                }
            }
            return str.substring(0, lastNonWhitespace + 1);
        }

        static function trim(str as String, removeNewlines as Boolean) {
            return removeTrailingWhitespace(removeLeadingWhitespace(str, removeNewlines), removeNewlines);
        }

        static function padRight(str as String, numChars as Number, padChar as Char) as String {
            // Pad a string with a char up to numChars
            var chars = str.toCharArray();
            if (chars.size() > numChars) {
                throw new ValueError(
                    "Length of `str` is greater than `numChars`");
            }
            var numPadChars = numChars - chars.size();
            for (var i = 0; i < numPadChars; i++) {
                chars.add(padChar);
            }
            return StringUtil.charArrayToString(chars);
        }

        static function isAlphanumeric(string as String) as Boolean {
            // Test if string is alphanumeric
            var chars = string.toCharArray();
            for (var i = 0; i < chars.size(); i++) {
                var char = chars[i];
                if (!((char >= 'a' && char <= 'z') || (char >= 'A' && char <= 'Z') || 
                    (char >= '0' && char <= '9'))) {
                    return false;
                }
            }
            return true;
        }

        static function replaceAll(searchFor as String, replaceWith as String, str as String) as String {
            // Replace all instances of searchFor with replaceWith in str
            // Will not converge if replaceWith contains searchFor
            while (true) {
                var occurrence = str.find(searchFor);
                if (occurrence != null) {
                    str = 
                        str.substring(0, occurrence) + 
                        replaceWith + 
                        str.substring(occurrence + 1, str.length());
                } else {
                    break;
                }
            }
            return str;
        }

        static function join(lines as Array<String>, token as String) as String {
            /*
            Join an array of strings with a token
            */
            var result = "";
            for (var i = 0; i < lines.size(); i++) {
                if (i > 0) {
                    result += token;
                }
                result += lines[i];
            }
            return result;
        }

        static function split(str as String, token as String, allowMultiple as Boolean) as Array<String> {
            /*
            Split a string into an array of strings based on a token
            */
            var result = [];
            while (true) {
                var occurrence = str.find(token);
                if (occurrence != null) {
                    result.add(str.substring(0, occurrence));
                    str = str.substring(occurrence + 1, str.length());
                    if (!allowMultiple) {
                        // Stop on first occurrence
                        result.add(str);
                        return result;
                    }
                } else {
                    result.add(str);
                    break;
                }
            }
            return result;
        }

        static function unquote(str as String) as String {
            /*
            Strip single leading and trailing quotes off of string, if present

            Handles '', "", and “”
            */
            var len = str.length();
            var firstChar = str.substring(0, 1).toCharArray()[0];
            var lastChar = str.substring(len - 1, len).toCharArray()[0];
            if ((firstChar == '\'' && lastChar == '\'') || ((firstChar == '"' || firstChar == '“' || firstChar == '”') && (lastChar == '"' || lastChar == '“' || lastChar == '”'))) {
                return str.substring(1, len - 1);
            } else {
                return str;
            }
        }

        static function repeat(str as String, n as Number) {
            /*
            Repeat a string n times
            */
            var result = "";
            for (var i = 0; i < n; i++) {
                result += str;
            }
            return result;
        }

        static function wrap(string as String, width as Number, font as Graphics.FontReference, dc as Graphics.Dc) as Array<String> {
            /*
            Wrap a string into an array of line strings
            */
            var result = [];
            while (string.length() > 0) {
                var split = TextWrapper.getLineBreak(string, width, font, dc);
                var line = string.substring(0, split != null ? split : string.length());
                result.add(removeTrailingWhitespace(line, true));
                if (split == null) {
                    break;
                } else {
                    var endsOnNewline = string.substring(split, split + 1).equals("\n");
                    string = string.substring(endsOnNewline ? split + 1 : split, string.length());
                }
            }
            return result;
        }

        static function wrapLine(string as String, width as Number, font as Graphics.FontReference, dc as Graphics.Dc) as String {
            /*
            Wrap a string into a single line
            */
            var split = TextWrapper.getLineBreak(string, width, font, dc);
            var line = string.substring(0, split != null ? split : string.length());
            return removeTrailingWhitespace(line, true);
        }
    }
}
