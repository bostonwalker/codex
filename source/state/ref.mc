import Toybox.Lang;
import Toybox.Test;
import Toybox.System;


module Codex {

    // Typedefs that attach a type to the Ref to make things more readable
    // Cannot actually guarantee the type of Ref.value(), since MonkeyC lacks generics
    typedef BooleanRef as Boolean or Ref;
    typedef NumberRef as Number or Ref;
    typedef LongRef as Long or Ref;
    typedef FloatRef as Float or Ref;
    typedef DoubleRef as Double or Ref;
    typedef NumericRef as Numeric or Ref;
    typedef SymbolRef as Symbol or Ref;
    typedef CharRef as Char or Ref;
    typedef StringRef as String or Ref;
    typedef ArrayRef as Array or Ref;
    typedef ByteArrayRef as ByteArray or Ref;
    typedef DictionaryRef as Dictionary or Ref;

    class Ref {
        
        enum UpdateMode {
            UPDATE_MODE_DISABLED,  // Neither Codex.getApp().requestUpdate() or requestPartialUpdate() will be called on value change
            UPDATE_MODE_ENABLED_PARTIAL,  // Codex.getApp().requestPartialUpdate() will be called on value change
            UPDATE_MODE_ENABLED,  // Codex.getApp().requestUpdate() will be called on value change
        }

        typedef Listener as [WeakReference, Symbol, Boolean];

        protected var _value as Object?;
        protected var _updateMode as UpdateMode;
        protected var _listeners as Array<Listener>;

        function initialize(initialValue as Object?) {
            /*
            :param initialValue: Initial value for this Ref
            */
            _value = initialValue;
            _updateMode = UPDATE_MODE_DISABLED;
            _listeners = [];
        }

        function value() as Object? {
            return _value;
        }

        function setUpdateMode(updateMode as UpdateMode) as Void {
            _updateMode = updateMode;
        }

        function enableUpdatesOnChange() as Void {
            // Cause Codex.getApp().requestUpdate() to be called when value changes
            if (_updateMode < UPDATE_MODE_ENABLED) {
                _updateMode = UPDATE_MODE_ENABLED;
            }
        }

        function enablePartialUpdatesOnChange() as Void {
            // Cause Codex.getApp().requestPartialUpdate() to be called when value changes
            if (_updateMode < UPDATE_MODE_ENABLED_PARTIAL) {
                _updateMode = UPDATE_MODE_ENABLED_PARTIAL;
            }
        }

        function update(value as Object?) as Void {
            _value = value;
            var overwriteIndex = 0;
            for (var i = 0; i < _listeners.size(); i++) {
                var listener = _listeners[i];
                var ref = listener[0];
                if (ref.stillAlive()) {
                    var object = ref.get();
                    var method = new Method(object, listener[1]);
                    var isLinked = listener[2];
                    if (isLinked) {
                        method.invoke(self);
                    } else {
                        method.invoke(value);
                    }
                    _listeners[overwriteIndex] = listener;
                    overwriteIndex ++;
                }
            }
            // Cull dead listeners as we go
            _listeners = _listeners.slice(0, overwriteIndex);
            // Update UI as appropriate
            if (_updateMode == UPDATE_MODE_ENABLED_PARTIAL) {
                Codex.getApp().requestPartialUpdate();
            } else if (_updateMode == UPDATE_MODE_ENABLED) {
                Codex.getApp().requestUpdate();
            }
        }

        function addListener(object as Object, method as Symbol) as Void {
            _addListener(object, method, false);
        }

        function removeListener(object as Object, method as Symbol) as Void {
            _removeListener(object, method, false);
        }

        function _addListener(object as Object, method as Symbol, linked as Boolean) as Void {
            if (!(object has method)) {
                // Check that `method` is valid
                throw new KeyError(
                    "method `" + method.toString() + "` not found for object: " + object.toString());
            }
            _listeners.add([object.weak(), method, true]);
        }

        function _removeListener(object as Object, method as Symbol, linked as Boolean) as Void {
            var overwriteIndex = 0;
            for (var i = 0; i < _listeners.size(); i++) {
                var listener = _listeners[i];
                var weakRef = listener[0];
                if (weakRef.stillAlive() && weakRef.get() == object) {
                    var listenerMethod = listener[1];
                    var listenerIsLinked = listener[2];
                    if (listenerMethod == method && listenerIsLinked == linked) {
                        // Skip this index, causing overwrite and removal
                        continue;
                    }
                }
                _listeners[overwriteIndex] = listener;
                overwriteIndex ++;
            }
            if (overwriteIndex == _listeners.size()) {
                throw new KeyError(
                    "Existing listener not found for object: " + object.toString() + " and method: " + method.toString());
            }
            _listeners = _listeners.slice(0, overwriteIndex);
        }

        function toString() {
            if (_value != null) {
                return "Ref{value=" + _value.toString() + "}";
            } else {
                return "Ref{value=null}";
            }
        }

        function equals(object) as Boolean {
            return object instanceof Ref &&
                _value.equals(object.value());
        }

        function greaterThan(object as Object) as BooleanRef {
            return new _GreaterThanRef(self, object);
        }

        /*
        Linking functionality

        Keeps persistent references to Ref objects while they are linked to objects so that they will stay alive.
        If there was a previous value linked to the object/setter, linking the new value will dereference the old 
            value and remove any zombie listeners.
        */
        protected static var _links = {} as Dictionary<Object, Dictionary<Symbol, Ref>>;
        
        (:typecheck(false))
        static function link(object as Object, setter as Symbol, arg as Ref or Object or Null) as Object? {
            /*
            Handle Ref objects as passed parameters, while also allowing for normal objects and null values.
            
            :param object: Object with setter method.
            :param setter: Setter to link to, if `obj` is a Ref.
            :param arg: Passed parameter. Can either be a Ref, a normal Object, or Null.
            :return: If `arg` is a Ref, the underlying value. Otherwise, will just return `arg`.
            */
            if (arg instanceof Ref) {
                // Create persistent reference to Ref object
                if (!_links.hasKey(object)) {
                    // Previous value was not a linked Ref
                    _links[object] = {setter => arg};
                    arg._addListener(object, setter, true);
                } else {
                    var objectLinks = _links[object];
                    if (objectLinks.hasKey(setter)) {
                        // Existing linkage
                        var ref = objectLinks[setter];
                        if (ref != arg) {
                            // Replace listener
                            ref._removeListener(object, setter, true);
                            arg._addListener(object, setter, true);
                            // Replace reference (will cause a dereference of the old Ref object)
                            objectLinks[setter] = arg;
                        }
                        // If ref == arg, no action required
                    } else {
                        // No existing Ref linked, link away
                        arg._addListener(object, setter, true);
                        objectLinks[setter] = arg;
                    }
                }
                // Resolve value
                var value = arg.value();
                return value;
            } else {
                if (_links.hasKey(object)) {
                    // Handle existing linkage
                    var objectLinks = _links[object];
                    if (objectLinks.hasKey(setter)) {
                        // Existing linkage
                        var ref = objectLinks[setter];
                        // Remove listener to prevent zombie callback
                        ref._removeListener(object, setter, true);
                        // Dereference
                        objectLinks.remove(setter);
                        if (objectLinks.size() == 0) {
                            // Clean up empty dict
                            _links.remove(object);
                        }
                    }
                }
                return arg;
            }
        }

        // Convenience functions with stricter types
        static function linkBoolean(object as Object, setter as Symbol, arg as BooleanRef) as Boolean {
            return link(object, setter, arg) as Boolean;
        }

        static function linkNumber(object as Object, setter as Symbol, arg as NumberRef) as Number {
            return link(object, setter, arg) as Number;
        }

        static function linkLong(object as Object, setter as Symbol, arg as LongRef) as Long {
            return link(object, setter, arg) as Long;
        }

        static function linkFloat(object as Object, setter as Symbol, arg as FloatRef) as Float {
            return link(object, setter, arg) as Float;
        }

        static function linkDouble(object as Object, setter as Symbol, arg as DoubleRef) as Double {
            return link(object, setter, arg) as Double;
        }

        static function linkNumeric(object as Object, setter as Symbol, arg as NumericRef) as Numeric {
            return link(object, setter, arg) as Numeric;
        }

        static function linkSymbol(object as Object, setter as Symbol, arg as SymbolRef) as Symbol {
            return link(object, setter, arg) as Symbol;
        }

        static function linkChar(object as Object, setter as Symbol, arg as CharRef) as Char {
            return link(object, setter, arg) as Char;
        }

        static function linkString(object as Object, setter as Symbol, arg as StringRef) as String {
            return link(object, setter, arg) as String;
        }

        static function linkArray(object as Object, setter as Symbol, arg as ArrayRef) as Array {
            return link(object, setter, arg) as Array;
        }

        static function linkByteArray(object as Object, setter as Symbol, arg as ByteArrayRef) as ByteArray {
            return link(object, setter, arg) as ByteArray;
        }

        static function linkDictionary(object as Object, setter as Symbol, arg as DictionaryRef) as Dictionary {
            return link(object, setter, arg) as Dictionary;
        }
    }


    class _TestListener {

        private var _id as String;

        function initialize(id as String) {
            _id = id;
        }
        
        function onUpdate(value as Object) {
            System.println("listener " + _id + ": onUpdate(" + value.toString() + ")");
        }
    }


    (:test)
    function testRefListenersDereferencing(logger as Logger) as Boolean {
        var ref = new Ref(0);
        var listener = new _TestListener("Foo");
        ref.addListener(listener, :onUpdate);
        ref.update(1);

        listener = new _TestListener("Bar");
        ref.addListener(listener, :onUpdate);
        // Should not throw error
        ref.update(2);
        
        return true;
    }
}


