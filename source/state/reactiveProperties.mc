import Toybox.Lang;
import Toybox.Application;


module Codex {
    
    class ReactiveProperties extends Reactive {
        /*
        Reactive state that takes its value from Application properties
        */
        function initialize() {
            // Initialize with empty state
            Reactive.initialize({});
        }

        function get(key as String or Symbol) as Ref {
            key = key as String;
            if (!_state.hasKey(key)) {
                // Initialize from properties if not already accessed
                var initialValue = Application.Properties.getValue(key);
                _state[key] = new Ref(initialValue);
            }
            return _state[key];
        }

        function set(key as String or Symbol, value as Object?) as Void {
            key = key as String;
            Reactive.set(key, value);
            Application.Properties.setValue(key, value);
        }
    }
}
