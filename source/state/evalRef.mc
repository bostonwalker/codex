import Toybox.Lang;


module Codex {

    class EvalRef extends Ref {
        /*
        Ref that derives its value from evaluating the value of another ref
        */
        protected var _src as Ref;

        function initialize(src as Ref) {
            _src = src;
            var initialValue = _eval(src.value());
            Ref.initialize(initialValue);
            Ref.link(self, :_onSrcUpdated, src);
        }

        protected function _eval(value as Object?) as Object? {
            // Abstract method that defines how this EvalRef works
            throw new NotImplementedError();
        }

        function _onSrcUpdated(src as Codex.Ref) as Void {
            var srcValue = src.value();
            var newValue = _eval(srcValue);
            if (_value != newValue) {
                update(newValue);
            }
        }
    }

    class _GreaterThanRef extends EvalRef {

        var _referenceValue as Object;
        
        function initialize(src as Ref, referenceValue as Object) {
            _referenceValue = referenceValue;
            EvalRef.initialize(src);
        }

        (:typecheck(false))
        protected function _eval(value as Object?) as Object? {
            // Abstract method that defines how this EvalRef works
            if (value == null) {
                throw new ValueError(
                    "null value");
            }
            return value > _referenceValue;
        }

        function toString() as String {
            return "(" + _src.toString() + " > " + _referenceValue.toString() + ")";
        }
    }
}
