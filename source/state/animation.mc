import Toybox.Lang;
using Toybox.System;


module Codex {

    class _Blink extends Ref {
        /*
        Value that is false when system time / duration is even and true when odd

        Uses system clock to ensure predictable behavior, all Blink instances with the same duration will blink on/off at the same time
        */

        protected var _duration as Number;
        protected var _timer as Timer;

        function initialize(duration as Number) {
            _duration = duration;
            _timer = new Timer(method(:_tick));
            _timer.startAtIntervals(duration);
            var currentTime = System.getTimer();
            Ref.initialize(_getValue(currentTime));
        }

        function stop() as Void {
            _timer.stop();
        }

        function _tick(currentTime as Number) as Void {
            update(_getValue(currentTime));
        }

        function _getValue(currentTime as Number) as Boolean {
            return ((currentTime / _duration) % 2) == 1;
        }

        function _getTimeUntilNextTick(currentTime as Number) as Number {
            return _duration - (currentTime % _duration);
        }
    }


    class _Disappear extends Ref {
        /*
        Value that is true for a duration and then false
        */
        protected var _timer as Timer;

        function initialize(duration as Number) {
            _timer = new Timer(method(:_disappear));
            _timer.startFor(duration);
            Ref.initialize(true);
        }

        function _getValue(currentTime as Number) as Boolean {
            return true;
        }

        function _disappear(unused as Number) as Void {
            update(false);
        }
    }


    class Animation {
        // Object for animating object properties

        protected static const BLINK_DURATION_DEFAULT = 500;
        protected static const DISAPPEAR_DURATION_DEFAULT = 1500;

        static function blink(options as {:duration as Number, :updateMode as Ref.UpdateMode}) as _Blink {
            // Factory method for Animation._Blink object
            var duration = Utils.getDefault(options, :duration, BLINK_DURATION_DEFAULT) as Number;
            var updateMode = Utils.getDefault(options, :updateMode, Ref.UPDATE_MODE_ENABLED) as Ref.UpdateMode;
            var animation = new _Blink(duration);
            animation.setUpdateMode(updateMode);
            return animation;
        }

        static function disappear(options as {:duration as Number, :updateMode as Ref.UpdateMode}) as _Disappear {
            // Factory method for Animation._Disappear object
            var duration = Utils.getDefault(options, :duration, DISAPPEAR_DURATION_DEFAULT) as Number;
            var updateMode = Utils.getDefault(options, :updateMode, Ref.UPDATE_MODE_ENABLED) as Ref.UpdateMode;
            var animation = new _Disappear(duration);
            animation.setUpdateMode(updateMode);
            return animation;
        }
    }
}
