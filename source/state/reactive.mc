import Toybox.Lang;


module Codex {

    class Reactive {

        protected var _state as Dictionary<String or Symbol, Ref>;

        function initialize(initialValues as Dictionary<String or Symbol, Object?>) {

            _state = {};

            var keys = initialValues.keys();
            var values = initialValues.values();
            for (var i = 0; i < keys.size(); i++) {
                var key = keys[i];
                var value = values[i];
                _state[key] = new Ref(value);
            }
        }

        function set(key as String or Symbol, value as Object?) as Void {
            if (_state.hasKey(key)) {
                _state[key].update(value);
            } else {
                _state[key] = new Ref(value);
            }
        }

        function get(key as String or Symbol) as Ref {
            var value = _state[key];
            if (value != null) {
                return value;
            } else {
                throw new KeyError(key.toString());
            }
        }
    }
}
