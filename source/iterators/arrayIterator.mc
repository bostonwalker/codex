import Toybox.Lang;
import Toybox.Test;


module Codex {
    
    class ArrayIterator extends BaseIterator {

        private var _items as Array;

        function initialize(items as Array) {
            /*
            An iterator that wraps an array.
    
            :param items: An array of items.
            */
            BaseIterator.initialize();

            _items = items;
        }

        function asArray() as Array {
            return _items;
        }

        function exploit() as Void {
            // Do nothing, ArrayIterators are initialized exploited
        }

        function get(index as Number) as Object {
            if (index >= 0 && index < _items.size()) {
                seek = index + 1;
                return _items[index];
            }
            throw new ValueError(
                "Item " + index.toString() + " of ArrayIterator does not exist");
        }

        function hasNext() as Boolean {
            return seek < _items.size();
        }

        function next() {
            var result;
            if (seek < _items.size()) {
                result = _items[seek];
                seek ++;
            } else {
                result = null;
            }
            return result;
        }

        function peek() {
            return seek < _items.size() ? _items[seek] : null;
        }

        function peekAt(index as Number) as Object {
            if (index >= 0 && index < _items.size()) {
                return _items[index];
            }
            throw new ValueError(
                "Item " + index.toString() + " of ArrayIterator does not exist");
        }

        function setSeek(seek as Number) {
            // Set seek of ArrayIterator
            if (seek >= 0 && seek <= _items.size()) {
                self.seek = seek;
            } else {
                throw new ValueError(
                    "Item " + seek.toString() + " of ArrayIterator does not exist");
            }
        }

        function size() as Number {
            return _items.size();
        }

        function toString() as String {
            return "ArrayIterator{items=" + _items.toString() + "}";
        }
    }


    (:test)
    function testArrayIterator(logger as Logger) as Boolean {

        var testInput;
        var testOutput;
        var obj;

        // Basic test
        testInput = ['A', 'B', 'C'];
        testOutput = testInput;

        obj = new ArrayIterator(testInput);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        // Test case: zero length
        testInput = [];
        testOutput = testInput;

        obj = new ArrayIterator(testInput);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        // Test case: step and reset
        testInput = ['A', 'B', 'C'];
        obj = new ArrayIterator(testInput);
        Test.assertEqual('A', obj.peek());
        Test.assert(obj.hasNext());
        Test.assert(!obj.isIterating());
        Test.assertEqual('A', obj.next());
        Test.assert(obj.hasNext());
        Test.assert(obj.isIterating());
        Test.assertEqual('B', obj.next());
        obj.reset();
        Test.assert(obj.hasNext());
        Test.assert(!obj.isIterating());
        Test.assertEqual('A', obj.next());
        Test.assertEqual('B', obj.next());
        Test.assertEqual('C', obj.next());
        Test.assert(obj.peek() == null);
        Test.assert(obj.next() == null);
        Test.assert(!obj.hasNext());
        Test.assert(obj.isIterating());

        // Test behaviour around null values
        testInput = [0, 3, 4, 6, null, 6];
        testOutput = [0, 3, 4, 6];

        obj = new ArrayIterator(testInput);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        return true;
    }


    (:test)
    function testArrayIteratorGetAndSize(logger as Logger) as Boolean {

        var testInput = ['A', 'B', 'C'];
        var obj = new ArrayIterator(testInput);

        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(2), 'C');
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.get(1), 'B');
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.get(0), 'A');
        Test.assertEqual(obj.getSeek(), 1);
        obj.reset();
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(0), 'A');
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.peek(), 'B');
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.get(1), 'B');
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.peek(), 'C');
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.get(2), 'C');
        Test.assertEqual(obj.getSeek(), 3);
        Test.assert(obj.peek() == null);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.get(0), 'A');
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.next(), 'B');
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.size(), 3);
        obj.reset();
        Test.assertEqual(obj.size(), 3);
        obj.reset();
        obj.setSeek(2);
        Test.assertEqual(obj.next(), 'C');
        obj.setSeek(0);
        Test.assertEqual(obj.next(), 'A');

        return true;
    }
}
