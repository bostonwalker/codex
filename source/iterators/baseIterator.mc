import Toybox.Lang;
import Toybox.System;


module Codex {

    class BaseIterator {  // implements Iterator

        protected var seek as Number;

        function initialize() {
            /*
            An iterator interface implementing next(), hasNext(), peek(), and other useful methods
            */
            seek = 0;
        }

        function asArray() as Array {
            // Return contents as array
            if (isIterating()) {
                throw new Lang.OperationNotAllowedException(
                    "Cannot call asArray() on Iterator while in a loop");
            }
            var array = [];
            while (true) {
                var item = next();
                if (item != null) {
                    array.add(item);
                } else {
                    break;
                }
            }
            reset();
            // Exclude null at end
            return array;
        }

        function equals(object) as Boolean {
            // Check equality of remaining children
            // Iterators should be reset before calling equals()
            if (!(object instanceof BaseIterator)) {
                return false;
            }
            var other = object as BaseIterator;
            if (isIterating() || other.isIterating()) {
                throw new Lang.OperationNotAllowedException(
                    "Cannot call equals() on iterator while in a loop");
            }
            var result = true;
            while (hasNext() && other.hasNext()) {
                if (!next().equals(other.next())) {
                    result = false;
                    break;
                }
            }
            if (hasNext() || other.hasNext()) {
                result = false;
            }
            reset();
            other.reset();
            return result;
        }

        function exploit() as Void {
            // Exploit the iterator and reset seek
            var currentSeek = seek;
            while (next() != null) {}
            seek = currentSeek;
        }

        function get(index as Number) as Object {
            // Get the nth item in the iterator
            if (seek != index) {
                setSeek(index);
            }
            var item = next();
            if (item != null) {
                return item;
            }
            throw new ValueError(
                "Iterator exhausted at item " + index.toString());
        }

        function getSeek() as Number {
            // Get index of next item produced by iterator
            return seek;
        }

        function hasNext() as Boolean {
            return peek() != null; 
        }

        function isIterating() as Boolean {
            // Return true if next() has been called at least once since last reset
            return seek > 0;
        }

        function next() as Object? {
            // Return next item, or null if not exists
            var result = peek();
            if (result != null) {
                seek ++;
            }
            return result;
        }

        function peek() as Object? {
            throw new Lang.OperationNotAllowedException(
                "Not implemented");
        }

        function peekAt(index as Number) as Object {
            // Like get(), but doesn't modify the seek
            var curSeek = getSeek();
            setSeek(index);
            var result = peek();
            setSeek(curSeek);
            if (result != null) {
                return result;
            } else {
                throw new ValueError(
                    "Item " + index.toString() + " of Iterator does not exist");
            }
        }

        function reset() as Void {
            // Reset iterator to top
            setSeek(0);
        }

        function setSeek(seek as Number) as Void {
            // Set seek of iterator
            throw new Lang.OperationNotAllowedException(
                "Not implemented");
        }

        function size() as Number {
            // Return number of items in iterator
            throw new Lang.OperationNotAllowedException(
                "Not implemented");
        }

        function testAgainstOutput(expectedItems as Array) as Boolean {
            /*
            Test an iterator versus an array of expected items. Iterator should produce the expected items in
            order and then produce null once the array is exhausted.

            :param iterator: Iterator to test
            :param expectedItems: Array of expected items
            :return: true if no exceptions raised
            */
            var item;
            for (var i = 0; i < expectedItems.size(); i++) {
                item = next();
                if (item == null) {
                    Test.assertMessage(false, "Iterator exhausted with items remaining: " + 
                                            expectedItems.slice(i, null).toString());
                }
                System.println("Item " + i.toString() + ": " + item.toString());
                Test.assertEqualMessage(item, expectedItems[i], 
                                        "Comparison failure, item: " + item.toString() + 
                                        ", expected item: " + expectedItems[i].toString());
            }
            item = next();
            if (item != null) {
                Test.assertMessage(false, "Iterator had excess items remaining: " + item.toString());
            }

            return true;
        }
    }
}
