import Toybox.Lang;
import Toybox.System;
import Toybox.Test;


module Codex {
        
    class Range extends BaseIterator {

        // Non-state variables
        private var _input as BaseIterator;
        private var _startIndex as Number;
        private var _endIndex as Number?;

        // State variables
        private var _seenIndex as Number?;

        function initialize(input as BaseIterator, startIndex as Number?, endIndex as Number?) {
            /*
            An iterator that takes another iterator as an input and outputs a range of indices into that iterator

            :param input: Input iterator
            :param startIndex: Start index (if null, defaults to 0)
            :param endIndex: End index (can be null)
            */
            BaseIterator.initialize();

            if (startIndex == null) {
                startIndex = 0;
            }

            if (startIndex < 0 || (endIndex != null && endIndex < startIndex)) {
                throw new ValueError(
                    "Invalid arguments for Range: startIndex=" + startIndex.toString() + 
                    ", endIndex=" + endIndex.toString());
            }

            _input = input;
            _startIndex = startIndex;
            _endIndex = endIndex;

            _seenIndex = _startIndex - 1;
        }

        function next() as Object? {
            var result;
            if (_endIndex != null && seek == _endIndex - _startIndex) {
                // At end of range
                result = null;
            } else if (_startIndex + seek > _seenIndex) {
                // Unknown if input has next item
                _input.setSeek(_startIndex + seek);
                result = _input.next();
                if (result == null) {
                    // Reached end of range
                    _endIndex = _startIndex + seek;
                } else {
                    _seenIndex = _startIndex + seek;
                    seek ++;
                }
            } else {
                // Item seen already
                result = _input.peekAt(_startIndex + seek);
                seek ++;
            }
            return result;
        }

        function peek() as Object? {
            var result;
            if (_endIndex != null && seek == _endIndex - _startIndex) {
                // At end of range
                result = null;
            } else if (_startIndex + seek > _seenIndex) {
                // Unknown if input has next item
                _input.setSeek(_startIndex + seek);
                result = _input.peek();
                if (result == null) {
                    // Reached end of range
                    _endIndex = _startIndex + seek;
                } else {
                    _seenIndex = _startIndex + seek;
                }
            } else {
                // Item seen already
                result = _input.peekAt(_startIndex + seek);
            }
            return result;
        }

        function setSeek(seek as Number) {
            while (self.seek < seek && next() != null) {}
            if (seek == 0 || (seek > 0 && (_endIndex == null || seek <= _endIndex - _startIndex))) {
                self.seek = seek;
            } else {
                throw new ValueError(
                    "Item " + seek.toString() + " of Range does not exist");
            }
        }

        function size() as Number {
            exploit();
            return _endIndex - _startIndex;
        }

        function toString() as String {
            return "Range{" + 
                "input=" + _input.toString() + "," +
                "startIndex=" + _startIndex.toString() + "," +
                "endIndex=" + (_endIndex != null ? _endIndex.toString() : "null") + "}";
        }
    }


    (:test)
    function testRange(logger as Logger) as Boolean {

        var testInput = new ArrayIterator(['A', 'B', 'C', 'D', 'E']);
        var startIndex;
        var endIndex;
        var testOutput;
        var obj;
        
        startIndex = 1;
        endIndex = 4;
        testOutput = ['B', 'C', 'D'];
        System.println("Test input: " + testInput + ", range: [" + startIndex + "," + endIndex + ")");
        obj = new Range(testInput, startIndex, endIndex);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        startIndex = 0;
        endIndex = 5;
        testOutput = ['A', 'B', 'C', 'D', 'E'];
        System.println("Test input: " + testInput + ", range: [" + startIndex + "," + endIndex + ")");
        obj = new Range(testInput, startIndex, endIndex);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        startIndex = 4;
        endIndex = null;
        testOutput = ['E'];
        System.println("Test input: " + testInput + ", range: [" + startIndex + "," + endIndex + ")");
        obj = new Range(testInput, startIndex, endIndex);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        startIndex = null;
        endIndex = 2;
        testOutput = ['A', 'B'];
        System.println("Test input: " + testInput + ", range: [" + startIndex + "," + endIndex + ")");
        obj = new Range(testInput, startIndex, endIndex);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        startIndex = null;
        endIndex = null;
        testOutput = ['A', 'B', 'C', 'D', 'E'];
        System.println("Test input: " + testInput + ", range: [" + startIndex + "," + endIndex + ")");
        obj = new Range(testInput, startIndex, endIndex);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        startIndex = 0;
        endIndex = 0;
        testOutput = [];
        System.println("Test input: " + testInput + ", range: [" + startIndex + "," + endIndex + ")");
        obj = new Range(testInput, startIndex, endIndex);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        startIndex = 0;
        endIndex = 10;
        testOutput = ['A', 'B', 'C', 'D', 'E'];
        System.println("Test input: " + testInput + ", range: [" + startIndex + "," + endIndex + ")");
        obj = new Range(testInput, startIndex, endIndex);
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        return true;
    }


    (:test)
    function testRangePeekAndHasNext(logger as Logger) as Boolean {

        var testInput = new ArrayIterator([0, 1, 2, 3]);
        var startIndex;
        var endIndex;
        var obj;
        
        startIndex = 0;
        endIndex = 3;
        obj = new Range(testInput, startIndex, endIndex);

        Test.assertEqual(obj.next(), 0);
        Test.assert(obj.hasNext());

        Test.assertEqual(obj.peek(), 1);
        Test.assertEqual(obj.next(), 1);

        Test.assertEqual(obj.next(), 2);
        Test.assert(!obj.hasNext());
        Test.assert(obj.peek() == null);
        Test.assert(obj.next() == null);

        // Peek before next()
        startIndex = 1;
        endIndex = 2;
        obj = new Range(testInput, startIndex, endIndex);

        Test.assertEqual(obj.peek(), 1);

        // Peek after exhausted
        startIndex = 0;
        endIndex = 3;
        obj = new Range(testInput, startIndex, endIndex);

        obj.setSeek(3);
        Test.assert(obj.peek() == null);

        return true;
    }


    (:test)
    function testRangeGetAndSize(logger as Logger) as Boolean {

        var testInput = new ArrayIterator(['A', 'B', 'C', 'D', 'E']);
        var startIndex = 1;
        var endIndex = 4;
        var obj;
        
        obj = new Range(testInput, startIndex, endIndex);
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(0), 'B');
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.peek(), 'C');
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.get(1), 'C');
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.peek(), 'D');
        Test.assertEqual(obj.get(2), 'D');
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.size(), 3);

        testInput.reset();
        obj = new Range(testInput, startIndex, endIndex);
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(2), 'D');
        Test.assertEqual(obj.getSeek(), 3);
        Test.assert(obj.peek() == null);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.get(1), 'C');
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.get(0), 'B');
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.next(), 'C');
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.size(), 3);

        testInput.reset();
        obj = new Range(testInput, startIndex, endIndex);
        Test.assertEqual(obj.size(), 3);
        obj.setSeek(2);
        Test.assertEqual(obj.next(), 'D');
        obj.setSeek(0);
        Test.assertEqual(obj.next(), 'B');

        return true;
    }


    (:test)
    function testRangeModifiesInputSeek(logger as Logger) as Boolean {

        // First time: input seek is modified
        var testInput = new ArrayIterator(['A', 'B', 'C', 'D', 'E']);
        
        var obj = new Range(testInput, 0, 3);
        obj.exploit();
        Test.assert(testInput.getSeek() == 3);

        // Reset: input seek is not modified
        obj.reset();
        Test.assert(testInput.getSeek() == 3);
        obj.next();
        Test.assert(testInput.getSeek() == 3);

        return true;
    }
}
