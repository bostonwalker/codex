import Toybox.Lang;


module Codex {

    typedef Iterator as interface {

        // Return contents as array
        function asArray() as Array;

        // Check equality of remaining children
        // Iterators should be reset before calling equals()
        function equals(object) as Boolean;

        // Exploit the iterator and reset seek
        function exploit() as Void;

        // Get the nth item in the iterator
        function get(index as Number) as Object;

        // Get index of next item produced by iterator
        function getSeek() as Number;

        // Return true if iterator has one or more items remaining
        function hasNext() as Boolean;

        // Return true if next() has been called at least once since last reset
        function isIterating() as Boolean;

        // Return next item, or null if not exists
        function next() as Object?;

        // Peek at next item in the iterator
        function peek() as Object?;

        // Like get(), but doesn't modify the seek
        function peekAt(index as Number) as Object;

        // Reset iterator to top
        function reset() as Void;

        // Set seek of iterator
        function setSeek(seek as Number) as Void;

        // Return number of items in iterator
        function size() as Number;
    };
}
