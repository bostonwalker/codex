import Toybox.Lang;
import Toybox.Test;
import Toybox.System;



module Codex {

    class MemoIterator extends BaseIterator {

        protected var memo as Array;

        function initialize() {
            /*
            An iterator that memoizes values returned by nextImpl()
            */
            BaseIterator.initialize();
            memo = [];
        }

        function asArray() as Array {
            if (isIterating()) {
                throw new Lang.OperationNotAllowedException(
                    "Cannot call asArray() on MemoIterator while in a loop");
            }
            exploit();
            // Exclude null at end
            return memo.slice(0, memo.size() - 1);
        }

        function getMemo() as Array {
            // Return remaining items that have been loaded so far as array
            if (memo.size() > 0 && memo[memo.size() - 1] == null) {
                // Iterator is fully exploited
                return memo.slice(seek, memo.size() - 1);
            } else {
                return memo.slice(seek, memo.size());
            }
        }

        function peek() as Object? {
            // Peek at next item
            if (seek == memo.size()) {
                // Ran out of memoized items
                memo.add(nextImpl());
            }
            var result = memo[seek];
            return result;
        }

        function setSeek(seek as Number) {
            // Set seek of iterator
            while (seek >= memo.size() && next() != null) {}
            if (seek == 0 || (seek > 0 && seek <= memo.size() && memo[seek - 1] != null)) {
                self.seek = seek;
            } else {
                throw new ValueError(
                    "Item " + seek.toString() + " of MemoIterator does not exist");
            }
        }

        function size() as Number {
            // Return number of items in iterator
            exploit();
            return memo.size() - 1;
        }

        function toString() as String {
            exploit();
            return "MemoIterator{" + memo.slice(0, memo.size() - 1).toString() + "}";
        }

        protected function nextImpl() as Object? {
            // Return next item, or null if not exists
            throw new Lang.OperationNotAllowedException(
                "Not implemented");
        }
    }


    class _TestMemoIterator extends MemoIterator {

        private var _items as Array;

        function initialize(items as Array) {
            MemoIterator.initialize();

            _items = items;
        }

        function nextImpl() as Object? {
            if (seek < _items.size()) {
                return _items[seek];
            } else {
                return null;
            }
        }
    }


    (:test)
    function testMemoIterator(logger as Logger) as Boolean {
        // Simple test of memoization ability
        var testInput = [0, 1, 2, 3, 4];
        var obj = new _TestMemoIterator(testInput);
        System.println("Test input: \"" + testInput + "\"");
        if (!obj.testAgainstOutput(testInput)) {
            return false;
        }
        obj.reset();
        if (!obj.testAgainstOutput(testInput)) {
            return false;
        }
        return true;
    }


    (:test)
    function testMemoIteratorPeekAndHasNext(logger as Logger) as Boolean {

        var testInput = [0, 1, 2];
        var obj = new _TestMemoIterator(testInput);

        Test.assertEqual(obj.next(), 0);
        Test.assert(obj.hasNext());

        Test.assertEqual(obj.peek(), 1);
        Test.assertEqual(obj.next(), 1);

        Test.assertEqual(obj.next(), 2);
        Test.assert(!obj.hasNext());
        Test.assert(obj.peek() == null);
        Test.assert(obj.next() == null);

        return true;
    }


    (:test)
    function testMemoIteratorGetAndSize(logger as Logger) as Boolean {

        var testInput = [0, 1, 2];
        var obj;
        
        obj = new _TestMemoIterator(testInput);
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(0), 0);
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.peek(), 1);
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.get(1), 1);
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.peek(), 2);
        Test.assertEqual(obj.get(2), 2);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.size(), 3);

        obj = new _TestMemoIterator(testInput);
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(2), 2);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assert(obj.peek() == null);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.get(1), 1);
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.get(0), 0);
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.next(), 1);
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.size(), 3);

        obj = new _TestMemoIterator(testInput);
        Test.assertEqual(obj.size(), 3);
        obj.setSeek(2);
        Test.assertEqual(obj.next(), 2);
        obj.setSeek(0);
        Test.assertEqual(obj.next(), 0);

        return true;
    }
}
