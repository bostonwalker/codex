import Toybox.Test;
import Toybox.Lang;
import Toybox.System;



module Codex {

    class While extends BaseIterator {

        private var _input as BaseIterator;
        private var _conditionMethod as Method(options as {:item as Object}) as Boolean;
        private var _conditionOptions as Dictionary;

        private var _endIndex as Number?;

        function initialize(input as BaseIterator, condition as {
            :method as Method(options as {:item as Object}) as Boolean,
            :options as Dictionary
        }) {
            /*
            An iterator that captures a sequence of items that meet a condition. Once the condition returns false,
            no further items will be returned.

            :param input: Input iterator
            :param condition: 
                :method: Method that takes as an argument the next item in an iterator, with optional additional arguments
                :options: Arguments which will be provided to :method along with :item (the current item)
            */
            BaseIterator.initialize();

            _input = input;
            if (!condition.hasKey(:method)) {
                throw new ValueError(
                    "Must provide :method");
            }
            _conditionMethod = condition[:method];

            if (condition.hasKey(:options)) {
                _conditionOptions = condition[:options];
            } else {
                _conditionOptions = {};
            }

            _endIndex = null;
        }

        private function _evalCondition(item as Object) as Boolean {
            var options = _conditionOptions;
            options[:item] = item;
            return _conditionMethod.invoke(options) as Boolean;
        }

        function next() as Object? {
            var result = peek();
            if (result != null) {
                seek ++;
                _input.next();
            }
            return result;
        }

        function peek() as Object? {
            var result;
            if (_endIndex != null && seek == _endIndex) {
                // Reached end
                result = null;
            } else {
                _input.setSeek(seek);
                result = _input.peek();
                if (result == null || !_evalCondition(result)) {
                    // Reached end of range
                    _endIndex = seek;
                    result = null;
                }
            }
            return result;
        }

        function setSeek(seek as Number) {
            if (seek == self.seek) {
                // Shortcut
                return;
            } else if (seek < self.seek) {
                self.seek = seek;
                _input.setSeek(seek);
            } else {
                while (self.seek < seek && next() != null) {}
                if (seek == 0 || (seek > 0 && (_endIndex == null || seek <= _endIndex))) {
                    self.seek = seek;
                    _input.setSeek(seek);
                } else {
                    throw new ValueError(
                        "Item " + seek.toString() + " of While does not exist");
                }
            }
        }

        function size() as Number {
            exploit();
            return _endIndex;
        }

        function toString() as String {
            return "While{" +
                "input=" + _input.toString() + "," +
                "conditionMethod=" + _conditionMethod.toString() + "," +
                "conditionOptions=" + _conditionOptions.toString() + "}";
        }
    }


    class _TestConditions {

        function equalTo(options as {:item as Numeric, :refValue as Numeric}) as Boolean {
            var item = options[:item] as Numeric;
            var refValue = options[:refValue] as Numeric;
            return refValue.equals(item);
        }

        function lessThan(options as {:item as Numeric, :refValue as Numeric}) as Boolean {
            var item = options[:item] as Numeric;
            var refValue = options[:refValue] as Numeric;
            return item < refValue;
        }
    }


    (:test)
    function testWhile(logger as Logger) as Boolean {

        var conditions = new _TestConditions();

        var testInput;
        var testOutput;
        var obj;
        
        testInput = new ArrayIterator([true, true, false]);
        testOutput = [true, true];
        System.println("Test input: \"" + testInput + "\"");
        obj = new While(testInput, {:method => conditions.method(:equalTo), :options => {:refValue => true}});
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        testInput = new ArrayIterator([0, 1, 2, 3, 4]);
        testOutput = [0, 1, 2, 3];
        System.println("Test input: \"" + testInput + "\"");
        obj = new While(testInput, {:method => conditions.method(:lessThan), :options => {:refValue => 4}});
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }
        obj.reset();
        if (!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        return true;
    }


    (:test)
    function testWhilePeekAndHasNext(logger as Logger) as Boolean {

        var conditions = new _TestConditions();

        var testInput = new ArrayIterator([0, 1, 2, 3]);
        var obj = new While(testInput, {:method => conditions.method(:lessThan), :options => {:refValue => 3}});

        Test.assertEqual(obj.next(), 0);
        Test.assert(obj.hasNext());

        Test.assertEqual(obj.peek(), 1);
        Test.assertEqual(obj.next(), 1);

        Test.assertEqual(obj.next(), 2);
        Test.assert(!obj.hasNext());
        Test.assert(obj.peek() == null);
        Test.assert(obj.next() == null);

        return true;
    }


    (:test)
    function testWhileGetAndSize(logger as Logger) as Boolean {

        var conditions = new _TestConditions();

        var testInput = new ArrayIterator([0, 1, 2, 3]);
        var obj;
        
        obj = new While(testInput, {:method => conditions.method(:lessThan), :options => {:refValue => 3}});
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(0), 0);
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.peek(), 1);
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.get(1), 1);
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.peek(), 2);
        Test.assertEqual(obj.get(2), 2);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.size(), 3);

        testInput.reset();
        obj = new While(testInput, {:method => conditions.method(:lessThan), :options => {:refValue => 3}});
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(2), 2);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assert(obj.peek() == null);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.get(1), 1);
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.get(0), 0);
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.next(), 1);
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.size(), 3);

        testInput.reset();
        obj = new While(testInput, {:method => conditions.method(:lessThan), :options => {:refValue => 3}});
        Test.assertEqual(obj.size(), 3);
        obj.setSeek(2);
        Test.assertEqual(obj.next(), 2);
        obj.setSeek(0);
        Test.assertEqual(obj.next(), 0);

        return true;
    }


    (:test)
    function testWhileModifiesInputSeek(logger as Logger) as Boolean {

        var conditions = new _TestConditions();

        // First time should modify seek
        var testInput = new ArrayIterator([0, 1, 2, 3, 4, 5]);
        var obj = new While(testInput, {:method => conditions.method(:lessThan), :options => {:refValue => 3}});

        obj.exploit();
        Test.assert(testInput.getSeek() == 3);

        // Second time should not
        obj.reset();
        Test.assert(testInput.getSeek() == 3);

        return true;
    }


    (:test)
    function testWhileRange(logger as Logger) as Boolean {

        // Stack While on top of Range
        var testInput = new ArrayIterator([
            "## Contact Report",
            "Proword \"Contact\"",
            "A.  Enemy (G)rid location",
            "B.  (E)nemy description (strength/type)",
            "C.  (O)wn action",
            "D.  (T)ime of contact",
        ]);
        testInput.setSeek(1);
        testInput.peek();
        var testRange1 = new Range(testInput, 1, null);
        testRange1.exploit();
        testInput.peek();
        var testRange2 = new Range(testInput, 2, null);
        testRange2.peek();
        Test.assert(testRange2.next().equals("A.  Enemy (G)rid location"));

        return true;
    }
}
