import Toybox.Lang;


module Codex {

    // Raised when functionality is not implemented
    class NotImplementedError extends Lang.Exception {

        function initialize() {
            Lang.Exception.initialize();
        }
    }
}