import Toybox.Lang;


module Codex {

    // Raised when a provided key is not present in the mapping
    class KeyError extends Lang.InvalidValueException {
        
        private var _msg as String;

        function initialize(msg as String) {
            _msg = msg;
            Lang.InvalidValueException.initialize(toString());
        }

        function toString() {
            return "KeyError: " + _msg;
        }
    }
}