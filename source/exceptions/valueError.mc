import Toybox.Lang;


module Codex {

    // Replaces InvalidValueException
    // adds proper toString() method to allow for better handling
    class ValueError extends Lang.InvalidValueException {
        
        private var _msg as String;

        function initialize(msg as String) {
            _msg = msg;
            Lang.InvalidValueException.initialize(toString());
        }

        function toString() {
            return "ValueError: " + _msg;
        }

        function getMsg() as String {
            return _msg;
        }
    }
}
