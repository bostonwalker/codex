import Toybox.Lang;
import Toybox.Graphics;
import Toybox.Test;


module Codex {
    
    (:test)
    function test_load_time_long_paragraph(logger as Logger) as Boolean {
        // Test that loading/unloading a long paragraph does not trigger WatchdogTrippedError
        var theme = Codex.App.THEME_LIGHT;

        var parser = new MarkdownParser(Rez.Strings.Codex_TestString_long_paragraph, 100, theme);
        var body = new Body(parser, 100, {});

        var width = 226;
        var height = 260;
        var buffer = 130;
        var scrollAmount = 110;
        var numScroll = 20;

        var options = {
            :width => width,
            :height => height,
        };
        var screen;
        if (Graphics has :createBufferedBitmap) {
            screen = Graphics.createBufferedBitmap(options).get();
        } else {
            screen = new Graphics.BufferedBitmap(options);
        }

        var dc = screen.getDc();

        for (var i = 0; i < numScroll; i++) {
            var scrollOffset = scrollAmount * i;
            body.loadYRange(dc, scrollOffset, height + buffer + scrollOffset);
        }

        return true;
    }
}




